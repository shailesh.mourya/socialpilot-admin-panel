import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { PropTypes } from 'prop-types';
import { IS_ENTERPRISE } from '../../../utils/misc/config';
import EnterPriceDetailsGenerator from '../../../components/EnterPriceDetailsGenerator';
import getLocalizeText from '../../../utils/functions/getLocalizeText';

const loadEnterPriseUI = (
  defaultEnterPriceTitle,
  logo,
  companyName,
  favicon,
) => (
  <EnterPriceDetailsGenerator
    enterPriceTitle={defaultEnterPriceTitle}
    companyLogo={logo}
    companyName={companyName}
    favicon={favicon}
  />
);

const TitleComponent = ({
  title,
  userDetails,
  enterPriceDetails,
  clientSignupLinkDetails,
}) => {
  const defaultTitle = 'SocialPilot';

  try {
    if (userDetails.status === 'success') {
      const { defaultLanguage } = userDetails.successResponse.data;

      if (title && title.split('-').length) {
        const [mainTitle, smallTitleText] = title.split('-');
        // eslint-disable-next-line no-param-reassign
        title = `${mainTitle}-${getLocalizeText(
          smallTitleText,
          defaultLanguage,
        )}`;
      }
    }
  } catch (error) {
    // will be the default title
  }

  const defaultEnterPriseTitle = title || defaultTitle;

  if (clientSignupLinkDetails.status === 200) {
    const { orgName } = clientSignupLinkDetails.response;
    return loadEnterPriseUI(defaultEnterPriseTitle, '', orgName);
  }

  if (clientSignupLinkDetails.isLoading) {
    return null;
  }

  if (userDetails.status === 'success') {
    const { role, clientOrgName } = userDetails.successResponse.data;
    if (role === 'C') {
      return loadEnterPriseUI(defaultEnterPriseTitle, '', clientOrgName);
    }
  }

  if (userDetails.status === 'success' && IS_ENTERPRISE) {
    const {
      companyLogo,
      companyName,
      favicon,
    } = userDetails.successResponse.data.enterpriseDetails;

    return loadEnterPriseUI(
      defaultEnterPriseTitle,
      companyLogo,
      companyName,
      favicon,
    );
  }
  if (enterPriceDetails.status === 'success' && IS_ENTERPRISE) {
    const { companyName, companyLogo, favicon } = enterPriceDetails;
    return loadEnterPriseUI(
      defaultEnterPriseTitle,
      companyLogo,
      companyName,
      favicon,
    );
  }
  return (
    <Helmet>
      <title>{title || defaultTitle}</title>
    </Helmet>
  );
};

TitleComponent.propTypes = {
  title: PropTypes.string.isRequired,
  userDetails: PropTypes.objectOf(PropTypes.object).isRequired,
  enterPriceDetails: PropTypes.objectOf(PropTypes.string).isRequired,
  clientSignupLinkDetails: PropTypes.objectOf(PropTypes.object).isRequired,
};

const mapStateToProps = state => ({
  userDetails: state.userSetting,
  enterPriceDetails: state.enterPriceDetailsReducer,
  clientSignupLinkDetails: state.clientSignupLinkDetails,
});

export default connect(mapStateToProps)(TitleComponent);

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import CreateActionButton from '../Buttons/createBtn/CreateActionButton';

const EmptyBoxComponent = ({
  src,
  emptyListHeaderText,
  createBtnLink,
  createActionBtnText,
}) => (
  <>
    <img src={src} alt={createActionBtnText} />
    <h2>
      <FormattedMessage
        id={emptyListHeaderText}
        defaultMessage={emptyListHeaderText}
      />
    </h2>
    <div className="no-found-btn">
      <Link to={createBtnLink}>
        <CreateActionButton BtnText={createActionBtnText} />
      </Link>
    </div>
  </>
);

EmptyBoxComponent.propTypes = {
  createActionBtnText: PropTypes.string.isRequired,
  createBtnLink: PropTypes.string.isRequired,
  emptyListHeaderText: PropTypes.string.isRequired,
  src: PropTypes.node.isRequired,
};

export default EmptyBoxComponent;

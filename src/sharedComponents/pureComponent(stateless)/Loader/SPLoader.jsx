/** Uses react-spinner as a loader https://www.npmjs.com/package/react-spinners */

import React from 'react';
import PropTypes from 'prop-types';
import { ClipLoader, ScaleLoader } from 'react-spinners/dist';

import './SPLoader.scss';

const SPLoader = props => {
  switch (props.type) {
    case 'scale':
      return (
        <div className="padding-rotate-top">
          <div className="rotate-loader text-center">
            <ScaleLoader
              color={props.color}
              loading={props.loading}
              size={props.size}
              height={props.height}
              width={props.width}
              radius={props.radius}
              margin={props.margin}
              style={props.style}
            />
          </div>
        </div>
      );

    default:
      return (
        <div className="clip-loader">
          <ClipLoader color={props.color} loading size={props.size} />
        </div>
      );
  }
};

SPLoader.propTypes = {
  type: PropTypes.oneOf([
    'clip',
    'bar',
    'beat',
    'bounce',
    'circle',
    'clip',
    'climbingBox',
    'dot',
    'fade',
    'grid',
    'hash',
    'moon',
    'pacman',
    'propogate',
    'pulse',
    'ring',
    'rise',
    'rotate',
    'scale',
    'sync',
  ]),
  color: PropTypes.string,
  margin: PropTypes.string,
  loading: PropTypes.bool,
  size: PropTypes.number,
  height: PropTypes.number,
  width: PropTypes.number,
  radius: PropTypes.number,
  style: PropTypes.string,
};

SPLoader.defaultProps = {
  type: 'scale',
  color: '#0f67ea',
  margin: '4px',
  loading: true,
  size: 100,
  height: 50,
  width: 4,
  radius: 2,
  style: null,
};

// type: 'clip',
// color: '#0f67ea',
// margin: null,
// loading: true,
// size: 100,
// height: null,
// width: null,
// radius: null,
// style: null,

export default SPLoader;

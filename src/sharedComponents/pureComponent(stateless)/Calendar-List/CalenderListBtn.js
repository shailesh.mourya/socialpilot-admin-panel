import React from 'react';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import DisplayText from '../../lables/index';

const removeSFunction = url => {
  if (!!url && url.length > 1 && url && url !== 'team' && url !== 'client') {
    try {
      return url.slice(0, url.length - 1);
    } catch (error) {
      return '';
    }
  } else {
    return url || '';
  }
};

function CalenderListBtn({ filterId, filter, page }) {
  return (
    <ul>
      <li className={page === 'post' && 'active'}>
        <Link
          className="calendar_link"
          to={
            filterId
              ? `/posts/${removeSFunction(filter)}/${filterId}`
              : '/posts'
          }
        >
          <i className="fas fa-list" aria-hidden="true" />{' '}
          <span className="hidden-xs">
            <FormattedMessage
              id={DisplayText.LIST}
              defaultMessage={DisplayText.LIST}
            />
          </span>
        </Link>
      </li>

      <li className={page === 'calendar' && 'active'}>
        <Link
          className="calendar_link"
          to={
            filterId
              ? `/posts/calendar/${removeSFunction(filter)}/${filterId}`
              : '/posts/calendar'
          }
        >
          <i className="far fa-calendar-alt" aria-hidden="true" />{' '}
          <span className="hidden-xs">
            <FormattedMessage
              id={DisplayText.CALENDAR}
              defaultMessage={DisplayText.CALENDAR}
            />
          </span>
        </Link>
      </li>
    </ul>
  );
}

CalenderListBtn.propTypes = {
  filter: PropTypes.string.isRequired,
  filterId: PropTypes.string.isRequired,
  page: PropTypes.string,
};

CalenderListBtn.defaultProps = {
  page: '',
};

export default CalenderListBtn;

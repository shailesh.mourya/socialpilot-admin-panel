import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import './EmailPdfPopup.scss';
import FormGroup from '../../FormInputs/FormGroup';
import FormControl from '../../FormInputs/FormControl';
import Button from '../../Buttons/Button';
import emailImage from './get-report-icon.png';
import successImage from './email-sent-icon.png';
import SPModal from '../../Modal/SPModal';
import DisplayText from '../../../lables/index';
import getLocalizeText from '../../../../utils/functions/getLocalizeText';

export default class EmailPdfPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      errorEmail: '',
      emailBeingSent: false,
      emailSuccess: null,
    };
    this.closeModal = this.closeModal.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    const { showSucess, dataPdfEmail, email } = this.props;

    if (showSucess && email) {
      this.setState({
        emailSuccess: dataPdfEmail.pdfSendStatus,
        email,
      });
    }
  }

  componentDidUpdate(prevProps) {
    const {
      isLoadingPdfEmail,
      errorPdfEmail,
      dataPdfEmail,
      email,
    } = this.props;
    if (isLoadingPdfEmail !== prevProps.isLoadingPdfEmail) {
      if (dataPdfEmail && !errorPdfEmail) {
        const { pdfSendStatus } = dataPdfEmail;
        if (dataPdfEmail !== prevProps.dataPdfEmail) {
          this.setState({ emailBeingSent: false, emailSuccess: pdfSendStatus });
        }
      } else if (errorPdfEmail !== prevProps.errorPdfEmail) {
        this.setState({ emailBeingSent: false, emailSuccess: false });
      }
      if (email) {
        this.setState({ email });
      }
    }
  }

  doNothing = e => {
    e.preventDefault();
  };

  emailTester = email => {
    // eslint-disable-next-line no-useless-escape
    const tester = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;

    if (!email) return false;

    if (email.length > 254) return false;

    if (!tester.test(email)) return false;

    const parts = email.split('@');
    if (parts.length !== 2) return false;
    if (parts[0].length > 64) return false;

    return true;
  };

  closeModal = () => {
    const { closeModal } = this.props;
    closeModal('email-pdf');
  };

  handleClick(e) {
    e.preventDefault();
    let { email } = this.state;
    const { sendEmail } = this.props;
    email = email.trim();
    let errorFromEmailField;
    if (email.trim().length === 0) {
      errorFromEmailField = 'Please provide your email address.';
    } else if (!this.emailTester(email)) {
      errorFromEmailField = 'Please provide a valid email address.';
    }
    this.setState(
      state => ({
        ...state,
        errorEmail: errorFromEmailField,
      }),
      () => {
        if (!errorFromEmailField) {
          sendEmail(email);
          this.setState({ emailBeingSent: true });
        }
      },
    );
  }

  handleEmailChange(e) {
    this.setState({
      email: e.target.value,
      errorEmail: '',
      emailSuccess: null,
    });
  }

  render() {
    const {
      isLoadingPdfEmail,
      showModal,
      height,
      width,
      errorPdfEmail,
    } = this.props;
    const { errorEmail, email } = this.state;
    const { emailBeingSent, emailSuccess } = this.state;

    const isLoading = isLoadingPdfEmail;

    const emailSuccessResponse =
      !emailBeingSent !== null && !emailBeingSent && emailSuccess;
    const containerExtraClass =
      emailSuccessResponse !== null && emailSuccessResponse === true
        ? 'email-success-extra'
        : '';
    return (
      <div>
        <SPModal
          showModal={showModal}
          styles={{
            modal: {
              width,
              height,
              maxWidth: width,
            },
          }}
          classNames={{
            overlay: 'email-popup-overlay',
            modal: 'share-popup-modal',
            closeButton: 'share-popup-close-modal',
          }}
          onCloseModal={this.closeModal}
        >
          <div className={`email-pdf-container ${containerExtraClass}`}>
            <img
              src={emailSuccessResponse ? successImage : emailImage}
              className="email-icon"
              alt="emai-pdf"
            />
            {!emailSuccessResponse && (
              <h3>
                <FormattedMessage
                  id={DisplayText.GET_REPORTS}
                  defaultMessage={DisplayText.GET_REPORTS}
                />
              </h3>
            )}
            {!emailSuccessResponse && (
              <p className="enter-email-msg">
                <FormattedMessage
                  id={DisplayText.ENTER_EMAIL_TO_GET_REPORTS}
                  defaultMessage={DisplayText.ENTER_EMAIL_TO_GET_REPORTS}
                />
              </p>
            )}
            <form>
              {!emailSuccessResponse && (
                <>
                  <FormGroup controlId="formGridEmail">
                    <div className="form-email">
                      <div
                        className={`input-group ${
                          errorEmail ? 'is-invalid' : ''
                        }`}
                      >
                        <FormControl
                          value={email}
                          disabled={!!isLoading}
                          onChange={this.handleEmailChange}
                          placeholder={getLocalizeText('Email Address')}
                          bsClass={`form-control ${
                            errorEmail ? 'is-invalid' : ''
                          }`}
                        />
                      </div>
                      {!!errorEmail && (
                        <p className="invalid-feedback">
                          {errorEmail ? (
                            <FormattedMessage
                              id={errorEmail}
                              defaultMessage={errorEmail}
                            />
                          ) : (
                            ''
                          )}
                        </p>
                      )}
                      {!isLoading &&
                      !emailSuccessResponse &&
                      !emailBeingSent !== null &&
                      !emailBeingSent &&
                      emailSuccess !== null ? (
                        <div className="help-block help-block-error error-email">
                          {errorPdfEmail && errorPdfEmail[0]
                            ? errorPdfEmail[0].message ||
                              DisplayText.EMAIL_SEND_ERROR
                            : DisplayText.EMAIL_SEND_ERROR}
                        </div>
                      ) : null}
                    </div>
                  </FormGroup>
                  <div className="send-form-btn">
                    <Button
                      bsClass={`btn send-button ${
                        isLoading ? 'disable-send-button' : ''
                      }`}
                      variant="primary"
                      type="submit"
                      onClick={isLoading ? this.doNothing : this.handleClick}
                    >
                      <FormattedMessage
                        id={DisplayText.SEND_REPORT}
                        defaultMessage={DisplayText.SEND_REPORT}
                      />
                    </Button>
                  </div>
                </>
              )}
              {isLoading ? (
                <div className="email-loading-msg">
                  <i
                    id="cur-search-loader"
                    style={{ color: 'orange' }}
                    className="fa fa-spinner fa-spin icon-large"
                  />
                  Sending Report...
                </div>
              ) : null}
              {emailSuccessResponse ? (
                <div className="email-success-error-msg">
                  <h3>
                    {' '}
                    <FormattedMessage
                      id={DisplayText.EMAIL_SENT}
                      defaultMessage={DisplayText.EMAIL_SENT}
                    />
                  </h3>
                  <p>
                    <FormattedMessage
                      id={DisplayText.EMAIL_SENT_TO}
                      defaultMessage={DisplayText.EMAIL_SENT_TO}
                    />{' '}
                    {email}
                  </p>
                </div>
              ) : null}
            </form>
          </div>
        </SPModal>
      </div>
    );
  }
}

EmailPdfPopup.propTypes = {
  isLoadingPdfEmail: PropTypes.bool.isRequired,
  showSucess: PropTypes.bool.isRequired,
  errorPdfEmail: PropTypes.string.isRequired,
  dataPdfEmail: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  closeModal: PropTypes.func.isRequired,
  sendEmail: PropTypes.func.isRequired,
  showModal: PropTypes.bool.isRequired,
  height: PropTypes.string.isRequired,
  width: PropTypes.string.isRequired,
};

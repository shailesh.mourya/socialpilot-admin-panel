/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import PropTypes from 'prop-types';
import './DownloadButton.scss';
import dropdownIcons from './download_pdf_icons.png';
import SplitButton from '../../SplitButton/SplitButton';

const DownloadButton = ({ onClick, data }) => {
  let downloadClass = '';
  let buttonDisable = false;
  if (data) {
    downloadClass = 'analytics-donwload';
  } else {
    downloadClass = 'analytics-donwload download_disable';
    buttonDisable = true;
  }
  if (data && parseInt(data.isReconnect, 10) === 1) {
    downloadClass = 'analytics-donwload download_disable';
    buttonDisable = true;
  }
  const splitButtonObject = {
    dropDownButton: [
      { name: 'Download PDF', value: 'download-pdf' },
      { name: 'Email PDF', value: 'email-pdf' },
    ],
  };
  return (
    <div className={downloadClass}>
      <div className="dropdown show">
        <SplitButton
          style={{ backgroundImage: `url(${dropdownIcons})` }}
          btnType={'btn-primary'}
          mainButton={{ name: 'Download', value: '' }}
          dropDownButton={splitButtonObject.dropDownButton}
          clickHandler={shareType => {
            if (shareType === 'download-pdf') {
              if (data && parseInt(data.isReconnect, 10) === 0) {
                onClick('download-pdf');
              }
            } else if (shareType === 'email-pdf') {
              if (data && parseInt(data.isReconnect, 10) === 0) {
                onClick('email-pdf');
              }
            }
          }}
          disabled={buttonDisable}
        />
      </div>
      {/* <div className="dropdown show">
        <a
          className="btn btn-primary dropdown-toggle"
          href="javaScript:;"
          role="button"
          id="dropdownMenuLink"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          <div className="download-btn-div">
            <FormattedMessage id="Download" defaultMessage="Download" />
          </div>
        </a>

        <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
          <a
            className="dropdown-item fb-download-pdf"
            style={{ backgroundImage: `url(${dropdownIcons})` }}
            href="javaScript:;"
            onClick={() =>
              data && parseInt(data.isReconnect, 10) === 0
                ? onClick('download-pdf')
                : null
            }
          >
            <FormattedMessage id="Download PDF" defaultMessage="Download PDF" />
          </a>
          <a
            className="dropdown-item email-pdf"
            style={{ backgroundImage: `url(${dropdownIcons})` }}
            href="javaScript:;"
            onClick={() =>
              data && parseInt(data.isReconnect, 10) === 0
                ? onClick('email-pdf')
                : null
            }
          >
            <FormattedMessage id="Email PDF" defaultMessage="Email PDF" />
          </a>
        </div>
      </div> */}
    </div>
  );
};

DownloadButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  data: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default DownloadButton;

/* eslint-disable react/destructuring-assignment */
import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import DisplayText from '../../../lables/index';

import './AccountLoadingAlert.css';

const AccountLoadingAlert = props => (
  <>
    <div className="row mt-2">
      <div className="col-lg-12 form-inline">
        <div className="text-danger ">
          <FormattedMessage id={props.error} defaultMessage={props.error} />
        </div>
        &nbsp;
        <a href={props.link} className="btn-number_formated btn btn-danger">
          <i className="fas fa-sync-alt" />
          <span className="hidden-xs hidden-sm">
            <FormattedMessage
              id={DisplayText.RECONNECT}
              defaultMessage={DisplayText.RECONNECT}
            />
          </span>
        </a>
      </div>
    </div>
  </>
);

AccountLoadingAlert.propTypes = {
  error: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
};

export default AccountLoadingAlert;

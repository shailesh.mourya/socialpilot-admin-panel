import React from 'react';
import PropTypes from 'prop-types';
import './Container.css';

const Container = ({ children }) => (
  <div className="analytics_container">
    <div className="page-container">{children}</div>
  </div>
);

export default Container;

Container.propTypes = {
  children: PropTypes.element.isRequired,
};

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class FormGroup extends PureComponent {
  render() {
    const { children, extraClass } = this.props;
    return <div className={`form-group ${extraClass}`}>{children}</div>;
  }
}

FormGroup.propTypes = {
  children: PropTypes.node.isRequired,
  extraClass: PropTypes.string,
};

FormGroup.defaultProps = {
  extraClass: '',
};

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class FormControl extends PureComponent {
  render() {
    const {
      placeholder,
      type,
      onChange,
      value,
      bsClass,
      id,
      onEnterKeyPress,
      disabled,
      hasError,
      maxLength,
    } = this.props;
    return (
      <input
        disabled={disabled}
        placeholder={placeholder}
        type={type}
        id={id}
        className={`form-control ${bsClass || ''} ${hasError && 'is-invalid'}`}
        value={value}
        maxLength={maxLength}
        onChange={onChange}
        onKeyPress={event => {
          if (event.key === 'Enter') {
            onEnterKeyPress(event);
          }
        }}
      />
    );
  }
}

FormControl.propTypes = {
  bsClass: PropTypes.string,
  id: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onEnterKeyPress: PropTypes.func,
  disabled: PropTypes.bool,
  hasError: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  maxLength: PropTypes.number,
};
FormControl.defaultProps = {
  bsClass: '',
  id: '',
  onChange: () => {},
  disabled: false,
  maxLength: null,
  onEnterKeyPress: () => {},
  placeholder: '',
  type: '',
  value: '',
  hasError: '',
};

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-responsive-modal';
import { FormattedMessage } from 'react-intl';
import Button from '../Buttons/Button';
import DisplayText from '../../lables/index';

const createPostTitle = createFrom => {
  switch (createFrom) {
    case 'editPost':
    case 'calendarEditPost':
      return DisplayText.EDIT_POST;
    case 'reshare':
    case 'calendarResharePost':
      return DisplayText.RESHARE_POST;
    case 'editDraft':
      return DisplayText.EDIT_DRAFT_POST;
    default:
      return DisplayText.CREATE_POST;
  }
};

export default class SPModal extends PureComponent {
  render() {
    const {
      title,
      children,
      onCloseModal,
      styles,
      showModal,
      classNames,
      createFrom,
      closeOnOverlayClick,
    } = this.props;
    const popupTitle = title || createPostTitle(createFrom);
    return (
      <Modal
        open={showModal}
        onClose={onCloseModal}
        closeOnOverlayClick={closeOnOverlayClick !== false}
        showCloseIcon={false}
        styles={styles}
        classNames={classNames}
        center
      >
        <div className="modal-dialog">
          <div className="modal-content">
            {title || createFrom ? (
              <div className="modal-header">
                <Button
                  type="button"
                  id="close-button"
                  bsClass="close"
                  dataDismiss="modal"
                  onClick={onCloseModal}
                  ariaHidden="true"
                >
                  ×
                </Button>
                <h2>
                  <FormattedMessage
                    id={popupTitle}
                    defaultMessage={popupTitle}
                  />
                </h2>
              </div>
            ) : (
              <Button
                type="button"
                id="close-button"
                bsClass="close"
                dataDismiss="modal"
                onClick={onCloseModal}
                ariaHidden="true"
              >
                ×
              </Button>
            )}
            <div className="modal-body">{children}</div>
          </div>
        </div>
      </Modal>
    );
  }
}

SPModal.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
  onCloseModal: PropTypes.func.isRequired,
  showModal: PropTypes.bool.isRequired,
  styles: PropTypes.oneOfType([PropTypes.object]),
  classNames: PropTypes.oneOfType([PropTypes.object]),
  createFrom: PropTypes.string,
  closeOnOverlayClick: PropTypes.bool,
};
SPModal.defaultProps = {
  title: '',
  styles: {},
  classNames: {},
  createFrom: '',
  closeOnOverlayClick: true,
};

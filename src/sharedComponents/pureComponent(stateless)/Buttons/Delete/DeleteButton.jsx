/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import DisplayText from '../../../lables/index';
import GlobalDisplayTexts from '../../../../utils/labels/localizationText';
import './DeleteButton.scss';

export const DeleteButton = ({
  onClick,
  disabled,
  condition,
  text,
  isLoading,
}) => {
  if (disabled) {
    return null;
  }
  if (condition) {
    return (
      <>
        <a
          disabled={disabled}
          onClick={onClick}
          href="javaScript:;"
          className="text-danger mx-2"
        >
          <i className="fa fa-trash-o  " aria-hidden="true" />
          <span className="">
            <FormattedMessage id={text} defaultMessage={text} />
          </span>
        </a>
        {isLoading && (
          <i
            aria-hidden="true"
            className="fas fa-spinner fa-spin copy-to-loader"
          />
        )}
      </>
    );
  }
  return null;
};

DeleteButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  condition: PropTypes.bool,
  text: PropTypes.string.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

DeleteButton.defaultProps = {
  disabled: false,
  condition: true,
};

export default DeleteButton;

export const TextForAllApiSelection = ({
  totalRecords,
  allApiPostDelete,
  checkAccounts,
  apiAllListCheckHandler,
  totalPages,
  from,
}) => {
  if (totalPages && totalPages === 1) return null;

  let deleteText = `${GlobalDisplayTexts.SELECT_ALL} ${totalRecords} ${from ||
    DisplayText.POSTS}.`;
  if (allApiPostDelete) {
    deleteText = DisplayText.CLEAR_SELECTION;
  }

  if (from && checkAccounts.length === 1 && !allApiPostDelete) {
    // here for removing s which means change, feeds=>feed
    // eslint-disable-next-line no-param-reassign
    from = from.replace('s', '');
  }
  const length = allApiPostDelete ? totalRecords : checkAccounts.length;
  const postSelectedText = `${from ? '' : 'All'} ${length} ${from ||
    'posts'} Selected.`;

  if (from && !checkAccounts.length) return null;

  return (
    <>
      <FormattedMessage
        id={postSelectedText}
        defaultMessage={postSelectedText}
      />{' '}
      <a href="javaScript:;" onClick={apiAllListCheckHandler}>
        <FormattedMessage id={deleteText} defaultMessage={deleteText} />
      </a>
    </>
  );
};

TextForAllApiSelection.propTypes = {
  allApiPostDelete: PropTypes.bool.isRequired,
  apiAllListCheckHandler: PropTypes.func.isRequired,
  checkAccounts: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.string, PropTypes.number]),
  ).isRequired,
  totalRecords: PropTypes.number.isRequired,
  totalPages: PropTypes.number,
  from: PropTypes.string,
};

TextForAllApiSelection.defaultProps = {
  totalPages: 0,
  from: '',
};

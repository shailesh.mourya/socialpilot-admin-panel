/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import Button from '../Button';
import './CreateActionButton.scss';

const CreateActionButton = ({
  onclick,
  BtnText,
  href,
  BtnClassName,
  iconClassName,
}) => {
  const CommonIcons = () => (
    <>
      <i className={`${iconClassName}`} aria-hidden="true" />
      <FormattedMessage id={BtnText} defaultMessage={BtnText} />
    </>
  );

  if (href) {
    return (
      <Link className={`btn ${BtnClassName}`} to={href}>
        <CommonIcons BtnText={BtnText} />
      </Link>
    );
  }

  return (
    <Button type="button" onClick={onclick} bsClass={`btn ${BtnClassName}`}>
      <CommonIcons />
    </Button>
  );
};
CreateActionButton.propTypes = {
  BtnText: PropTypes.string,
  href: PropTypes.string,
  onclick: PropTypes.func,
  iconClassName: PropTypes.string,
  BtnClassName: PropTypes.string,
};

CreateActionButton.defaultProps = {
  BtnText: 'create',
  onclick: () => {},
  iconClassName: 'fas fa-plus-circle',
  BtnClassName: 'btn-primary',
  href: '',
};

export default CreateActionButton;

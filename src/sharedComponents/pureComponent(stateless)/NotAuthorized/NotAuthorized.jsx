import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import DisplayText from '../../lables/index';

import unauthorised from '../../../assets/unauthorised.png';
import socialPilotLogo from '../../../assets/sp_logo.png';

import './NotAuthorized.css';
import { AlertMsg } from '../Alerts/Alert';

export default class NotAuthorized extends PureComponent {
  render() {
    const url = new URL(window.location.href);
    const from = url.searchParams.get('from');
    if (from === 'extension') {
      const data = {
        command: 'sPilotResize',
        height: 89,
        width: 800,
      };
      window.top.postMessage(data, '*');
      return (
        <AlertMsg type="danger" msg={DisplayText.NOT_AUTHORIZED} disableClose />
      );
    }
    return (
      <>
        <Helmet>
          <title>{`Not Authorized`}</title>
          <meta name="description" content="Page Not Authorized" />
          <meta name="theme-color" content="#ccc" />
        </Helmet>
        <div className="container">
          <div className="row">
            <div className="lang-main-block" />
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="text-center">
                <a href="https://www.socialpilot.co" className="brand">
                  <img
                    src={socialPilotLogo}
                    alt="SocialPilot"
                    title="SocialPilot"
                  />
                </a>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="text-center site-error">
                <img
                  src={unauthorised}
                  alt="You are not authorized to access this page"
                />
                <div className="processing-line">
                  <p>
                    <FormattedMessage
                      id={DisplayText.NOT_AUTHORIZED}
                      defaultMessage={DisplayText.NOT_AUTHORIZED}
                    />
                  </p>
                  <br />
                  <br />
                  <Link to="/" className="btn btn-primary">
                    <FormattedMessage
                      id={DisplayText.BACK_TO_DASHBOARD}
                      defaultMessage={DisplayText.BACK_TO_DASHBOARD}
                    />
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

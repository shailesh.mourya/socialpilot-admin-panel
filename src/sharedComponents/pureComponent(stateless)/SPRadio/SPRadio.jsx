import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

export default class SPRadio extends PureComponent {
  render() {
    const {
      extraClassName,
      label,
      name,
      checked,
      onChange,
      value,
      id,
      disabled,
      inlineClass,
    } = this.props;
    return (
      <div className={`custom-control custom-radio ${inlineClass}`}>
        <input
          type="radio"
          id={id}
          name={name}
          checked={checked}
          className={`custom-control-input ${extraClassName}`}
          onChange={onChange}
          value={value}
          disabled={disabled}
        />
        {label && (
          <label
            className={`custom-control-label ${
              disabled ? 'disable-frequency' : ''
            }`}
            htmlFor={id}
          >
            <FormattedMessage id={label} defaultMessage={label} />
          </label>
        )}
      </div>
    );
  }
}

SPRadio.propTypes = {
  extraClassName: PropTypes.string,
  checked: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  value: PropTypes.string.isRequired,
  id: PropTypes.string,
  disabled: PropTypes.bool,
  inlineClass: PropTypes.string,
};

SPRadio.defaultProps = {
  extraClassName: '',
  label: '',
  checked: false,
  disabled: false,
  onChange: () => {},
  id: 'checkboxid',
  inlineClass: 'custom-control-inline',
};

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import FormGroup from '../FormInputs/FormControl';

function InputWithAlertLabel(props) {
  const { hasError, extraClass, labelText, errorMessage, id } = props;
  return (
    <div className={`${extraClass}`}>
      <label className="control-label" htmlFor={id}>
        <FormattedMessage id={labelText} defaultValue={labelText} />
      </label>
      <FormGroup {...props} />
      <div className="invalid-feedback">
        {hasError && (
          <FormattedMessage id={errorMessage} defaultValue={errorMessage} />
        )}
      </div>
    </div>
  );
}

InputWithAlertLabel.propTypes = {
  hasError: PropTypes.string,
  extraClass: PropTypes.string,
  labelText: PropTypes.string,
  errorMessage: PropTypes.string,
  id: PropTypes.string,
};

InputWithAlertLabel.defaultProps = {
  hasError: '',
  extraClass: '',
  labelText: '',
  errorMessage: '',
  id: '',
};

export default InputWithAlertLabel;

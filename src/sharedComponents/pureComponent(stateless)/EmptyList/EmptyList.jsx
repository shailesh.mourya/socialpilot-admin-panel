import React from 'react';
import PropTypes from 'prop-types';
import './EmptyList.scss';

const EmptyList = ({ Box2Content, Box1Content }) => (
  <div className="no-record-found">
    {Box2Content === null ? (
      <div className="row">
        <div className="col-lg-4" />
        <div className="col-sm-6 col-lg-4 no-found-box">
          <div className="no-record-box">{Box1Content}</div>
        </div>
      </div>
    ) : (
      <div className="row">
        <div className="col-lg-2" />
        <div className="col-sm-6 col-lg-4 no-found-box">
          <div className="no-record-box">{Box1Content}</div>
        </div>
        <div className="col-sm-6 col-lg-4 no-found-box">
          <div className="no-record-box">{Box2Content}</div>
        </div>
      </div>
    )}
  </div>
);

EmptyList.propTypes = {
  Box1Content: PropTypes.string,
  Box2Content: PropTypes.string,
};

EmptyList.defaultProps = {
  Box1Content: '',
  Box2Content: '',
};

export default EmptyList;

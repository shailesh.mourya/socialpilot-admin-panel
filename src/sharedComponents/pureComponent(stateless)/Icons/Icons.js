import React from 'react';

export const EditIcon = (id = '') => (
  <span id={id} className="fas fa-pencil-alt" />
);

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import DisplayText from '../../lables/index';
import './ErrorView.scss';

const ErrorView = ({ href }) => (
  <div className="error-view">
    <i className="fas fa-exclamation-triangle" />
    <h1>
      <FormattedMessage
        id={DisplayText.OOPS_SOMETHING_WENT_WRONG}
        defaultMessage={DisplayText.OOPS_SOMETHING_WENT_WRONG}
      />
    </h1>

    <p>
      <FormattedMessage
        id={DisplayText.REQUEST_COMPLETE_ERROR}
        defaultMessage={DisplayText.REQUEST_COMPLETE_ERROR}
      />
    </p>

    <a href={href} type="button" className="btn btn-primary">
      <FormattedMessage
        id={DisplayText.TRY_AGAIN}
        defaultMessage={DisplayText.TRY_AGAIN}
      />
    </a>
  </div>
);

ErrorView.propTypes = {
  href: PropTypes.string,
};
ErrorView.defaultProps = {
  href: window.location.href,
};

export default ErrorView;

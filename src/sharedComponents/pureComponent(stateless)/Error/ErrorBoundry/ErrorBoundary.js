import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import ErrorView from '../ErrorView';
import log from '../../../../services/logger';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
    };
  }

  componentDidCatch(error) {
    this.setState(state => {
      const currentState = { ...state };
      currentState.hasError = true;
      log('ErrorBoundary', error);

      return currentState;
    });
  }

  render() {
    const { hasError } = this.state;
    const { children, history } = this.props;

    history.listen(() => {
      if (hasError) {
        this.setState({
          hasError: false,
        });
      }
    });

    if (hasError) {
      return <ErrorView href={history.location.pathname} />;
    }
    return children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node.isRequired,
  history: PropTypes.objectOf(PropTypes.object).isRequired,
};

export default withRouter(ErrorBoundary);

/* eslint-disable no-script-url */
import React, { PureComponent } from 'react';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { Link } from 'react-router-dom';
import PageNotFound from '../../../assets/page-not-found.png';
import SocialPilotLogo from '../../../assets/sp_logo.png';
import DisplayText from '../../lables/index';

export default class NotFound extends PureComponent {
  render() {
    return (
      <>
        <Helmet>
          <title>{`Not Found`}</title>
          <meta name="description" content="Page Not Found" />
          <meta name="theme-color" content="#ccc" />
        </Helmet>
        <div className="container">
          <div className="row">
            <div className="lang-main-block" />
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="text-center">
                <a href="https://www.socialpilot.co" className="brand">
                  <img
                    src={SocialPilotLogo}
                    alt="SocialPilot"
                    title="SocialPilot"
                  />
                </a>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="text-center site-error">
                <img src={PageNotFound} alt="page not found" />
                <div className="processing-line">
                  <p>{DisplayText.PAGE_DOESNT_EXIST}</p>
                  {/* <h1>Not Found</h1> */}
                  <br />
                  <br />
                  <Link to="/" className="btn btn-primary">
                    <FormattedMessage
                      id={DisplayText.BACK_TO_DASHBOARD}
                      defaultMessage={DisplayText.BACK_TO_DASHBOARD}
                    />
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

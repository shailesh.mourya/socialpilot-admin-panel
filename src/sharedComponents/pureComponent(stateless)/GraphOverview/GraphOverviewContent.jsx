import React from 'react';
import PropTypes from 'prop-types';

const GraphOverviewContent = props => {
  const { extraClassName, children } = props;
  return (
    <div className={extraClassName || 'graph_overview_stats'}>{children}</div>
  );
};

GraphOverviewContent.propTypes = {
  extraClassName: PropTypes.string,
  children: PropTypes.node.isRequired,
};
GraphOverviewContent.defaultProps = {
  extraClassName: '',
};

export default GraphOverviewContent;

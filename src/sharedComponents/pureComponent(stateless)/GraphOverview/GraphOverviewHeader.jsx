import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import './GraphOverviewHeader.scss';

const GraphOverviewHeader = props => {
  const { header, subheader } = props;
  return (
    <>
      <h3>
        <FormattedMessage id={header} defaultMessage={header} />
      </h3>
      {subheader && (
        <span>
          <FormattedMessage id={subheader} defaultMessage={subheader} />
        </span>
      )}
    </>
  );
};

GraphOverviewHeader.propTypes = {
  header: PropTypes.string.isRequired,
  subheader: PropTypes.string,
};

GraphOverviewHeader.defaultProps = {
  subheader: '',
};

export default GraphOverviewHeader;

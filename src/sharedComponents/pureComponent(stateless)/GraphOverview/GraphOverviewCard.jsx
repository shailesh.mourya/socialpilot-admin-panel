import React from 'react';
import PropTypes from 'prop-types';
import './GraphOverviewCard.scss';

const GraphOverviewCard = props => {
  const { children } = props;
  return <div className="graph_overview align-self-end">{children}</div>;
};
GraphOverviewCard.propTypes = {
  children: PropTypes.node.isRequired,
};

export default GraphOverviewCard;

import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import DisplayText from '../../lables/index';

const GraphOverviewGender = props => {
  const { female, type, male } = props;
  return (
    <>
      <div className="fanoverview d-flex justify-content-center">
        <div className="female_percent fan_percent">
          <div className="fan_icon">
            <i className="fa fa-female" aria-hidden="true" />
            <strong>{female}%</strong>
          </div>
          <div className="fan_percent_chield text-center">
            <span>
              <FormattedMessage
                id={`${DisplayText.FEMALE} ${type}`}
                defaultMessage={`${DisplayText.FEMALE} ${type}`}
              />
            </span>
          </div>
        </div>
        <div className="male_percent fan_percent">
          <div className="fan_icon">
            <i className="fa fa-male" aria-hidden="true" />
            <strong>{male}%</strong>
          </div>
          <div className="fan_percent_chield text-center">
            <span>
              <FormattedMessage
                id={`${DisplayText.MALE} ${type}`}
                defaultMessage={`${DisplayText.MALE} ${type}`}
              />
            </span>
          </div>
        </div>
      </div>
    </>
  );
};

GraphOverviewGender.propTypes = {
  female: PropTypes.number.isRequired,
  male: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
};

export default GraphOverviewGender;

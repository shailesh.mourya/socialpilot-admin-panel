/* eslint-disable jsx-a11y/control-has-associated-label */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default class EditDelete extends PureComponent {
  manageDelete = event => {
    let { id } = event.target;
    const { delete: deleteLink, id: groupId } = this.props;
    if (!id) {
      id = groupId;
    }
    deleteLink([parseInt(id, 10)]);
  };

  render() {
    const { onEdit, id, href, canDelete } = this.props;
    const editIcon = <i id={id} className="fas fa-pencil-alt" />;

    return (
      <>
        {href ? (
          <Link to={href}>{editIcon}</Link>
        ) : (
          <a href="javascript:;" onClick={onEdit}>
            {editIcon}
          </a>
        )}
        {canDelete && (
          <a href="javascript:;" onClick={this.manageDelete} title="Delete">
            <i
              id={id}
              className="fa fa-trash-o group_delete text-danger "
              tabIndex="0"
            />
          </a>
        )}
      </>
    );
  }
}

EditDelete.defaultProps = {
  canDelete: true,
  href: '',
  onEdit: () => {},
};

EditDelete.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  delete: PropTypes.func.isRequired,
  onEdit: PropTypes.func,
  href: PropTypes.string,
  canDelete: PropTypes.bool,
};

import React from 'react';
import PropTypes from 'prop-types';

class SPCheckbox extends React.PureComponent {
  render() {
    const {
      children,
      className,
      disabled,
      checked,
      value,
      id,
      onChange,
    } = this.props;

    return (
      <div className={`custom-control custom-checkbox ${className} `}>
        <input
          type="checkbox"
          className="custom-control-input"
          onChange={e => onChange(e.target.id, e.target.checked)}
          value={value}
          disabled={disabled}
          checked={checked}
          id={id}
        />

        <label
          htmlFor={id}
          className="custom-control-label"
          disabled={disabled}
        >
          {children}
        </label>
      </div>
    );
  }
}
SPCheckbox.defaultProps = {
  disabled: false,
  id: null,
  checked: false,
  onChange: () => {},
  className: '',
  value: '',
  children: '',
};

SPCheckbox.propTypes = {
  checked: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.array,
    PropTypes.object,
  ]),
  className: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChange: PropTypes.func,
  value: PropTypes.string,
};

export default SPCheckbox;

/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */

import React, { PureComponent } from 'react';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import upgradeMessage from './upgradeMessage';
import DisplayText from '../../lables/index';
import GlobalDisplayTexts from '../../../utils/labels/localizationText';
import './upgradePopUp.css';

export default class MembershipModal extends PureComponent {
  render() {
    const { role, onMembershipClick, type } = this.props;
    const {
      headerText,
      feature,
      featureAvailableInPlan,
      image,
      featureDescription,
    } = upgradeMessage(type);

    const upgradeBoostPostText = GlobalDisplayTexts.NO_ACCESS_CONTACT_OWNER_MSG;
    const headerUpgradeText =
      role !== 'O' ? DisplayText.SORRY_CANT_USE_FEATURE : headerText;
    const textToshow =
      role !== 'O' ? upgradeBoostPostText : featureAvailableInPlan;
    return (
      <div className="row">
        <div className="col-lg-6 col-sm-6">
          <div className="download-sample">
            <h2>
              <FormattedMessage
                id={headerUpgradeText}
                defaultMessage={headerUpgradeText}
              />
            </h2>

            <div className="download-pdf download-pdf-trail download-pdf-trail">
              {type !== 'email-pdf' ? (
                <span>
                  <FormattedMessage id={feature} defaultMessage={feature} />
                </span>
              ) : (
                ''
              )}
            </div>

            <p>
              <FormattedMessage id={textToshow} defaultMessage={textToshow} />
            </p>
            {role === 'O' && (
              <a
                href="javascript:;"
                className="btn payment_popup"
                onClick={() => onMembershipClick()}
              >
                <FormattedMessage
                  id={GlobalDisplayTexts.UPGRADE_PLAN_NOW}
                  defaultMessage={GlobalDisplayTexts.UPGRADE_PLAN_NOW}
                />
              </a>
            )}
          </div>
        </div>
        <div className="col-lg-6 col-sm-6">
          <div className="right_strategic">
            <img src={image} alt="boost-post-membership" />
            <p>
              <FormattedMessage
                id={featureDescription}
                defaultMessage={featureDescription}
              />
            </p>
          </div>
        </div>
      </div>
    );
  }
}

MembershipModal.propTypes = {
  onMembershipClick: PropTypes.func.isRequired,
  role: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

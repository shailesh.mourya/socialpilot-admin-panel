import multipleImage from '../../../assets/upgradePopUp/lock_multiple.png';
import draftImage from '../../../assets/upgradePopUp/lock_drafts_popup.png';
import gif from '../../../assets/upgradePopUp/lock_gifpopup.png';
import dropbox from '../../../assets/upgradePopUp/lock_dropbox_popup.png';
import box from '../../../assets/upgradePopUp/lock_box_popup.png';
import advancedPost from '../../../assets/upgradePopUp/lock_advanced_post_popup.png';
import pdfDownload from '../../../assets/upgradePopUp/upgrade_analysis_popup.png';
import boostpost from '../../../assets/upgradePopUp/upgrade_boostpost_popup.png';
import DisplayText from '../../lables/index';
import GlobalDisplayTexts from '../../../utils/labels/localizationText';

const upgradeMessage = type => {
  let headerText = GlobalDisplayTexts.UPGRADE_TO_ACCESS_PRO_FAETURE;
  let feature = '';
  let featureAvailableInPlan = '';
  let image = '';
  let featureDescription = '';

  switch (type) {
    case 'multipleImage':
      feature = DisplayText.MULTI_IMG_POSTING;
      featureAvailableInPlan = DisplayText.MULTI_IMG_PLAN_MSG;
      image = multipleImage;
      featureDescription = DisplayText.MULTI_IMG_DESC;
      break;
    case 'gif':
      feature = DisplayText.GIF_SUPPORT;
      featureAvailableInPlan = DisplayText.GIF_SUPPORT_PLAN_MSG;
      image = gif;
      featureDescription = DisplayText.GIF_SUPPORT_DESC;
      break;
    case 'dropbox':
      feature = DisplayText.DROPBOX_SUPPORT;
      featureAvailableInPlan = DisplayText.DROPBOX_SUPPORT_PLAN_MSG;
      image = dropbox;
      featureDescription = DisplayText.DROPBOX_SUPPORT_DESC;
      break;
    case 'box':
      feature = DisplayText.BOX_SUPPORT;
      featureAvailableInPlan = DisplayText.BOX_SUPPORT_PLAN_MSG;
      image = box;
      featureDescription = DisplayText.BOX_SUPPORT_DESC;
      break;
    case 'draft':
      feature = DisplayText.DRAFT_SUPPORT;
      featureAvailableInPlan = DisplayText.DRAFT_SUPPORT_PLAN_MSG;
      image = draftImage;
      featureDescription = DisplayText.DRAFT_SUPPORT_DESC;
      break;
    case 'advancedPost':
      feature = DisplayText.ADV_POST_SUPPORT;
      featureAvailableInPlan = DisplayText.ADV_POST_SUPPORT_MSG;
      image = advancedPost;
      featureDescription = DisplayText.ADV_POST_SUPPORT_DESC;
      break;
    case 'pdfDownload':
      headerText = DisplayText.DOWNLOAD_SAMPLE_REPORT_HEADER;
      feature = DisplayText.DOWNLOAD_SAMPLE_REPORT;
      featureAvailableInPlan = DisplayText.DOWNLOAD_SAMPLE_REPORT_MSG;
      image = pdfDownload;
      featureDescription = DisplayText.DOWNLOAD_SAMPLE_REPORT_DESC;
      break;
    case 'email-pdf':
      feature = '';
      headerText = DisplayText.EMAIL_PDF_HEADER;
      image = pdfDownload;
      featureAvailableInPlan = DisplayText.EMAIL_PDF_MSG;
      featureDescription = DisplayText.DOWNLOAD_SAMPLE_REPORT_DESC;
      break;
    case 'boostpost':
      feature = DisplayText.BOOST_POST_SUPPORT;
      featureAvailableInPlan = DisplayText.BOOST_POST_SUPPORT_MSG;
      image = boostpost;
      featureDescription = DisplayText.BOOST_POST_SUPPORT_DESC;
      break;
    default:
      break;
  }

  return {
    headerText,
    feature,
    featureAvailableInPlan,
    image,
    featureDescription,
  };
};

export default upgradeMessage;

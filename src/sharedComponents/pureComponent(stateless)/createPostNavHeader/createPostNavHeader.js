import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import DisplayText from '../../lables/index';

import './createPostNavHeader.scss';

const CreatePostNavHeader = ({ children }) => (
  <div className={`row  mt-2 `}>
    <div className="col">
      <div className="manage-post-tab ">
        <div className="manage-post-options tableborder-bottom tableborder-top pt-1">
          <ul>
            <li>
              <NavLink activeClassName="active" to="/posts/create">
                <FormattedMessage
                  id={DisplayText.CREATE_POST}
                  defaultMessage={DisplayText.CREATE_POST}
                />
              </NavLink>
            </li>
            <li>
              <NavLink activeClassName="active" to="/posts/draft">
                <FormattedMessage
                  id={DisplayText.DRAFTS}
                  defaultMessage={DisplayText.DRAFTS}
                />
              </NavLink>
            </li>
            <li>
              <NavLink activeClassName="active" to="/posts/suggestion">
                <FormattedMessage
                  id={DisplayText.CURATED_CONTENT}
                  defaultMessage={DisplayText.CURATED_CONTENT}
                />
              </NavLink>
            </li>
            <li>
              <NavLink activeClassName="active" to="/posts/articles">
                <FormattedMessage
                  id={DisplayText.FEED_CONTENT}
                  defaultMessage={DisplayText.FEED_CONTENT}
                />
              </NavLink>
            </li>
          </ul>
          <div className="clear" />
        </div>
      </div>
    </div>
    {children}
  </div>
);

CreatePostNavHeader.propTypes = {
  children: PropTypes.node,
};

CreatePostNavHeader.defaultProps = {
  children: null,
};

export default CreatePostNavHeader;

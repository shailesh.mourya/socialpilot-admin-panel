/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */

import React from 'react';
import { PropTypes } from 'prop-types';
import './splitButton.scss';
import { FormattedMessage } from 'react-intl';
import Button from '../Buttons/Button';
// pass attribute type="splitSide" if older spilt button is required remove main label form dropdown object

const SplitButton = ({
  mainButton,
  dropDownButton,
  clickHandler,
  className,
  btnType,
  style,
  type,
  disabled,
  fromAccount,
}) => (
  <div className={`btn-group splitButton ${className}`}>
    {type === 'splitSide' ? (
      <Button
        type="button"
        bsClass={`btn post-common-btn ${btnType}`}
        onClick={() => {
          clickHandler(mainButton.value);
        }}
        disabled={disabled}
      >
        <FormattedMessage
          id={mainButton.name}
          defaultMessage={mainButton.name}
        />
      </Button>
    ) : (
      ''
    )}

    <Button
      type="button"
      bsClass={`btn ${btnType} dropdown-toggle dropdown-toggle-split post-common-btn ${
        fromAccount ? '' : 'ml-2'
      }`}
      dataToggle="dropdown"
      ariaHaspopup="true"
      ariaExpanded="false"
      disabled={disabled}
    >
      {type !== 'splitSide' ? (
        <FormattedMessage
          id={mainButton.name}
          defaultMessage={mainButton.name}
        />
      ) : (
        ''
      )}
      <i className="fas fa-chevron-down" aria-hidden="true" />
    </Button>
    <div className="dropdown-menu">
      {dropDownButton.map(item => {
        let classParam = '';
        if (item.value === 'email-pdf') {
          classParam = 'email-pdf';
        } else if (item.value === 'download-pdf') {
          classParam = 'fb-download-pdf';
        }
        return (
          <a
            key={item.value}
            className={`dropdown-item ${classParam}`}
            href="#"
            style={style}
            onClick={e => {
              e.preventDefault();
              clickHandler(item.value);
            }}
          >
            <FormattedMessage id={item.name} defaultMessage={item.name} />
          </a>
        );
      })}
    </div>
  </div>
);
SplitButton.defaultProps = {
  btnType: 'btn-primary',
  className: '',
  style: {},
  type: '',
  disabled: false,
  fromAccount: false,
};
SplitButton.propTypes = {
  btnType: PropTypes.string,
  className: PropTypes.string,
  mainButton: PropTypes.objectOf(PropTypes.string).isRequired,
  dropDownButton: PropTypes.arrayOf(PropTypes.object).isRequired,
  clickHandler: PropTypes.func.isRequired,
  style: PropTypes.oneOfType([PropTypes.object]),
  type: PropTypes.string,
  disabled: PropTypes.bool,
  fromAccount: PropTypes.bool,
};
export default SplitButton;

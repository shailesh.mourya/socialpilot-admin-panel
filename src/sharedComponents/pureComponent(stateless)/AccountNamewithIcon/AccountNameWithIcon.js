import React from 'react';
import PropTypes from 'prop-types';
import UserIcon from '../UserIcon/UserIcon';

const AccountNameWithIcon = ({
  accountUsername,
  accountType,
  profilePicture,
  accountUrl,
}) => (
  <div className="d-flex align-items-center">
    <UserIcon
      user_name={accountUsername}
      profile_type={accountType}
      user_img_src={profilePicture}
    />
    <div className={`account_user_name account-name-desc `}>
      {`${accountUsername}  `}

      <a href={accountUrl} target="blank">
        <i className="fas fa-external-link-alt" />
      </a>
    </div>
  </div>
);

AccountNameWithIcon.propTypes = {
  accountType: PropTypes.string.isRequired,
  accountUrl: PropTypes.string.isRequired,
  accountUsername: PropTypes.string.isRequired,
  profilePicture: PropTypes.string.isRequired,
};

export default AccountNameWithIcon;

import React from 'react';
import { FormattedMessage } from 'react-intl';
import PropTypes from 'prop-types';
import './Header.scss';

const HeaderComponent = ({ header, subheader, children }) => (
  <div className="row heading-area">
    <div className="col">
      <h1>
        <FormattedMessage id={header} defaultMessage={header} />
      </h1>

      {subheader && (
        <p>
          <FormattedMessage id={subheader} defaultMessage={subheader} />
        </p>
      )}
    </div>
    {children ? (
      <div className="col-auto my-auto form-inline">{children}</div>
    ) : null}
  </div>
);

HeaderComponent.propTypes = {
  header: PropTypes.string,
  subheader: PropTypes.string,
  children: PropTypes.node,
};
HeaderComponent.defaultProps = {
  header: 'header',
  children: null,
  subheader: '',
};

export default HeaderComponent;

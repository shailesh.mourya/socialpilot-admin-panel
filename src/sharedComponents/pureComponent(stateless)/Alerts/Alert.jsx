import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Button from '../Buttons/Button';
import DisplayText from '../../lables/index';

export class AlertMsg extends PureComponent {
  render() {
    const {
      type,
      msg,
      StrongMsg,
      onClick,
      disableClose,
      children,
    } = this.props;

    return (
      <div
        className={`alert alert-${type} alert-dismissible fade show`}
        role="alert"
      >
        {StrongMsg ? (
          <strong>
            <FormattedMessage id={StrongMsg} defaultMessage={StrongMsg} />
          </strong>
        ) : null}
        {msg ? (
          <FormattedMessage
            id={msg || DisplayText.NO_MSG}
            defaultMessage={msg || DisplayText.NO_MSG}
          />
        ) : null}
        {children || null}
        {disableClose ? null : (
          <Button
            type="button"
            bsClass="close"
            dataDismiss="alert"
            ariaLabel="Close"
            onClick={onClick}
          >
            <span aria-hidden="true">×</span>
          </Button>
        )}
      </div>
    );
  }
}

AlertMsg.propTypes = {
  msg: PropTypes.string,
  type: PropTypes.string,
  StrongMsg: PropTypes.string,
  onClick: PropTypes.func,
  disableClose: PropTypes.bool,
  children: PropTypes.node,
};

AlertMsg.defaultProps = {
  msg: '',
  StrongMsg: '',
  type: 'warning',
  onClick: () => {},
  disableClose: false,
  children: null,
};

export default AlertMsg;

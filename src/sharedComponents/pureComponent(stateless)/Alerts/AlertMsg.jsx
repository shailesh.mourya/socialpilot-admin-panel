import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

const AlertMsg = ({ type, msg }) => (
  <div className={type}>
    {msg ? <FormattedMessage id={msg} defaultMessage={msg} /> : <span />}
  </div>
);

AlertMsg.propTypes = {
  msg: PropTypes.string,
  type: PropTypes.oneOf(['text-danger', 'success']).isRequired,
};

AlertMsg.defaultProps = {
  msg: '',
};

export default AlertMsg;

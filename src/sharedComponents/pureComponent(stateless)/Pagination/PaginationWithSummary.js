import React from 'react';
import { FormattedMessage } from 'react-intl';
import Pagination from 'react-js-pagination';
import PropTypes from 'prop-types';
import DisplayText from '../../lables/index';

const PaginationComponent = ({
  handlePage,
  currentPage,
  pageLimit,
  totalRecords,
  totalPages,
  isLoading,
}) => {
  const getPageRange = () => {
    if (currentPage === 1) {
      return `1 - ${totalRecords > pageLimit ? pageLimit : totalRecords}`;
    }
    if (currentPage === totalPages) {
      return `${currentPage * pageLimit - (pageLimit - 1)} - ${totalRecords}`;
    }
    return `${currentPage * pageLimit - (pageLimit - 1)} - ${currentPage *
      pageLimit}`;
  };
  if (totalPages <= 1) {
    return null;
  }

  return (
    <>
      <div className="row">
        <div className="col">
          <nav aria-label="Page navigation ">
            <Pagination
              activePage={currentPage}
              itemsCountPerPage={pageLimit}
              totalItemsCount={totalRecords}
              pageRangeDisplayed={10}
              lastPageText="»"
              firstPageText="«"
              prevPageText="‹"
              nextPageText="›"
              onChange={pageNo => {
                handlePage(pageNo);
              }}
              itemClass="page-item"
              linkClass="page-link"
              hideFirstLastPages={totalPages <= 10}
            />
            {isLoading && (
              <i
                className="fas fa-spinner fa-spin feed_content-spinner"
                aria-hidden="true"
              />
            )}
          </nav>
        </div>
        <div className="col my-auto">
          <div className="summary text-right ">
            <FormattedMessage
              id={DisplayText.SHOWING}
              defaultMessage={DisplayText.SHOWING}
            />{' '}
            <b>{getPageRange()}</b>
            {'  '}
            <FormattedMessage
              id={DisplayText.OF}
              defaultMessage={DisplayText.OF}
            />{' '}
            <b>{totalRecords}</b>{' '}
            <FormattedMessage
              id={DisplayText.ITEMS}
              defaultMessage={DisplayText.ITEMS}
            />
          </div>
        </div>
      </div>
    </>
  );
};

PaginationComponent.propTypes = {
  currentPage: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired,
  handlePage: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  pageLimit: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired,
  totalPages: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired,
  totalRecords: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
    .isRequired,
};

PaginationComponent.defaultProps = {
  isLoading: false,
};

export default PaginationComponent;

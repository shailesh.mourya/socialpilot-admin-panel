/* eslint-disable camelcase */
// i have disabled this because every one was using this kind of props it will be harm for old component's
import React from 'react';
import PropTypes from 'prop-types';
import getAccountIcon from '../../../utils/functions/getAccountIcon';
import socialMediaAccount from '../../../assets/social_media_account.png';
import './UserIcon.scss';

const getProfileIcon = profile_type => (
  <i className={getAccountIcon(profile_type)} />
);

const UserIcon = ({ user_img_src, user_name, profile_type, withPic }) => {
  if (!withPic) {
    return (
      <div className="onlyicon float-left hidden-sm ">
        {getProfileIcon(profile_type)}
      </div>
    );
  }

  if (user_img_src === '') {
    return (
      <div className="post-user-icon float-left hidden-sm ">
        <span className="account-first-alphabate">
          {user_name.charAt(0).toUpperCase()}
        </span>
        {getProfileIcon(profile_type)}
      </div>
    );
  }

  return (
    <div className="post-user-icon  float-left  hidden-sm">
      <img
        src={user_img_src}
        className="img-circle hidden-sm"
        alt="user_avatar"
        width="35px"
        height="35px"
        onError={e => {
          e.target.src = socialMediaAccount;
        }}
      />
      {getProfileIcon(profile_type)}
    </div>
  );
};

UserIcon.propTypes = {
  user_name: PropTypes.string,
  user_img_src: PropTypes.string,
  profile_type: PropTypes.string,
  withPic: PropTypes.bool,
};

UserIcon.defaultProps = {
  user_img_src: '',
  user_name: '',
  profile_type: '',
  withPic: false,
};
export default React.memo(UserIcon);

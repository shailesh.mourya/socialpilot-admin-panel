const DisplayText = {
  // STATEFULL COMPONENTS

  AD_ACCOUNTS: 'Ad Accounts',
  FB_PAGES: 'Facebook Pages',
  SELECT_ACCOUNT: 'Select Account',
  SEARCH: 'Search',
  SEARCH_GRP: 'Search Group',
  SEARCH_CLIENT: 'Search Client',
  BY_AD_ACCOUNTS: 'By Ad Accounts',
  BY_FB_PAGES: 'By Facebook Pages',
  FILTER_RECORDS: 'Filter Records',
  DONE: 'Done',
  BY_GROUPS: 'By Groups',
  BY_CLIENTS: 'By Clients',
  BY_ACCOUNTS: 'By Accounts',
  YOUR_LOGO: 'Your Logo',
  CREATE_GROUP: 'Create Group',
  NO_DATA_FOUND: 'No Data Found',
  INVITE_CLIENT: 'Invite Client',
  CONTACT_US: 'Contact Us',
  UPGRADE_NOW: 'Upgrade Now',
  BOOST_POST_NOT_AVAILABLE: 'Boost Post not available for this account',
  CANT_FIND_ACCOUNT: 'Couldn’t find this account.',
  POSTS: 'Posts',
  LIST: 'List',
  CALENDAR: 'Calendar',
  NO_MSG: 'no mesage',
  RECONNECT: 'Reconnect',
  GET_REPORTS: 'Get Reports',
  SEND_REPORT: 'Send Report',
  EMAIL_SENT: 'Email Sent',
  CLEAR_SELECTION: `Clear Selection.`,
  CURATED_CONTENT: 'Curated Content',
  DRAFTS: 'Drafts',
  CREATE_POST: 'Create Post',
  FEED_CONTENT: 'Feed Content',
  MALE: 'Male',
  FEMALE: 'Female',
  SHOWING: 'Showing',
  OF: 'of',
  ITEMS: 'items',
  EDIT_POST: 'Edit Post',
  RESHARE_POST: 'Reshare Post',
  EDIT_DRAFT_POST: 'Edit Draft Post',
  BACK_TO_DASHBOARD: 'Back To Dashboard',
  NOT_AUTHORIZED: 'You are not authorized to access this page.',
  PAGE_DOESNT_EXIST: `The page you are looking for doesn't exist.`,
  EMAIL_SENT_TO: 'Email with PDF link successfully sent to',
  ENTER_EMAIL_TO_GET_REPORTS:
    'Enter your email address to get reports straight to your inbox',
  EMAIL_SEND_ERROR: 'Oops!! Something went wrong while sending the Email',
  TRY_AGAIN: 'Try Again!',
  OOPS_SOMETHING_WENT_WRONG: 'Oops! Something Went Wrong',
  REQUEST_COMPLETE_ERROR:
    "We're Sorry. We have Trouble Completing your Request. Please try again.",
  MULTI_IMG_POSTING: 'Multiple Image Postings',
  MULTI_IMG_PLAN_MSG:
    'Upgrade to any of our paid plans to access this premium feature of sharing multiple images in a single post',
  MULTI_IMG_DESC:
    'Multiple Image Posting lets you share upto 4 images in one single post.',
  GIF_SUPPORT: 'GIF Support',
  GIF_SUPPORT_PLAN_MSG:
    'Upgrade to our premium plans (Professional, Small Team, Agency) to schedule and share GIFs’ to your multiple social media profiles.',
  GIF_SUPPORT_DESC:
    'You can add and schedule amazing GIFs’ to your posts to make them more engaging.',
  DROPBOX_SUPPORT: 'Dropbox Support',
  DROPBOX_SUPPORT_PLAN_MSG:
    'Upgrade to our premium plans (Small Team, Agency) to schedule and share Dropbox images to your multiple social media profiles.',
  DROPBOX_SUPPORT_DESC:
    'Make your image sharing much easier by adding and scheduling images right from your Dropbox.',
  DRAFT_SUPPORT: 'Draft Posts Support',
  DRAFT_SUPPORT_PLAN_MSG:
    'Upgrade to our premium plans ( Professional, Small Team, Agency) to save your posts as Drafts and schedule/publish it later.',
  DRAFT_SUPPORT_DESC:
    'You can add and save number of posts as Drafts and use it later as and when needed.',
  ADV_POST_SUPPORT: 'Advanced Post Support',
  ADV_POST_SUPPORT_MSG:
    'Upgrade to our premium plans ( Professional, Small Team, Agency) to create posts for different accounts at a time.',
  ADV_POST_SUPPORT_DESC:
    'You can create different posts for different type of accounts.',
  DOWNLOAD_SAMPLE_REPORT: 'Download Sample Report',
  DOWNLOAD_SAMPLE_REPORT_HEADER:
    'Download Beautiful reports with your own branding',
  DOWNLOAD_SAMPLE_REPORT_MSG:
    'Upgrade to our premium plans (Small Team, Agency) to download unlimited branded PDF reports to share with your clients and team.',
  DOWNLOAD_SAMPLE_REPORT_DESC:
    'No need to waste extra time and efforts in designing reports separately',
  EMAIL_PDF_HEADER: 'Email Beautiful Reports with your own Branding',
  EMAIL_PDF_MSG:
    'Upgrade to our premium plans (Small Team, Agency) to email branded PDF reports directly to your clients and team members.',
  BOOST_POST_SUPPORT: 'Boost Post Support',
  BOOST_POST_SUPPORT_MSG:
    'Upgrade to our premium plans (Professional, Small Team & Agency) to effortlessly boost your Facebook posts with SocialPilot.',
  BOOST_POST_SUPPORT_DESC:
    'Promote your Facebook posts easily with SocialPilot’s boost post support.',
  SORRY_CANT_USE_FEATURE: 'Sorry! You cannot use this feature.',

  BOX_SUPPORT: 'Box Support',
  BOX_SUPPORT_PLAN_MSG:
    'Upgrade to our premium plans (Small Team, Agency) to schedule and share Box images to your multiple social media profiles.',
  BOX_SUPPORT_DESC:
    'Make your image sharing much easier by adding and scheduling images right from your Box.',
};

export default DisplayText;

import englishText from '../index';

const spanish = {
  [englishText.UPGRADE_NOW]: 'Actualiza Ahora Mismo',
  [englishText.ITEMS]: 'Artículos',
  [englishText.DRAFTS]: 'Borradores',
  [englishText.SEARCH]: 'Buscar',
  [englishText.SEARCH_CLIENT]: 'Buscar cliente',
  [englishText.SEARCH_GRP]: 'Buscar grupo',
  [englishText.CALENDAR]: 'Calendario',
  [englishText.CONTACT_US]: 'Cont\u00e1ctanos',
  [englishText.FEED_CONTENT]: 'Contenido de canal',
  [englishText.CURATED_CONTENT]: 'Contenido sugerido',
  [englishText.EMAIL_SENT]: 'Correo electrónico enviado',
  [englishText.CREATE_GROUP]: 'Crear grupo',
  [englishText.CREATE_POST]: 'Crear publicación',
  [englishText.AD_ACCOUNTS]: 'Cuentas de aviso',
  [englishText.NO_DATA_FOUND]: 'DATOS NO ENCONTRADOS',
  [englishText.OF]: 'de',
  [englishText.EDIT_DRAFT_POST]: 'Editar borrador de publicación',
  [englishText.EDIT_POST]: 'Editar un Post',
  [englishText.FB_PAGES]: 'Facebook p\u00e1ginas',
  [englishText.INVITE_CLIENT]: 'Invitar cliente',
  [englishText.LIST]: 'Lista',
  [englishText.DONE]: 'Listo',
  [englishText.SHOWING]: 'Mostrado',
  [englishText.CANT_FIND_ACCOUNT]: 'No se pudo encontrar esta cuenta',
  [englishText.BY_GROUPS]: 'Por Grupos',
  [englishText.POSTS]: 'Publicaciones',
  [englishText.SELECT_ACCOUNT]: 'Seleccionar cuenta',
  [englishText.DROPBOX_SUPPORT]: 'Suporte Dropbox',
  [englishText.YOUR_LOGO]: 'Tu logo',
  [englishText.RESHARE_POST]: 'Volver a Compartir Entrada',
  [englishText.RECONNECT]: 'Volver a conectar',
  [englishText.BY_AD_ACCOUNTS]: 'Por cuentas de anuncios',
  [englishText.BY_FB_PAGES]: 'Por páginas de Facebook',
  [englishText.FILTER_RECORDS]: 'Filtrar registros',
  [englishText.BY_CLIENTS]: 'Por clientes',
  [englishText.BY_ACCOUNTS]: 'Por cuentas',
  [englishText.BOOST_POST_NOT_AVAILABLE]:
    'Impulsar publicación no disponible para esta cuenta',
  [englishText.NO_MSG]: 'No hay mensaje',
  [englishText.GET_REPORTS]: 'Obtener reportes',
  [englishText.SEND_REPORT]: 'Enviar reporte',
  [englishText.CLEAR_SELECTION]: 'Limpiar selección',
  [englishText.MALE]: 'Hombre',
  [englishText.FEMALE]: 'Mujer',
  [englishText.BACK_TO_DASHBOARD]: 'Regresar al tablero',
  [englishText.NOT_AUTHORIZED]:
    'No estás autorizado para acceder a esta página.',
  [englishText.PAGE_DOESNT_EXIST]: 'La página que estás buscando no existe.',
  [englishText.EMAIL_SENT_TO]:
    'Correo electrónico con enlace de PDF enviado con éxito a',
  [englishText.ENTER_EMAIL_TO_GET_REPORTS]:
    'Ingresa tu dirección de correo electrónico para obtener los reportes directo en tu bandeja de entrada',
  [englishText.EMAIL_SEND_ERROR]:
    '¡¡Oops!! Algo salió mal durante el envío del correo electrónico',
  [englishText.TRY_AGAIN]: '¡Inténtalo de nuevo!',
  [englishText.OOPS_SOMETHING_WENT_WRONG]: '¡Oops! Algo salió mal',
  [englishText.REQUEST_COMPLETE_ERROR]:
    'Lo sentimos. Tenemos problemas para completar su solicitud. Por favor inténtalo de nuevo.',
  [englishText.MULTI_IMG_POSTING]: 'Publicaciones de varias imágenes ',
  [englishText.MULTI_IMG_PLAN_MSG]:
    'Mejora a cualquiera de nuestros planes pagados para acceder a esta característica premium de compartir varias imágenes en una sola publicación',
  [englishText.MULTI_IMG_DESC]:
    'La publicación de varias imágenes te permite compartir hasta 4 imágenes en una sola publicación.',
  [englishText.GIF_SUPPORT]: 'Soporte de GIF',
  [englishText.GIF_SUPPORT_PLAN_MSG]:
    'Mejora a nuestros planes premium (Professional, Small Team, Agency) para programar y compartir GIFS a todos tus perfiles de redes sociales.',
  [englishText.GIF_SUPPORT_DESC]:
    'Puedes añadir y programar GIFS increíbles para hacer que tus publicaciones sean más atractivas.',
  [englishText.DROPBOX_SUPPORT_PLAN_MSG]:
    'Mejora a nuestros planes premium (Small Team, Agency) para programar y compartir imágenes de Dropbox a todos tus perfiles de redes sociales.',
  [englishText.DROPBOX_SUPPORT_DESC]:
    'Haz que el compartir de imágenes sea mucho más sencillo añadiendo y programando imágenes directo desde tu Dropbox.',
  [englishText.DRAFT_SUPPORT]: 'Soporte para publicaciones de borrador',
  [englishText.DRAFT_SUPPORT_PLAN_MSG]:
    'Mejora a nuestros planes premium (Professional, Small Team, Agency) para guardar tus publicaciones como borradores y programar/publicarlos luego.',
  [englishText.DRAFT_SUPPORT_DESC]:
    'Puedes añadir y guardar un número de publicaciones como borrador y usarlos luego cuando sean necesarios.',
  [englishText.ADV_POST_SUPPORT]: 'Soporte de publicación avanzado',
  [englishText.ADV_POST_SUPPORT_MSG]:
    'Mejora a nuestros planes premium (Professional, Small Team, Agency) para crear publicaciones para cuentas diferentes al mismo tiempo.',
  [englishText.ADV_POST_SUPPORT_DESC]:
    'Puedes crear publicaciones diferentes para diferentes tipos de cuentas.',
  [englishText.DOWNLOAD_SAMPLE_REPORT]: 'Descargar reporte de muestra',
  [englishText.DOWNLOAD_SAMPLE_REPORT_HEADER]:
    'Descarga reportes hermosos con tu propia marca',
  [englishText.DOWNLOAD_SAMPLE_REPORT_MSG]:
    'Mejora a nuestros planes premium (Small Team, Agency) para descargar reportes PDF con marca ilimitados para compartirlos con tus clientes y tu equipo.',
  [englishText.DOWNLOAD_SAMPLE_REPORT_DESC]:
    'No es necesario desperdiciar tiempo y esfuerzo extra en diseñar los reportes por separado',
  [englishText.EMAIL_PDF_HEADER]:
    'Envía reportes hermosos por correo electrónico con tu propia marca',
  [englishText.EMAIL_PDF_MSG]:
    'Mejora a nuestros planes premium (Small Team, Agency) para enviar por correo electrónico reportes PDF con marca directamente a tus clientes y miembros del equipo.',
  [englishText.BOOST_POST_SUPPORT]: 'Soporte de impulso de publicaciones',
  [englishText.BOOST_POST_SUPPORT_MSG]:
    'Mejora a nuestros planes premium (Professional, Small Team, Agency) para impulsar sin esfuerzo tus publicaciones de Facebook con SocialPilot.',
  [englishText.BOOST_POST_SUPPORT_DESC]:
    'Promociona tus publicaciones de Facebook fácilmente con el apoyo del impulso de publicación de SocialPilot.',
  [englishText.SORRY_CANT_USE_FEATURE]:
    '¡Lo sentimos! No puedes usar esta característica',
};

export default spanish;

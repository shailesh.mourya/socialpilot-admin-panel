import englishText from '../index';

const german = {
  [englishText.SHOWING]: 'Anzeigen',
  [englishText.AD_ACCOUNTS]: 'Anzeigenkontos',
  [englishText.ITEMS]: 'Artikel',
  [englishText.EDIT_POST]: 'Beitrag bearbeiten',
  [englishText.RESHARE_POST]: 'Beitrag erneut teilen',
  [englishText.CREATE_POST]: 'Beitrag erstellen',
  [englishText.POSTS]: 'Beiträge',
  [englishText.CANT_FIND_ACCOUNT]: 'Dieses Konto konnte nicht gefunden werden.',
  [englishText.EDIT_DRAFT_POST]: 'Draft bearbeiten',
  [englishText.DROPBOX_SUPPORT]: 'Dropbox-Unterst\u00fctzung',
  [englishText.DRAFTS]: 'Entw\u00fcrfe',
  [englishText.DONE]: 'Erledigt',
  [englishText.RECONNECT]: 'Erneut verbinden',
  [englishText.FB_PAGES]: 'Facebook Seiten',
  [englishText.FEED_CONTENT]: 'Feed Inhalt',
  [englishText.EMAIL_SENT]: 'Gesendete E-Mail',
  [englishText.BY_GROUPS]: 'Gruppe By',
  [englishText.CREATE_GROUP]: 'Gruppe erstellen',
  [englishText.SEARCH_GRP]: 'Gruppe suchen',
  [englishText.YOUR_LOGO]: 'Ihr Logo',
  [englishText.UPGRADE_NOW]: 'Jetzt upgraden',
  [englishText.CALENDAR]: 'Kalender',
  [englishText.NO_DATA_FOUND]: 'KEINE DATEN GEFUNDEN',
  [englishText.CONTACT_US]: 'Kontaktiere uns',
  [englishText.SELECT_ACCOUNT]: 'Konto auswählen',
  [englishText.BY_CLIENTS]: 'Kunde',
  [englishText.INVITE_CLIENT]: 'Kunden einladen',
  [englishText.SEARCH_CLIENT]: 'Kunden suchen',
  [englishText.LIST]: 'Liste',
  [englishText.SEARCH]: 'Suche',
  [englishText.OF]: 'von',
  [englishText.CURATED_CONTENT]: 'Vorgeschlagener Inhalt',
  [englishText.BY_AD_ACCOUNTS]: 'Nach Werbekonten',
  [englishText.BY_FB_PAGES]: 'Nach Facebook-Seiten',
  [englishText.FILTER_RECORDS]: 'Datensätze filtern',
  [englishText.BY_ACCOUNTS]: 'Nach Konten',
  [englishText.BOOST_POST_NOT_AVAILABLE]:
    'Boost Post nicht verfügbar für dieses Konto',
  [englishText.NO_MSG]: 'Keine Nachricht',
  [englishText.GET_REPORTS]: 'Berichte erhalten',
  [englishText.SEND_REPORT]: 'Bericht senden',
  [englishText.CLEAR_SELECTION]: 'Auswahl löschen.',
  [englishText.MALE]: 'Männlich',
  [englishText.FEMALE]: 'Weiblich',
  [englishText.BACK_TO_DASHBOARD]: 'Zurück zum Dashboard',
  [englishText.NOT_AUTHORIZED]:
    'Sie sind nicht berechtigt, auf diese Seite zuzugreifen.',
  [englishText.PAGE_DOESNT_EXIST]:
    'Die von Ihnen gesuchte Seite existiert nicht.',
  [englishText.EMAIL_SENT_TO]: 'E-Mail mit PDF-Link erfolgreich gesendet an',
  [englishText.ENTER_EMAIL_TO_GET_REPORTS]:
    'Geben Sie Ihre E-Mail-Adresse ein, um Berichte direkt in Ihren Posteingang zu erhalten',
  [englishText.EMAIL_SEND_ERROR]:
    'Hoppla!! Etwas ist beim Versenden der Email schiefgegangen',
  [englishText.TRY_AGAIN]: "Versuch's noch mal!",
  [englishText.OOPS_SOMETHING_WENT_WRONG]: 'Hoppla! Etwas ist schiefgegangen.',
  [englishText.REQUEST_COMPLETE_ERROR]:
    'Es tut uns leid. Wir haben Probleme, Ihre Anfrage zu vervollständigen. Bitte versuchen Sie es erneut.',
  [englishText.MULTI_IMG_POSTING]: 'Mehrere Bild-Postings',
  [englishText.MULTI_IMG_PLAN_MSG]:
    'Aktualisieren Sie auf einen unserer kostenpflichtigen Tarife, um auf diese Premium-Funktion zuzugreifen, mit der Sie mehrere Bilder in einem einzigen Post veröffentlichen können.',
  [englishText.MULTI_IMG_DESC]:
    'Mit Multiple Image Posting können Sie bis zu 4 Bilder in einem einzigen Beitrag veröffentlichen.',
  [englishText.GIF_SUPPORT]: 'GIF-Unterstützung',
  [englishText.GIF_SUPPORT_PLAN_MSG]:
    'Upgraden Sie zu unseren Premium-Paketen (Professional, Small Team, Agentur), um GIFs für Ihre verschiedenen Social Media-Profile zu planen und zu teilen.',
  [englishText.GIF_SUPPORT_DESC]:
    'Sie können erstaunliche GIFs zu Ihren Beiträgen hinzufügen und planen, um sie noch interessanter zu machen.',
  [englishText.DROPBOX_SUPPORT_PLAN_MSG]:
    'Upgraden Sie zu unseren Premium-Paketen (Small Team, Agentur), um Dropbox-Bilder zu planen und für Ihre verschiedenen Social Media-Profile zu verwenden.',
  [englishText.DROPBOX_SUPPORT_DESC]:
    'Machen Sie Ihre Bildfreigabe viel einfacher, indem Sie Bilder direkt von Ihrer Dropbox aus hinzufügen und planen.',
  [englishText.DRAFT_SUPPORT]: 'Draft Posts Unterstützung',
  [englishText.DRAFT_SUPPORT_PLAN_MSG]:
    'Upgraden Sie zu unseren Premium-Plänen (Professional, Small Team, Agentur), um Ihre Posts als Entwürfe zu speichern und später zu planen/veröffentlichen.',
  [englishText.DRAFT_SUPPORT_DESC]:
    'Sie können die Anzahl der Posts als Entwürfe speichern und später bei Bedarf wieder verwenden.',
  [englishText.ADV_POST_SUPPORT]: 'Erweiterter Post-Support',
  [englishText.ADV_POST_SUPPORT_MSG]:
    'Upgraden Sie zu unseren Premium-Plänen (Professional, Small Team, Agentur), um gleichzeitig Posts für verschiedene Accounts zu erstellen.',
  [englishText.ADV_POST_SUPPORT_DESC]:
    'Sie können verschiedene Beiträge für verschiedene Arten von Accounts erstellen.',
  [englishText.DOWNLOAD_SAMPLE_REPORT]: 'Musterbericht herunterladen',
  [englishText.DOWNLOAD_SAMPLE_REPORT_HEADER]:
    'Downloaden Sie Schöne Berichte mit Ihrem eigenen Branding',
  [englishText.DOWNLOAD_SAMPLE_REPORT_MSG]:
    'Mit einem Upgrade auf unsere Premium-Pläne (Small Team, Agentur) können Sie unbegrenzt viele gebrandete PDF-Berichte herunterladen, die Sie mit Ihren Kunden und Ihrem Team teilen können.',
  [englishText.DOWNLOAD_SAMPLE_REPORT_DESC]:
    'Keine Notwendigkeit, zusätzliche Zeit und Mühe für die separate Erstellung von Berichten zu verschwenden.',
  [englishText.EMAIL_PDF_HEADER]:
    'Schöne Berichte mit eigenem Branding per E-Mail versenden',
  [englishText.EMAIL_PDF_MSG]:
    'Mit einem Upgrade auf unsere Premium-Pakete (Small Team, Agentur) können Sie gebrandete PDF-Berichte direkt per E-Mail an Ihre Kunden und Teammitglieder senden.',
  [englishText.BOOST_POST_SUPPORT]: 'Booste Post-Support',
  [englishText.BOOST_POST_SUPPORT_MSG]:
    'Mit einem Upgrade auf unsere Premium-Pläne (Professional, Small Team & Agentur) können Sie Ihre Facebook-Posts mit SocialPilot mühelos boosten.',
  [englishText.BOOST_POST_SUPPORT_DESC]:
    'Bewerben Sie Ihre Facebook-Posts ganz einfach mit der SocialPilot-Boost-Post-Unterstützung.',
  [englishText.SORRY_CANT_USE_FEATURE]:
    'Entschuldigung! Sie können diese Funktion nicht nutzen.',
};

export default german;

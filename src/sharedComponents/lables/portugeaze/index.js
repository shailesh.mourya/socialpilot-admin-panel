import englishText from '../index';

const portugeaze = {
  [englishText.SHOWING]: 'Mostrando',
  [englishText.AD_ACCOUNTS]: 'Contas de anúncios',
  [englishText.ITEMS]: 'items',
  [englishText.EDIT_POST]: 'Editar post',
  [englishText.RESHARE_POST]: 'Partilha de novo o post',
  [englishText.CREATE_POST]: 'Criar post',
  [englishText.POSTS]: 'Posts',
  [englishText.CANT_FIND_ACCOUNT]: 'Não foi possível encontrar esta conta.',
  [englishText.EDIT_DRAFT_POST]: 'Editar draft de post',
  [englishText.DROPBOX_SUPPORT]: 'Suporte Dropbox',
  [englishText.DRAFTS]: 'Drafts',
  [englishText.DONE]: 'Feito',
  [englishText.RECONNECT]: 'Reconectar',
  [englishText.FB_PAGES]: 'Páginas Facebook',
  [englishText.FEED_CONTENT]: 'Conteúdo do Feed',
  [englishText.EMAIL_SENT]: 'Email enviado',
  [englishText.BY_GROUPS]: 'Por grupos',
  [englishText.CREATE_GROUP]: 'Criar Grupo',
  [englishText.SEARCH_GRP]: 'Pesquisar Group',
  [englishText.YOUR_LOGO]: 'Seu logotipo',
  [englishText.UPGRADE_NOW]: 'Atualize Agora',
  [englishText.CALENDAR]: 'Calendário',
  [englishText.NO_DATA_FOUND]: 'Nenhum dado encontrado',
  [englishText.CONTACT_US]: 'Contate-nos',
  [englishText.SELECT_ACCOUNT]: 'Selecione uma conta',
  [englishText.BY_CLIENTS]: 'Por clientes',
  [englishText.INVITE_CLIENT]: 'Convidar Cliente',
  [englishText.SEARCH_CLIENT]: 'Pesquisar Cliente',
  [englishText.LIST]: 'Lista',
  [englishText.SEARCH]: 'Procurar',
  [englishText.OF]: 'do',
  [englishText.CURATED_CONTENT]: 'Conteúdo Tratado',
  [englishText.BY_AD_ACCOUNTS]: 'Por Contas de Anúncio',
  [englishText.BY_FB_PAGES]: 'Por Páginas do Facebook',
  [englishText.FILTER_RECORDS]: 'Filtrar Registros',
  [englishText.BY_ACCOUNTS]: 'Por Contas',
  [englishText.BOOST_POST_NOT_AVAILABLE]:
    'Impulsionamento de Post não está disponível para esta conta',
  [englishText.NO_MSG]: 'nenhuma mensagem',
  [englishText.GET_REPORTS]: 'Receber Relatórios',
  [englishText.SEND_REPORT]: 'Enviar Relatório',
  [englishText.CLEAR_SELECTION]: 'Limpar Seleção.',
  [englishText.MALE]: 'Masculino',
  [englishText.FEMALE]: 'Feminino',
  [englishText.BACK_TO_DASHBOARD]: 'Voltar para o Painel de Controle',
  [englishText.NOT_AUTHORIZED]:
    'Você não está autorizado a acessar esta página.',
  [englishText.PAGE_DOESNT_EXIST]:
    'A página que você está procurando não existe',
  [englishText.EMAIL_SENT_TO]:
    'Email com o link em PDF foi enviado com sucesso para',
  [englishText.ENTER_EMAIL_TO_GET_REPORTS]:
    'Digite o seu endereço de email para receber relatórios diretamente na sua caixa de entrada',
  [englishText.EMAIL_SEND_ERROR]:
    'Ops!! Alguma coisa deu errado enquanto o Email estava sendo enviado',
  [englishText.TRY_AGAIN]: 'Tente Novamente!',
  [englishText.OOPS_SOMETHING_WENT_WRONG]: 'Ops! Alguma Coisa Deu Errado',
  [englishText.REQUEST_COMPLETE_ERROR]:
    'Desculpe. Nós estamos tendo Problemas para Completar a sua Solicitação. Por favor tente novamente.',
  [englishText.MULTI_IMG_POSTING]: 'Posts com Diversas Imagens',
  [englishText.MULTI_IMG_PLAN_MSG]:
    'Faça um upgrade para qualquer um dos nossos planos pagos para acessar este recurso premium de compartilhar diversas imagens em um único post',
  [englishText.MULTI_IMG_DESC]:
    'Posts com Diversas Imagens permite com que você compartilhe até 4 imagens em um só post.',
  [englishText.GIF_SUPPORT]: 'Suporte de GIF',
  [englishText.GIF_SUPPORT_PLAN_MSG]:
    "Faça um upgrade para um dos nossos planos premium (Profissional, Equipe Pequena, Agência) para agendar e compartilhar GIF's para os diversos perfis de suas mídias sociais.",
  [englishText.GIF_SUPPORT_DESC]:
    "Você pode adicionar e agendar GIF's incríveis aos seus posts para torná- los ainda mais engajadores.",
  [englishText.DROPBOX_SUPPORT_PLAN_MSG]:
    'Faça um upgrade para um dos nossos planos premium (Profissional, Equipe Pequena, Agência) para agendar e compartilhar imagens do Dropbox para os seus diversos perfis de mídias socias.',
  [englishText.DROPBOX_SUPPORT_DESC]:
    'Torne o compartilhamento de imagem ainda mais fácil ao adicionar e agendar as imagens diretamente do seu Dropbox.',
  [englishText.DRAFT_SUPPORT]: 'Suporte ao Rascunho de Posts',
  [englishText.DRAFT_SUPPORT_PLAN_MSG]:
    'Faça um upgrade para um dos nossos planos premium (Profissional, Equipe Pequena, Agência) para salvar os seus posts como Rascunhos e agendá-los e publicá-los depois.',
  [englishText.DRAFT_SUPPORT_DESC]:
    'Você pode adicionar e salvar um número de posts como Rascunhos e depois usá-los conforme for necessário.',
  [englishText.ADV_POST_SUPPORT]: 'Suporte Avançado de Post',
  [englishText.ADV_POST_SUPPORT_MSG]:
    'Faça um upgrade para um dos nossos planos premium (Profissional, Equipe Pequena, Agência) para criar posts para diferentes contas ao mesmo tempo.',
  [englishText.ADV_POST_SUPPORT_DESC]:
    'Você pode criar diferentes posts para diferentes tipos de contas.',
  [englishText.DOWNLOAD_SAMPLE_REPORT]:
    'Faça o Dowload da Amostra de Relatório',
  [englishText.DOWNLOAD_SAMPLE_REPORT_HEADER]:
    'Faça o Download de Lindos relatórios com o seu próprio branding',
  [englishText.DOWNLOAD_SAMPLE_REPORT_MSG]:
    'Faça um upgrade para um dos nossos planos premium (Profissional, Equipe Pequena, Agência) para fazer o download ilimitado de relatórios da sua marca em PDF para compartilhar com os seus clientes e com a sua equipe.',
  [englishText.DOWNLOAD_SAMPLE_REPORT_DESC]:
    'Não há necesidade de gastar tempo e esforço extras no desenvolvimento de relatórios separadamente',
  [englishText.EMAIL_PDF_HEADER]:
    'Envie Emails com Lindos Relatórios com o seu próprio Branding',
  [englishText.EMAIL_PDF_MSG]:
    'Faça um upgrade para um dos nossos planos premium (Profissional, Equipe Pequena, Agência) para enviar emails com relatórios da sua marca em PDF diretamente aos seus clientes e aos membros da sua equipe.',
  [englishText.BOOST_POST_SUPPORT]: 'Suporte de Impulsionamento de Post',
  [englishText.BOOST_POST_SUPPORT_MSG]:
    'Faça um upgrade para um dos nossos planos premium (Profissional, Equipe Pequena, Agência) para impulsionar com facilidades os seus posts do Facebook com o SocialPilot.',
  [englishText.BOOST_POST_SUPPORT_DESC]:
    'Promova os seus posts do Facebook facilmente com o suporte de impulsionamento de post do SocialPilot.',
  [englishText.SORRY_CANT_USE_FEATURE]:
    'Desculpe! Você não pode usar este recurso.',
};

export default portugeaze;

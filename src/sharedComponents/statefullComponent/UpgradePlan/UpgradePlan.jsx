/* eslint-disable no-nested-ternary */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import Header from '../../pureComponent(stateless)/Header/Header';
import Button from '../../pureComponent(stateless)/Buttons/Button';
import redirectTo from '../../../utils/functions/redirectTo';
import './UpgradePlan.scss';
import GridLayout from '../../../Layouts/GridLayout';
import UpgradeMembershipModal from '../../../features/membership/modal/membershipModal';
import {
  showAlert,
  cancelAlert,
} from '../../../actions/flashMessage/falshMessage.action';

import DisplayText from '../../lables/index';
import GlobalDisplayTexts from '../../../utils/labels/localizationText';

class UpgradePlan extends Component {
  constructor(props) {
    super(props);
    this.state = { show: false, showFlashAlert: true };
  }

  componentDidMount() {
    const { upgradeMsg, showAlertAction } = this.props;
    if (upgradeMsg) {
      showAlertAction({
        showAlert: true,
        type: 'danger',
        message: upgradeMsg,
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { planDetails } = this.props;
    if (
      planDetails.planUpgradeURL &&
      planDetails.planUpgradeURL !== prevProps.planDetails.planUpgradeURL
    ) {
      redirectTo(planDetails.planUpgradeURL);
    }
  }

  componentWillUnmount() {
    const { disableAlert } = this.props;
    disableAlert();
  }

  handleDismiss = () => {
    this.setState({ showFlashAlert: false });
  };

  openPlanModal = () => {
    this.setState(state => {
      const currentState = { ...state };
      currentState.show = true;
      return currentState;
    });
  };

  closePlanModal = () => {
    this.setState(state => {
      const currentState = { ...state };
      currentState.show = false;
      return currentState;
    });
  };

  upgradePlan = membershipId => {
    const { upgradePlan } = this.props;
    upgradePlan(membershipId);
  };

  render() {
    const {
      // upgradeMsg,
      upgradeHeaderText,
      upgradeSubHeaderText,
      planDetails,
      hideButton,
      // isTrialUsed,
      featureScreenshot,
    } = this.props;
    const { show } = this.state;
    return (
      <GridLayout>
        <Header header={upgradeHeaderText}>
          <div className="membership-upgrade-btn">
            {upgradeSubHeaderText && <p>{upgradeSubHeaderText}</p>}
            {!hideButton ? (
              planDetails.successResponse.data.currentPlan.mmId === 6 ? (
                <Button
                  bsClass={'btn btn-primary'}
                  onClick={() => {
                    window.location.href =
                      'https://www.socialpilot.co/contact-us';
                  }}
                >
                  <FormattedMessage
                    id={DisplayText.CONTACT_US}
                    defaultMessage={DisplayText.CONTACT_US}
                  />
                </Button>
              ) : (
                <Button
                  bsClass={'btn btn-primary'}
                  onClick={this.openPlanModal}
                >
                  <FormattedMessage
                    id={GlobalDisplayTexts.UPGRADE_NOW}
                    defaultMessage={GlobalDisplayTexts.UPGRADE_NOW}
                  />
                </Button>
              )
            ) : null}
          </div>
        </Header>
        <div className="row">
          <div className="col-lg-12">
            <img
              src={featureScreenshot}
              alt="screenshot"
              className="upgradeimage"
            />
          </div>
        </div>

        {show && (
          <UpgradeMembershipModal
            pathFrom="upgradeplan"
            onCloseModal={this.closePlanModal}
          />
        )}
      </GridLayout>
    );
  }
}

UpgradePlan.propTypes = {
  upgradeHeaderText: PropTypes.string.isRequired,
  featureScreenshot: PropTypes.string.isRequired,
  upgradePlan: PropTypes.func.isRequired,
  planDetails: PropTypes.oneOfType([PropTypes.object]).isRequired,
  upgradeMsg: PropTypes.string,
  upgradeSubHeaderText: PropTypes.string,
  // isTrialUsed: PropTypes.string.isRequired,
  showAlertAction: PropTypes.func.isRequired,
  hideButton: PropTypes.bool,
  disableAlert: PropTypes.func.isRequired,
};
UpgradePlan.defaultProps = {
  upgradeSubHeaderText: '',
  upgradeMsg: '',
  hideButton: false,
};

const mapStateToProps = () => {};
const mapDispatchToProps = dispatch => ({
  showAlertAction: obj => {
    dispatch(showAlert(obj));
  },
  disableAlert: () => {
    dispatch(cancelAlert());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(UpgradePlan);

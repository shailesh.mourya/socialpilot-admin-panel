import React from 'react';
import PropTypes from 'prop-types';

class SPCheckbox extends React.Component {
  constructor(props) {
    super(props);
    this.state = { check: props.checked };
    this.change = this.change.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { checked } = this.props;
    if (prevProps.checked !== checked) {
      this.setState({
        check: checked,
      });
    }
  }

  change(event) {
    const { id, checked } = event.target;

    try {
      this.setState(
        state => {
          const newState = { ...state };
          const checkState = state.check;
          newState.check = !checkState;
          return newState;
        },
        () => {
          const { onChange } = this.props;
          onChange(id, checked);
        },
      );
    } catch (_err) {
      // ignore
    }
  }

  render() {
    const { children, className, disabled, value, id, checked } = this.props;

    return (
      <div className={`custom-control custom-checkbox ${className} `}>
        <input
          type="checkbox"
          className="custom-control-input"
          onChange={this.change}
          value={value}
          disabled={disabled}
          checked={checked}
          id={id}
        />

        <label
          htmlFor={id}
          className="custom-control-label"
          disabled={disabled}
        >
          {children}
        </label>
      </div>
    );
  }
}

SPCheckbox.defaultProps = {
  disabled: false,
  id: '',
  checked: false,
  onChange: () => {},
  className: '',
  value: '',
  children: null,
};

SPCheckbox.propTypes = {
  checked: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.array,
    PropTypes.object,
  ]),
  className: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChange: PropTypes.func,
  value: PropTypes.string,
};

export default SPCheckbox;

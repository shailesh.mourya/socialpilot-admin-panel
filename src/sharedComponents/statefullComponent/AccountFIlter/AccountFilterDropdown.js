import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SPDropdown from '../SPdropDown/SPDropdown';
import accountsData, {
  findAccountWithId,
} from '../../../utils/misc/accountWithId';
import GlobalDisplayTexts from '../../../utils/labels/localizationText';
import DisplayText from '../../lables/index';
import getLocalizeText from '../../../utils/functions/getLocalizeText';

// please make sure you add this after getting all the successResponse
// from the apis which you wanted to include
class AccountSelectionSettings extends Component {
  constructor(props) {
    super(props);
    this.options = this.initializeOptions();
    this.state = {
      defaultSelection: { value: '', label: GlobalDisplayTexts.ALL },
      keyword: '',
      firstinitilize: true,
      criteriaKeyword: {
        value: '',
        selectedGroupOption: 0,
        selectedSubOption: 0,
      },
    };
    this.handleSearchInput = this.handleSearchInput.bind(this);
    this.handleSearchCriteria = this.handleSearchCriteria.bind(this);
  }

  componentDidMount() {
    const { clientFromProps } = this.props;
    const { firstinitilize } = this.state;
    this.initializeOptions();
    if (clientFromProps && firstinitilize && this.options) {
      let optionIndex = 0;
      this.options.forEach((option, indexvalue) => {
        if (option.label === 'Clients') {
          optionIndex = indexvalue;
        }
      });
      const hasclientInOption = this.options[optionIndex].options.find(
        account => account.value === `client_${clientFromProps}`,
      );

      if (hasclientInOption) {
        this.setState({
          firstinitilize: false,
          defaultSelection: hasclientInOption,
          criteriaKeyword: {
            value: hasclientInOption.value,
          },
        });
      }
    }
  }

  componentDidUpdate(preprops) {
    const { accountDelete, allCheckedState, facebookPagesList } = this.props;
    if (accountDelete !== preprops.accountDelete) {
      if (allCheckedState !== preprops.accountDelete) {
        if (allCheckedState) {
          this.setState({
            defaultSelection: { value: '', label: GlobalDisplayTexts.ALL },
            keyword: '',
          });
        }
      }
    }
    if (facebookPagesList !== preprops.facebookPagesList) {
      this.options = this.initializeOptions();
    }
    const { getFilterID } = this.props;
    if (getFilterID !== preprops.getFilterID) {
      this.options = this.initializeOptions();
    }
  }

  handlesearchbtn = () => {
    const { criteriaKeyword, keyword } = this.state;
    const { handleSearch } = this.props;
    const [criteria, value] = criteriaKeyword.value.split('_');
    handleSearch(keyword, criteria, parseInt(value, 10));
  };

  sortAccountName = () => {
    const { accountList } = this.props;
    try {
      return accountList.list.sort((a, b) => {
        const nameA = a.accountUserName.toLowerCase();
        const nameB = b.accountUserName.toLowerCase();
        if (nameA < nameB) return -1;
        if (nameA > nameB) return 1;
        return 0; // default return value (no sorting)
      });
    } catch (error) {
      return accountList.list;
    }
  };

  generateFilterAndFilterIdFromProps = (generatedOptions, filterId, filter) => {
    let label;
    let value;
    switch (filter) {
      case 'accounts':
        label = GlobalDisplayTexts.ACCOUNTS;
        value = `accounts_${filterId}`;
        break;
      case 'groups':
        label = GlobalDisplayTexts.GROUPS;
        value = `groups_${filterId}`;
        break;

      case 'team':
      case 'teams':
        label = GlobalDisplayTexts.TEAMS;
        value = `team_${filterId}`;
        break;

      case 'Client':
      case 'client':
        label = GlobalDisplayTexts.CLIENTS;
        value = `client_${filterId}`;
        break;

      case 'adAccounts':
        label = DisplayText.AD_ACCOUNTS;
        value = `accountId_${filterId}`;
        break;

      case 'facebookPages':
        label = DisplayText.FB_PAGES;
        value = `facebookPages_${filterId}`;
        break;

      default:
        label = '';
        value = '';
        break;
    }

    const hasAccount = generatedOptions.findIndex(
      i => i.label && i.label.includes(label),
    );

    let indexofAccount = 0;
    if (hasAccount !== -1) {
      indexofAccount = generatedOptions[hasAccount].options.find(
        i => i.value === value,
      );
    }

    if (indexofAccount) {
      this.setState({
        defaultSelection: indexofAccount,
      });
    }
  };

  handleFilterAccount = selectedOption => {
    this.setState(
      {
        selectedOption,
      },
      () => {
        const {
          handlingOfValues,
          fetchUnschedulePostDetails,
          fetchErrorPostDetails,
          fetchDeliverePostDetails,
          fetchContributedPost,
          fetchQueuedPostAction,
        } = this.props;
        let filter = selectedOption.value.split('_')[0];
        const filterId = selectedOption.value.split('_')[1];
        if (filter === 'teams') {
          filter = filter.slice(0, filter.length - 1);
        }

        const filterObject = { filter, filterId };
        handlingOfValues(filterObject);

        const { filterWith } = this.props;
        switch (filterWith) {
          case 'unschedulePost':
            fetchUnschedulePostDetails(1, filter, filterId, 1);
            break;
          case 'deliveredPost':
            fetchDeliverePostDetails(1, filter, filterId, '', 1);
            break;
          case 'contributedPost':
            fetchContributedPost(1, filter, filterId, 1);
            break;
          case 'queuedPost':
            fetchQueuedPostAction(1, filter, filterId, 1);
            break;
          case 'ErrorPost':
            fetchErrorPostDetails(1, filter, filterId, 1);
            break;
          default:
            break;
        }
      },
    );
  };

  handleEmptySearch = () => {
    this.setState(
      {
        keyword: '',
      },
      () => {
        const { searchWithCloseBtn, handleSearch } = this.props;

        if (searchWithCloseBtn) {
          handleSearch('');
        }
      },
    );
  };

  loadDropDownUI = isAdmin => {
    const { defaultSelection } = this.state;
    const dropdownOptions = this.getOptionsLength(this.options);
    return (
      <SPDropdown
        options={this.options}
        isDisabled={isAdmin}
        onChange={this.handleSearchCriteria}
        dropdownType="group"
        isClearable
        placeHolder={getLocalizeText(GlobalDisplayTexts.SELECT_ACCOUNT)}
        width="200px"
        defaultValue={defaultSelection}
        isSearchable={dropdownOptions > 5}
      />
    );
  };

  loadSearchUI = (isAdmin, keyword, searchWithCloseBtn) => (
    <>
      <div className="form-group ml-2 search-group-filter-block">
        <input
          className="form-control"
          type="text"
          placeholder={getLocalizeText(DisplayText.SEARCH)}
          disabled={isAdmin}
          value={keyword}
          onChange={this.handleSearchInput}
          onKeyPress={event => {
            if (event.key === 'Enter') {
              event.preventDefault();
              this.handlesearchbtn();
            }
          }}
        />

        {keyword && searchWithCloseBtn && (
          <btn className="btn_search_close" onClick={this.handleEmptySearch}>
            <i className="fas fa-times" />
          </btn>
        )}
        {!keyword && searchWithCloseBtn && (
          <i className="fas fa-search search-icon" />
        )}
      </div>
    </>
  );

  getOptionsLength = dropdownOptions => {
    let count = 0;
    if (dropdownOptions && dropdownOptions.length > 0) {
      for (let index = 0; index < dropdownOptions.length; index += 1) {
        const element = dropdownOptions[index];
        if (element && element.options) {
          count += element.options.length;
        }
      }
    }
    return count;
  };

  initializeOptions() {
    const {
      groupsList,
      clientsList,
      adAccountsList,
      facebookPagesList,
      disableAccountList,
      teamList,
    } = this.props;

    const groups = [];
    const clients = [];
    const accounts = [];
    const adAccounts = [];
    const teams = [];
    const facebookPages = [];
    const groupwithlabel = [{ label: 'By Groups', options: groups }];
    const teamwithlabel = [{ label: 'By Teams', options: teams }];
    const clientwithlabel = [{ label: 'By Clients', options: clients }];
    const accountwithlabel = [{ label: 'By Accounts', options: accounts }];
    const adaccountwithlabel = [
      { label: DisplayText.BY_AD_ACCOUNTS, options: adAccounts },
    ];
    const facebookPageswithlabel = [
      { label: DisplayText.BY_FB_PAGES, options: facebookPages },
    ];

    if (groupsList) {
      for (let i = 0; i < groupsList.length; i += 1) {
        const { groupId, groupName } = groupsList[i];

        groups.push({ label: groupName, value: `groups_${groupId}` });
      }
    }

    const { fromPosting } = this.props;
    // const { fromCalendar} = this.props
    // const clientOrTeam = fromCalendar ? 'team' : 'client';

    if (clientsList) {
      for (let i = 0; i < clientsList.length; i += 1) {
        const { teamId, firstName, lastName } = clientsList[i];
        clients.push({
          label: `${firstName} ${lastName}`,
          value: `client_${teamId}`,
        });
      }
    }

    if (teamList && teamList.length) {
      teamList.forEach(teamDetail => {
        const { teamId, firstName, lastName } = teamDetail;
        teams.push({
          label: `${firstName} ${lastName}`,
          value: `team_${teamId}`,
        });
      });
    }
    if (adAccountsList && adAccountsList.length) {
      for (let i = 0; i < adAccountsList.length; i += 1) {
        const { loginId, actName } = adAccountsList[i];
        adAccounts.push({
          label: actName,
          value: `accountId_${loginId}`,
        });
      }
    }

    for (let i = 0; i < facebookPagesList.length; i += 1) {
      const { loginId, accountUsername } = facebookPagesList[i];
      facebookPages.push({
        label: accountUsername,
        value: `pageAccountId_${loginId}`,
      });
    }

    const { accountList } = this.props;
    if (fromPosting || accountList.list) {
      let socialAc = '';
      const list = this.sortAccountName();

      if (accountList.list && accountList.list.length) {
        for (let i = 0; i < accountList.list.length; i += 1) {
          if (list[i]) {
            const { loginId, accountUserName, isLocked, accountId } = list[i];
            if (isLocked !== 'Y') {
              socialAc = findAccountWithId(accountId);
              accounts.push({
                label: `${accountUserName} - ${socialAc}`,
                value: `accounts_${loginId}`,
              });
            }
          }
        }
      }
    } else if (!disableAccountList) {
      Object.values(accountsData).forEach(act => {
        accounts.push({
          label: act.socialmedaia,
          value: `accounts_${act.accountIds[0]}`,
        });
      });
    }

    let generatedOptions = [];
    const clientLength = Object.values(clients).length;
    const groupLength = Object.values(groups).length;
    const teamLength = Object.values(teams).length;
    const adAccountsLength = Object.values(adAccounts).length;
    const facebookPagesLength = Object.values(facebookPages).length;
    if (groupLength) {
      generatedOptions = [...groupwithlabel];
    }

    if (facebookPagesLength) {
      generatedOptions = [...facebookPageswithlabel, ...generatedOptions];
    }

    if (adAccountsLength) {
      generatedOptions = [...adaccountwithlabel, ...generatedOptions];
    }

    if (clientLength) {
      generatedOptions = [...clientwithlabel, ...generatedOptions];
    }

    if (teamLength) {
      generatedOptions = [...teamwithlabel, ...generatedOptions];
    }

    if (accounts.length) {
      generatedOptions = [...generatedOptions, ...accountwithlabel];
    }
    generatedOptions = [...generatedOptions];

    const { getFilter, getFilterID } = this.props;
    if (getFilterID && getFilter) {
      this.generateFilterAndFilterIdFromProps(
        generatedOptions,
        getFilterID,
        getFilter,
      );
    }
    if (generatedOptions.length && generatedOptions[0].custom) {
      generatedOptions.shift();
    }
    return generatedOptions;
  }

  handleSearchCriteria(obj) {
    const { handleDropdownChange } = this.props;
    handleDropdownChange(obj);
    this.setState(state => {
      const currentState = { ...state };

      let {
        selectedGroupOption,
        selectedSubOption,
        value,
      } = currentState.criteriaKeyword;
      if (obj.value === '') {
        selectedGroupOption = 0;
        selectedSubOption = 0;
        value = '';
      } else {
        const [criteria, index, id] = obj.value.split('_');
        // selected group option which is now in use by now
        // eslint-disable-next-line default-case
        switch (criteria) {
          case 'group':
            selectedGroupOption = 2;
            break;

          case 'client':
            selectedGroupOption = 1;
            break;

          case 'adAccounts':
            selectedGroupOption = 4;
            break;

          case 'facebookPages':
            selectedGroupOption = 5;
            break;

          default:
            selectedGroupOption = 3;
            break;
        }
        selectedSubOption = parseInt(index, 10);
        value = `${criteria}_${id}`;
      }
      currentState.criteriaKeyword = {
        selectedGroupOption,
        selectedSubOption,
        value,
      };
      return currentState;
    });
  }

  handleSearchInput(event) {
    this.setState(
      {
        keyword: event.target.value,
      },
      () => {
        const {
          searchWithCloseBtn,
          handleSearch,
          blockinputChangeProp,
        } = this.props;
        const { keyword } = this.state;
        if (blockinputChangeProp) {
          return null;
        }
        if (searchWithCloseBtn) {
          handleSearch(keyword);
        }
        return null;
      },
    );
  }

  render() {
    const { keyword, defaultSelection } = this.state;
    const dropdownOptions = this.getOptionsLength(this.options);
    const {
      isAdmin,
      fromPosting,
      // disableSearchBtn,
      searchWithCloseBtn,
      disableSearch,
      showDropdown,
      children,
    } = this.props;
    if (fromPosting) {
      return (
        <SPDropdown
          width="300px"
          options={this.options}
          onChange={this.handleFilterAccount}
          dropdownType="group"
          menuBorderRadius="2px"
          placeHolder={getLocalizeText(DisplayText.FILTER_RECORDS)}
          isClearable
          defaultValue={defaultSelection}
          isSearchable={dropdownOptions > 5}
        />
      );
    }
    return (
      <>
        {showDropdown && <>{this.loadDropDownUI(isAdmin)}</>}
        {!disableSearch && (
          <>{this.loadSearchUI(isAdmin, keyword, searchWithCloseBtn)}</>
        )}
        {children}
      </>
    );
  }
}
AccountSelectionSettings.propTypes = {
  accountDelete: PropTypes.bool,
  accountList: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  adAccountsList: PropTypes.arrayOf(PropTypes.object),
  facebookPagesList: PropTypes.arrayOf(PropTypes.object),
  allCheckedState: PropTypes.bool,
  clientFromProps: PropTypes.string,
  clientsList: PropTypes.arrayOf(PropTypes.object),
  fetchContributedPost: PropTypes.func,
  fetchDeliverePostDetails: PropTypes.func,
  fetchErrorPostDetails: PropTypes.func,
  fetchQueuedPostAction: PropTypes.func,
  fetchUnschedulePostDetails: PropTypes.func,
  filterWith: PropTypes.objectOf(PropTypes.string),
  fromPosting: PropTypes.bool,
  getFilter: PropTypes.string,
  // disableSearchBtn: PropTypes.bool,
  disableSearch: PropTypes.bool,
  getFilterID: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  teamList: PropTypes.arrayOf(PropTypes.object).isRequired,
  groupsList: PropTypes.arrayOf(PropTypes.object),
  handleSearch: PropTypes.func,
  handlingOfValues: PropTypes.func,
  isAdmin: PropTypes.bool,
  showDropdown: PropTypes.bool,
  handleDropdownChange: PropTypes.func.isRequired,
  disableAccountList: PropTypes.bool,
  searchWithCloseBtn: PropTypes.bool,
  children: null,
  blockinputChangeProp: PropTypes.bool,
};
AccountSelectionSettings.defaultProps = {
  accountDelete: false,
  getFilter: '',
  getFilterID: '',
  filterWith: { object: 'value' },
  allCheckedState: false,
  clientFromProps: '',
  clientsList: [],
  adAccountsList: [],
  facebookPagesList: [],
  isAdmin: false,
  fromPosting: false,
  children: PropTypes.node,
  accountList: {},
  showDropdown: true,
  groupsList: [],
  disableSearch: false,
  handlingOfValues: () => {},
  handleSearch: () => {},
  fetchUnschedulePostDetails: () => {},
  fetchContributedPost: () => {},
  fetchDeliverePostDetails: () => {},
  fetchErrorPostDetails: () => {},
  fetchQueuedPostAction: () => {},
  disableAccountList: false,
  searchWithCloseBtn: false,
  blockinputChangeProp: false,
};

export default AccountSelectionSettings;

/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import PropTypes from 'prop-types';
import 'react-datepicker/dist/react-datepicker.css';
import Flatpickr from 'react-flatpickr';
import moment from 'moment-timezone';
import './CalendarGroup.scss';
import { FormattedMessage } from 'react-intl';
import Button from '../../pureComponent(stateless)/Buttons/Button';
import DisplayText from '../../lables/index';

export default class CalendarGroup extends Component {
  constructor(props) {
    super(props);
    const { dateValue } = this.props;
    this.state = {
      startDate: moment(),
      timeValue: [dateValue],
      displayDate: '',
      displayTime: '',
      isOpen: false,
    };

    this.toggleCalendar = this.toggleCalendar.bind(this);
    this.clickedClose = this.clickedClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleTime = this.handleTime.bind(this);
    this.dateFormat = this.dateFormat.bind(this);
    this.timeFormat = this.timeFormat.bind(this);
    this.clickedSubmit = this.clickedSubmit.bind(this);
    this.onClickOutSideFunc = this.onClickOutSideFunc.bind(this);
  }

  componentDidMount() {
    const { userDetails, dateValue } = this.props;
    moment.updateLocale('en', {
      week: { dow: userDetails.calendarStartDay },
    });

    if (dateValue) {
      const datf = this.dateFormat(userDetails.dateFormat, dateValue);
      const timef = this.timeFormat(userDetails.timeFormat, dateValue);

      const a = moment(dateValue).format('YYYY-MM-DD HH:mm');
      const b = moment(a);

      this.setState({
        startDate: b,
        displayDate: datf,
        displayTime: timef,
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { dateValue } = this.props;
    if (dateValue !== prevProps.dateValue) {
      if (!dateValue) {
        this.setState({
          startDate: '',
          displayDate: '',
          displayTime: '',
          timeValue: '',
        });
      }
    }
  }

  onClickOutSideFunc() {
    const { handleCalOpenFunction } = this.props;
    handleCalOpenFunction('', false);
  }

  timeFormat = (timeFormat, date) => {
    let ampm;
    let strTime;
    let hours = date.getHours();
    let minutes = date.getMinutes();

    if (timeFormat === '12') {
      ampm = hours >= 12 ? 'PM' : 'AM';
      hours %= 12;
      hours = hours || 12;
      minutes = minutes < 10 ? `0${minutes}` : minutes;
      strTime = `${hours}:${minutes} ${ampm}`;
    } else {
      minutes = minutes < 10 ? `0${minutes}` : minutes;
      hours = hours < 10 ? `0${hours}` : hours;
      strTime = `${hours}:${minutes}`;
    }
    return strTime;
  };

  handleChange(date) {
    const { timeValue: timeValueData } = this.state;
    const { userDetails, getValueOfCalendar, wantTime, id } = this.props;
    const datef = this.dateFormat(userDetails.dateFormat, date._d);
    const timef = timeValueData[0]
      ? this.timeFormat(userDetails.timeFormat, timeValueData[0])
      : this.timeFormat(userDetails.timeFormat, date._d);
    this.setState(
      {
        startDate: date,
        displayDate: datef,
        timeValue: timeValueData || [date._d],
        displayTime: timef,
      },
      () => {
        const { startDate, timeValue } = this.state;
        // eslint-disable-next-line no-shadow
        const date = startDate._d;

        if (wantTime !== true) {
          getValueOfCalendar(id, date);
        } else {
          document.getElementsByClassName('flatpickr-hour')[0].focus();

          const time = timeValue[0];
          const timeHours = time ? time.getHours() : '';
          const timeMinutes = time ? time.getMinutes() : '';
          const timeSeconds = time ? time.getSeconds() : '';
          const submitDate = new Date(
            date.getFullYear(),
            date.getMonth(),
            date.getDate(),
            timeHours,
            timeMinutes,
            timeSeconds,
          );

          getValueOfCalendar(id, submitDate);
        }
      },
    );
  }

  handleTime(date) {
    const { userDetails, getValueOfCalendar, id } = this.props;
    const timef = this.timeFormat(userDetails.timeFormat, date[0]);
    this.setState(
      {
        timeValue: date,
        displayTime: timef,
      },
      () => {
        const { startDate, timeValue } = this.state;
        // eslint-disable-next-line no-shadow
        const date = startDate._d;
        const time = timeValue[0];
        const submitDate = new Date(
          date.getFullYear(),
          date.getMonth(),
          date.getDate(),
          time.getHours(),
          time.getMinutes(),
          time.getSeconds(),
        );

        getValueOfCalendar(id, submitDate);
      },
    );
  }

  dateFormat(dateFormat, date) {
    const { userDetails } = this.props;
    let DateTime;
    const today = date;
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();

    const options = {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
    };
    const a = today.toLocaleDateString(userDetails.defaultLanguage, options);

    const temp = a.split(' ');
    const mon = temp[0];

    switch (dateFormat) {
      case 'MMM DD, YYYY':
        DateTime = a;
        break;
      case 'YYYY-MM-DD':
        DateTime = `${yyyy}-${mm}-${dd}`;
        break;
      case 'DD-MM-YYYY':
        DateTime = `${dd}-${mm}-${yyyy}`;
        break;
      case 'DD-MMM-YYYY':
        DateTime = `${dd}-${mon}-${yyyy}`;
        break;
      case 'MM-DD-YYYY':
        DateTime = `${mm}-${dd}-${yyyy}`;
        break;
      default:
        DateTime = `${dd}-${mm}-${yyyy}`;
        break;
    }
    return DateTime;
  }

  clickedSubmit() {
    const { startDate, timeValue } = this.state;
    const { getValueOfCalendar, id, fromBoostPost } = this.props;
    const date = startDate._d;
    const time = timeValue[0];
    const timeHours = time ? time.getHours() : '';
    const timeMinutes = time ? time.getMinutes() : '';
    const timeSeconds = time ? time.getSeconds() : '';
    let sd = moment({
      year: date.getFullYear(),
      month: date.getMonth(),
      day: date.getDate(),
      hour: timeHours,
      minute: timeMinutes,
      second: timeSeconds,
    }).format();

    if (fromBoostPost) {
      sd = new Date(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        time.getHours(),
        time.getMinutes(),
        time.getSeconds(),
      );
    }
    getValueOfCalendar(id, sd);
    this.toggleCalendar();
  }

  toggleCalendar(id) {
    const { singleCalender, handleCalOpenFunction, disabled } = this.props;
    if (disabled) {
      return;
    }
    const { isOpen } = this.state;
    if (singleCalender === true) {
      this.setState({ isOpen: !isOpen });
    } else {
      handleCalOpenFunction(id, false);
    }
  }

  clickedClose() {
    this.toggleCalendar();
  }

  renderDatePicker() {
    const {
      minDate,
      maxDate,
      disabled,
      fromBoostPost,
      wantTime,
      userDetails,
    } = this.props;
    const { timeValue, startDate } = this.state;
    if (fromBoostPost === true) {
      return (
        <DatePicker
          calendarClassName="calClass"
          className="norClass"
          inline
          onChange={this.handleChange}
          moment={moment}
          onClickOutside={() => this.toggleCalendar('')}
          showMonthDropdown
          showYearDropdown
          onYearChange={this.handleChange}
          dropdownMode="select"
          selected={startDate}
          minDate={minDate ? moment(minDate) : moment()}
          maxDate={moment(maxDate)}
          disabled={disabled}
        >
          <div className="tabel-border border-top submit-cancel-area">
            {(wantTime === 'undefined' ||
              wantTime === true ||
              wantTime === '') && (
              <div className="row">
                <div className="col-lg-12">
                  <div className="custTemp">
                    <Flatpickr
                      value={timeValue}
                      onChange={this.handleTime}
                      options={{
                        noCalendar: true,
                        inline: true,
                        enableTime: true,
                        time_24hr: userDetails.timeFormat !== '12',
                      }}
                    />
                  </div>
                </div>
              </div>
            )}
            <div className="calendar-done table-border border-top">
              <Button
                type="submit"
                value="Done"
                bsClass="btn btn-primary"
                onClick={this.clickedSubmit}
              >
                <FormattedMessage
                  id={DisplayText.DONE}
                  defaultMessage={DisplayText.DONE}
                />
              </Button>
            </div>
          </div>
        </DatePicker>
      );
    }
    return (
      <DatePicker
        calendarClassName="calClass"
        className="norClass"
        inline
        onChange={this.handleChange}
        moment={moment}
        onClickOutside={() => this.toggleCalendar('')}
        showMonthDropdown
        showYearDropdown
        dropdownMode="select"
        selected={startDate}
        disabled={disabled}
        minDate={minDate ? moment(minDate) : moment()}
      >
        <div className="tabel-border border-top submit-cancel-area">
          {(wantTime === 'undefined' ||
            wantTime === true ||
            wantTime === '') && (
            <div className="row">
              <div className="col-lg-12">
                <div className="custTemp">
                  <Flatpickr
                    value={timeValue}
                    onChange={this.handleTime}
                    options={{
                      noCalendar: true,
                      inline: true,
                      enableTime: true,
                      time_24hr: userDetails.timeFormat !== '12',
                    }}
                  />
                </div>
              </div>
            </div>
          )}
          <div className="calendar-done table-border border-top">
            <Button
              type="submit"
              bsClass="btn btn-primary"
              onClick={this.clickedSubmit}
            >
              <FormattedMessage
                id={DisplayText.DONE}
                defaultMessage={DisplayText.DONE}
              />
            </Button>
          </div>
        </div>
      </DatePicker>
    );
  }

  render() {
    const { id, wantTime, isOpenCalendar, disabled, isError } = this.props;
    const { displayDate, displayTime, isOpen } = this.state;
    return (
      <>
        <input
          type="text"
          onClick={() => this.toggleCalendar(id)}
          value={displayDate}
          className={`form-control ${isError ? 'is-invalid' : ''}`}
          disabled={disabled}
        />
        {wantTime === true && (
          <input
            type="text"
            onClick={() => this.toggleCalendar(id)}
            value={displayTime}
            className={`form-control ${isError ? 'is-invalid' : ''}`}
            disabled={disabled}
          />
        )}
        <div
          role="presentation"
          className={`${disabled ? 'disabled' : ''} input-group-btn-addon`}
          disabled={disabled}
          onClick={() => this.toggleCalendar(id)}
        >
          <i className="far fa-calendar-plus" aria-hidden="true" />
        </div>
        {isOpenCalendar === true ? (
          <div>{this.renderDatePicker()}</div>
        ) : (
          isOpen && <div>{this.renderDatePicker()}</div>
        )}
      </>
    );
  }
}

CalendarGroup.propTypes = {
  // dateFormat: PropTypes.string,
  dateValue: PropTypes.oneOfType([PropTypes.object]).isRequired,
  defaultLanguage: PropTypes.string,
  getValueOfCalendar: PropTypes.func.isRequired,
  handleCalOpenFunction: PropTypes.func,
  id: PropTypes.string.isRequired,
  isOpenCalendar: PropTypes.bool,
  maxDate: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  minDate: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  singleCalender: PropTypes.bool.isRequired,
  userDetails: PropTypes.oneOfType([PropTypes.object]).isRequired,
  wantTime: PropTypes.bool,
  disabled: PropTypes.bool,
  fromBoostPost: PropTypes.bool,
  isError: PropTypes.bool,
};

CalendarGroup.defaultProps = {
  wantTime: true,
  // dateFormat: '',
  handleCalOpenFunction: '',
  fromBoostPost: false,
  isOpenCalendar: false,
  maxDate: '',
  minDate: '',
  defaultLanguage: '',
  disabled: false,
  isError: false,
};

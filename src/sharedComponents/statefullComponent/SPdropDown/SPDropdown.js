/* eslint-disable no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { FormattedMessage } from 'react-intl';

class SPDropdown extends React.Component {
  groupStyles = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  };

  groupBadgeStyles = {
    backgroundColor: '#EBECF0',
    borderRadius: '2em',
    color: '#172B4D',
    display: 'inline-block',
    fontSize: 14,
    fontWeight: 'normal',
    lineHeight: '1',
    minWidth: 1,
    padding: '0.16666666666667em 0.5em',
    textAlign: 'center',
  };

  constructor(props) {
    super(props);
    this.state = { value: props.defaultValue };

    this.onChange = this.onChange.bind(this);
    const { styles, width } = this.props;
    const styleWithWidth = { ...styles };
    styleWithWidth.width = width;

    this.allOption = {
      label: 'Select all',
      value: '*',
    };
    this.simpleStyles = {
      option: (provided, state) => ({
        ...provided,
        color: state.isSelected ? '#0f67ea' : '#4e515e',
        background: 'white',
        '&:active': {
          color: '#4d6180',
          backgroundColor: '#e1e5eb',
        },
        '&:hover': {
          color: '#4d6180',
          backgroundColor: '#e1e5eb',
        },
        '&:focus': {
          outline: 'none',
        },
        cursor: state.isDisabled ? 'not-allowed' : 'pointer',
        padding: styleWithWidth.optionPadding,
        fontSize: styleWithWidth.optionFontSize,
        margin: '0 0 0 0',
      }),
      control: (base, state) => ({
        ...base,
        borderColor: this.checkConditionForBorder(state.selectProps.menuIsOpen),

        opacity: '2',
        boxShadow: 'none',
        borderRadius: '2px',
        minHeight: styleWithWidth.dropdownHeight,
        height: styleWithWidth.dropdownHeight,
        padding: styleWithWidth.controlPadding,
        width: styleWithWidth.width,
        '&:hover': this.checkConditionForBorder(
          state.selectProps.menuIsOpen,
          'hover',
        ),
      }),
      container: base => ({
        ...base,
        height: styleWithWidth.dropdownHeight,
        // zIndex: '2',
      }),
      menu: base => ({
        ...base,
        borderRadius: '2px',
        margin: '0',
        zIndex: '2',
        border: 'solid 1px rgba(84, 106, 140, 0.5)',
        boxShadow: '1px 2px 10px 0 rgba(0, 0, 0, 0.1)',
        padding: '8px 0',
      }),
      clearIndicator: provided => ({
        ...provided,
        padding: '5px',
        color: '#7a8699',
      }),
      dropdownIndicator: (base, state) => ({
        ...base,
        color: state.selectProps.menuIsOpen ? '#ffffff' : '#7a8699',
        padding: '5px',
        background: state.selectProps.menuIsOpen
          ? '#0f67ea'
          : styleWithWidth.arrowBackground,
        transform: state.selectProps.menuIsOpen ? 'rotate(180deg)' : null,
        '&:hover': {
          color: state.selectProps.menuIsOpen ? '#fff' : '#7a8699',
        },
      }),
      indicatorSeparator: provided => ({
        ...provided,
        display: 'none !important',
      }),
      placeholder: base => ({
        ...base,
        color: '#cccdcd',
      }),
    };
    this.groupStyles = {
      option: (provided, state) => ({
        ...provided,

        color: state.isSelected ? '#0f67ea' : '#4d6180',
        background: 'white',
        '&:active': {
          backgroundColor: 'white',
        },
        '&:hover': {
          color: '#4d6180',
          backgroundColor: '#e1e5eb',
        },
        '&:focus': {
          outline: 'none',
        },
        cursor: state.isOptionDisabled ? 'not-allowed' : 'pointer',
        padding: styleWithWidth.optionPadding,
        fontSize: styleWithWidth.optionFontSize,
        lineHeight: 'normal',
        margin: '0 0 0 0',
      }),
      group: provided => ({
        ...provided,
        // padding: '8px 0',
        padding: '0',
        margin: '0',
      }),
      groupHeading: provided => ({
        ...provided,
        // padding: '8px',
        padding: '0',
        marginBottom: '0',
        fontWeight: styleWithWidth.groupFontWeight,
        color: '#4d6180 !important',
        textTransform: 'capitalize',
        fontSize: styleWithWidth.groupFontSize,
        // borderBottom: '1px solid #a9b4c5',
        lineHeight: 'normal',
      }),
      clearIndicator: provided => ({
        ...provided,
        padding: '5px',
        color: '#7a8699',
      }),
      control: (base, state) => ({
        ...base,
        padding: styleWithWidth.controlPadding,
        borderColor: this.checkConditionForBorder(state.selectProps.menuIsOpen),
        boxShadow: 'none',
        borderRadius: '2px',
        minHeight: styleWithWidth.dropdownHeight,
        height: styleWithWidth.dropdownHeight,
        width: styleWithWidth.width,
        '&:hover': this.checkConditionForBorder(
          state.selectProps.menuIsOpen,
          'hover',
        ),
      }),
      container: base => ({
        ...base,
        height: styleWithWidth.dropdownHeight,
        // zIndex: '2',
      }),
      menu: base => ({
        ...base,
        borderRadius: '2px',
        margin: '0',
        zIndex: '2',
        border: 'solid 1px rgba(84, 106, 140, 0.5)',
        boxShadow: '1px 2px 10px 0 rgba(0, 0, 0, 0.1)',
      }),
      dropdownIndicator: (base, state) => ({
        ...base,
        color: state.selectProps.menuIsOpen ? '#fff' : '#7a8699',
        padding: '5px',
        background: state.selectProps.menuIsOpen
          ? '#0f67ea'
          : styleWithWidth.arrowBackground,
        transform: state.selectProps.menuIsOpen ? 'rotate(180deg)' : null,
        '&:hover': {
          color: state.selectProps.menuIsOpen ? '#fff' : '#7a8699',
        },
      }),
      indicatorSeparator: provided => ({
        ...provided,
        display: 'none',
      }),
      placeholder: base => ({
        ...base,
        color: '#cccdcd',
      }),
    };
  }

  componentDidUpdate(prevProps) {
    const { defaultValue } = this.props;
    if (defaultValue && defaultValue !== prevProps.defaultValue) {
      this.setState({ value: defaultValue });
    }
  }

  onChange(value) {
    const { onChange, doChangeState } = this.props;
    if (!doChangeState) {
      this.setState({ value });
    }
    let valuePass = value;
    if (!value) {
      valuePass = { label: '', value: '' };
    }
    onChange(valuePass);
  }

  getSelectedValue = () => {
    const { forceSelectValue } = this.props;
    const { value } = this.state;

    if (forceSelectValue) {
      if (Object.keys(forceSelectValue).length) {
        return forceSelectValue;
      }
      return null;
    }
    return value;
  };

  checkConditionForBorder = (menuIsOpen, type) => {
    const { borderRed } = this.props;

    if (borderRed) {
      return '#ff6c60';
    }

    if (type === 'hover') {
      return '#a9b4c5';
    }
    return menuIsOpen ? '#0f67ea !important' : '#a9b4c5';
  };

  formatGroupLabel = data => (
    <>
      <div className="group-dropdownLabel">
        <span className="group-dropdown-border">&nbsp;</span>
      </div>
      {data.label ? (
        <div style={{ padding: '16px 8px 8px 8px' }}>
          <span>
            <FormattedMessage id={data.label} defaultValue={data.label} />
          </span>
        </div>
      ) : null}
    </>
  );

  render() {
    const {
      dropdownType,
      key,
      placeHolder,
      isDisabled,
      defaultValue,
      name,
      isOptionDisabled,
      isClearable,
      isSearchable,
      // eslint-disable-next-line no-unused-vars
      menuIsOpen,
      id,
      options,
      // isSearchable,
    } = this.props;
    const { value } = this.state;

    const selectedValue = this.getSelectedValue();

    if (!dropdownType) {
      return (
        <Select
          key={key}
          id={id}
          name={name}
          placeholder={placeHolder}
          isDisabled={isDisabled}
          defaultValue={
            !defaultValue ? null : defaultValue.value ? defaultValue : ''
          }
          isClearable={isClearable}
          value={
            !selectedValue ? null : selectedValue.value ? selectedValue : ''
          }
          isSearchable={isSearchable}
          onChange={this.onChange}
          options={options}
          styles={this.simpleStyles}
          isOptionDisabled={isOptionDisabled}
          classNamePrefix={'react-simple-select'}
          // menuIsOpen // if anyone want to handle menu opening with their own props
        />
      );
    }
    if (dropdownType && dropdownType === 'group') {
      return (
        <Select
          key={key}
          id={id}
          name={name}
          placeholder={placeHolder}
          isDisabled={isDisabled}
          defaultValue={
            !defaultValue ? null : defaultValue.value ? defaultValue : ''
          }
          isClearable={isClearable}
          isSearchable={isSearchable}
          // value={value}
          value={!value ? null : value.value ? value : ''}
          onChange={this.onChange}
          options={options}
          // components={{ Option: customComponentOption }}
          formatGroupLabel={this.formatGroupLabel}
          styles={this.groupStyles}
          classNamePrefix="react-select-prefix"
          // menuIsOpen
        />
      );
    }
    return null;
  }
}

SPDropdown.propTypes = {
  borderRed: PropTypes.bool,
  // eslint-disable-next-line react/require-default-props
  defaultValue: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  doChangeState: PropTypes.bool,
  id: PropTypes.string,
  isDisabled: PropTypes.bool,
  isOptionDisabled: PropTypes.bool,
  key: PropTypes.string,
  menuIsOpen: PropTypes.bool,
  name: PropTypes.string,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  placeHolder: PropTypes.string,
  dropdownType: PropTypes.string,
  isClearable: PropTypes.bool,
  // eslint-disable-next-line react/require-default-props
  width: PropTypes.string,
  styles: PropTypes.shape({
    groupHeadingPadding: PropTypes.string,
    groupFontWeight: PropTypes.string,
    groupFontSize: PropTypes.string,
    optionFontSize: PropTypes.string,
    optionPadding: PropTypes.string,
    dropdownHeight: PropTypes.string,
    menuMargin: PropTypes.string,
    controlPadding: PropTypes.string,
  }),
  forceSelectValue: PropTypes.arrayOf(PropTypes.object),
  isSearchable: PropTypes.bool,
};

SPDropdown.defaultProps = {
  key: '',
  id: '',
  name: '',
  defaultValue: {},
  placeHolder: '',
  borderRed: false,
  isDisabled: false,
  menuIsOpen: false,
  doChangeState: false,
  isOptionDisabled: false,
  isClearable: false,
  onChange: () => {},
  dropdownType: '',
  width: '100%',
  styles: {
    groupHeadingPadding: '0px 8px 0 8px',
    groupFontWeight: '500',
    groupFontSize: '14px',
    optionFontSize: '14px',
    optionPadding: '8px',
    dropdownHeight: '32px',
    menuMargin: '0',
    controlPadding: '0',
    arrowBackground: '#e1e5eb',
  },
  forceSelectValue: null,
  isSearchable: false,
};

export default SPDropdown;

const selectCustomStyles = {
  optionPadding: '4px 0 4px 10px',
  optionFontSize: '12px',
  menuMargin: '3px 0 0 0',
  groupHeadingPadding: '0 0 7px 10px',
  groupFontWeight: '700',
  groupFontSize: '14px',
  dropdownHeight: '33px',
};

export default selectCustomStyles;

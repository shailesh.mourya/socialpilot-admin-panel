/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import DisplayText from '../../lables/index';

import VideoProcessing from '../../../features/posts/createPost/components/postVideoPreview/videoProcessing';

import { UploadFile } from '../../../features/setting/editProfile/components/callApiForAws';

import './ClientInviteDetails.scss';

const ImageUpload = ({
  clientConnectLogo,
  onUploadImageSuccess,
  hideYourLogoText,
}) => {
  const [selectedFile, setSelectedFile] = useState();
  const [showLoading, setshowLoading] = useState(false);
  const [preview, setPreview] = useState();
  const [error, seterror] = useState('');

  useEffect(() => {
    setPreview(clientConnectLogo);
  }, [clientConnectLogo]);

  useEffect(() => {
    if (!selectedFile) {
      setPreview(undefined);
      return;
    }

    const objectUrl = URL.createObjectURL(selectedFile);
    setPreview(objectUrl);

    // eslint-disable-next-line consistent-return
    return () => URL.revokeObjectURL(objectUrl);
  }, [selectedFile]);

  const onSelectFile = async e => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(undefined);
      return;
    }

    const file = e.target.files[0];

    const allowedExtensions = ['image/jpg', 'image/jpeg', 'image/png'];

    if (allowedExtensions.indexOf(file.type) < 0) {
      seterror('Only files with these extensions are allowed: jpg, jpeg, png.');
      return;
    }
    if (file.size >= 1048576) {
      seterror('File size should not be greater than 1MB');
      return;
    }
    seterror('');
    setshowLoading(true);
    setSelectedFile(file);
    const res = await UploadFile(
      e.target.files[0],
      null,
      'clientId-123',
      '1123',
    );
    if (res) {
      setshowLoading(false);
      onUploadImageSuccess(res.Location);
    }
  };

  const handleUloadImage = () => {
    setSelectedFile('');
    setPreview('');
    onUploadImageSuccess('');
    document.querySelector('input[type=file]').value = '';
  };

  return (
    <>
      {!hideYourLogoText && (
        <label className="control-label" htmlFor="logo">
          <FormattedMessage
            id={DisplayText.YOUR_LOGO}
            defaultMessage={DisplayText.YOUR_LOGO}
          />
        </label>
      )}

      <div className={`image-upload ${preview && 'd-none'}`}>
        <label className="filebutton" style={{ border: '0px' }}>
          <span>
            <input
              type="file"
              accept=".png, .jpg, .jpeg, .tiff"
              onChange={onSelectFile}
            />
          </span>
        </label>
      </div>

      <div className={`uploaded-image  ${!preview && 'd-none image-upload '}`}>
        <div className="">
          <span
            role="presentation"
            className="remove-client-image"
            onClick={handleUloadImage}
          >
            <i className="fas fa-times" aria-hidden="true" />
          </span>
        </div>
        <div className="click_slide_hover">
          {showLoading && <VideoProcessing id="clientId-123" />}
        </div>
        <img
          src={preview}
          className="img-thumbnail  client-image"
          height={140}
          width={141}
          alt="clientImage"
        />
      </div>

      {error && (
        <p style={{ width: '140px' }} className="text-danger">
          {error}
        </p>
      )}
    </>
  );
};

ImageUpload.propTypes = {
  clientConnectLogo: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
    .isRequired,
  onUploadImageSuccess: PropTypes.func.isRequired,
  hideYourLogoText: PropTypes.bool,
};

ImageUpload.defaultProps = {
  hideYourLogoText: false,
};

export default ImageUpload;

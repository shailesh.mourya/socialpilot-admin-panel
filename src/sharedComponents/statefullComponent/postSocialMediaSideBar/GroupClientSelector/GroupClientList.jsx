import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import PropTypes from 'prop-types';
import GroupClientListItem from './GroupClientListItem';
import './GroupClientList.css';

export default class GroupClientList extends Component {
  constructor(props) {
    super(props);
    this.selectedGroups = [];
    this.checkedListItem = this.checkedListItem.bind(this);
  }

  extractList() {
    const lists = [];
    const { list, checked, pathfrom } = this.props;
    for (let i = 0; i < list.length; i += 1) {
      lists[i] = (
        <li className="form-group" key={i}>
          <GroupClientListItem
            checked={checked[list[i].id]}
            changefunc={this.checkedListItem}
            labelText={list[i].name}
            id={`${list[i].id}${pathfrom}`}
          />
        </li>
      );
    }

    return lists;
  }

  checkedListItem(id, checked) {
    const IdData = id.replace('FeedCuration', '');
    try {
      const accountId = parseInt(IdData, 10);
      const { setSelectedAccountsList, tab } = this.props;
      setSelectedAccountsList(accountId, tab, checked);
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const list = this.extractList();
    return (
      <Scrollbars style={{ height: '524px' }} autoHeightMax={30}>
        {list}
      </Scrollbars>
    );
  }
}
GroupClientList.propTypes = {
  pathfrom: PropTypes.string,
  setSelectedAccountsList: PropTypes.func.isRequired,
  tab: PropTypes.string,
  list: PropTypes.oneOfType([PropTypes.array]),
  checked: PropTypes.oneOfType([PropTypes.object]),
};
GroupClientList.defaultProps = {
  pathfrom: '',
  tab: '',
  list: [],
  checked: {},
};

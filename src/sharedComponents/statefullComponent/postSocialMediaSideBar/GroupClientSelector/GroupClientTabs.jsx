import React, { Component } from 'react';
import { FormattedMessage } from 'react-intl';
import { PropTypes } from 'prop-types';
import DisplayText from '../../../lables/index';
import LocalizationText from '../../../../utils/labels/localizationText';
import Searchbar from '../../Searchbar/Searchbar';
import { CURRENT_ENVIORMENT, CONFIG } from '../../../../utils/misc/config';
import GroupClientList from './GroupClientList';
import './GroupClientTabs.scss';
import getLocalizeText from '../../../../utils/functions/getLocalizeText';

class GroupClientTabs extends Component {
  constructor(props) {
    super(props);
    this.state = { key: 1 };
  }

  getCurrentTabList = () => {
    let list = null;
    const { key } = this.state;
    const {
      searchedGroupList,
      groupsList,
      searchedClientList,
      clientsList,
    } = this.props;
    try {
      switch (key) {
        case 1:
          list = searchedGroupList !== null ? searchedGroupList : groupsList;
          break;
        case 2:
          list = searchedClientList !== null ? searchedClientList : clientsList;
          break;
        default:
          list = null;
          break;
      }
    } catch (error) {
      console.error(error);
    }
    return list;
  };

  setTabContent = tab => {
    const searchGroupPlaceHolder = DisplayText.SEARCH_GRP;
    const searchClientPlaceHolder = DisplayText.SEARCH_CLIENT;

    const {
      searchModeGroup,
      searchModeClient,
      userRole,
      setSelectedAccountsList,
      pathfrom,
    } = this.props;

    let tabContent = (
      <a
        className="btn btn-primary full-width"
        href={CONFIG[CURRENT_ENVIORMENT].createGroupLink}
        // eslint-disable-next-line react/jsx-no-target-blank
        target="_blank"
        rel="noopener noreferrer"
      >
        <FormattedMessage
          id={DisplayText.CREATE_GROUP}
          defaultMessage={DisplayText.CREATE_GROUP}
        />
      </a>
    );
    switch (tab) {
      case 'group':
        if (this.getCurrentTabList().length === 0) {
          if (searchModeGroup === true) {
            tabContent = (
              <>
                <div className="form-group search-group-filter-block">
                  <Searchbar
                    placeholder={getLocalizeText(searchGroupPlaceHolder)}
                    searchfunc={this.handleSearch}
                  />
                </div>
                <center>
                  <FormattedMessage
                    id={DisplayText.NO_DATA_FOUND}
                    defaultMessage={DisplayText.NO_DATA_FOUND}
                  />
                </center>
              </>
            );
          } else {
            tabContent = (
              // <Button
              //   bsClass="btn btn-primary full-width"
              //   href={CONFIG[CURRENT_ENVIORMENT].createGroupLink}
              // >
              //   <FormattedMessage
              //     id={DisplayText.CREATE_GROUP}
              //     defaultMessage={DisplayText.CREATE_GROUP}
              //   />
              // </Button>
              <a
                className="btn btn-primary full-width"
                href={CONFIG[CURRENT_ENVIORMENT].createGroupLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                <FormattedMessage
                  id={DisplayText.CREATE_GROUP}
                  defaultMessage={DisplayText.CREATE_GROUP}
                />
              </a>
            );
          }
        } else {
          tabContent = (
            <>
              <div className="form-group search-group-filter-block">
                <Searchbar
                  placeholder={getLocalizeText(searchGroupPlaceHolder)}
                  searchfunc={this.handleSearch}
                />
              </div>
              <GroupClientList
                list={this.getCurrentTabList()}
                setSelectedAccountsList={setSelectedAccountsList}
                checked={this.getCurrentCheckedList()}
                tab={this.getCurrentTabName()}
                pathfrom={pathfrom}
              />
            </>
          );
        }
        break;

      case 'client':
        if (this.getCurrentTabList().length === 0) {
          if (searchModeClient === true) {
            tabContent = (
              <>
                <div className="form-group search-group-filter-block">
                  <Searchbar
                    placeholder={getLocalizeText(searchClientPlaceHolder)}
                    searchfunc={this.handleSearch}
                  />
                </div>
                <center>
                  <FormattedMessage
                    id={DisplayText.NO_DATA_FOUND}
                    defaultMessage={DisplayText.NO_DATA_FOUND}
                  />
                </center>
              </>
            );
          } else {
            tabContent =
              userRole === 'O' || userRole === 'A' ? (
                <a
                  className="btn btn-primary full-width"
                  href={CONFIG[CURRENT_ENVIORMENT].inviteClientLink}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <FormattedMessage
                    id={DisplayText.INVITE_CLIENT}
                    defaultMessage={DisplayText.INVITE_CLIENT}
                  />
                </a>
              ) : (
                <>
                  <center>
                    <FormattedMessage
                      id={DisplayText.NO_DATA_FOUND}
                      defaultMessage={DisplayText.NO_DATA_FOUND}
                    />
                  </center>
                </>
              );
          }
        } else {
          tabContent = (
            <>
              <div className="form-group search-group-filter-block">
                <Searchbar
                  placeholder={getLocalizeText(searchClientPlaceHolder)}
                  searchfunc={this.handleSearch}
                />
              </div>
              <GroupClientList
                list={this.getCurrentTabList()}
                setSelectedAccountsList={setSelectedAccountsList}
                checked={this.getCurrentCheckedList()}
                tab={this.getCurrentTabName()}
                pathfrom={pathfrom}
              />
            </>
          );
        }

        break;

      default:
    }
    return tabContent;
  };

  getCurrentTabName = () => {
    const { key } = this.state;
    let tabName = null;
    try {
      switch (key) {
        case 1:
          tabName = 'groups';
          break;
        case 2:
          tabName = 'clients';
          break;
        default:
          tabName = null;
      }
    } catch (error) {
      console.error(error);
    }
    return tabName;
  };

  getCurrentCheckedList = () => {
    let list = null;
    const { key } = this.state;
    const { checkedGroups, checkedClients } = this.props;
    try {
      switch (key) {
        case 1:
          list = checkedGroups;
          break;
        case 2:
          list = checkedClients;
          break;
        default:
          list = null;
      }
    } catch (error) {
      console.error(error);
    }
    return list;
  };

  handleSearch = searchKey => {
    const { searchList } = this.props;
    searchList(this.getCurrentTabName(), searchKey);
  };

  handleTabSelect = key => {
    try {
      this.setState(state => {
        const currentState = { ...state };
        currentState.key = key;
        return currentState;
      });
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    const { userRole, clientsList, isBulk } = this.props;
    // const { groupsList } = this.props;
    const { key } = this.state;
    const clientCount = clientsList.length;
    return (
      <div
        className={`groupv2-select manage-post-options tableborder-bottom  ${
          isBulk ? '' : 'tableborder-bottom tableborder-left'
        }`}
      >
        <div className={`${isBulk ? 'manage-post-options' : 'selectv2_tabs'}`}>
          <div
            id="filter-tab-account"
            className="filter-tab d-flex border-bottom mb-2"
          >
            <ul>
              <li
                className={` ${((userRole === 'T' && clientCount === 0) ||
                  userRole === 'C') &&
                  'only-group-tab'}`}
              >
                <a
                  className={`${key === 1 ? 'active' : ''} `}
                  id="group-tab"
                  data-toggle="tab"
                  href="#group"
                  role="tab"
                  aria-controls="group"
                  aria-selected="true"
                  onClick={() => {
                    this.handleTabSelect(1);
                  }}
                >
                  {/* {groupsList.length}{' '} */}
                  <FormattedMessage
                    id={LocalizationText.GROUPS}
                    defaultMessage={LocalizationText.GROUPS}
                  />
                </a>
              </li>
              {(userRole === 'O' ||
                userRole === 'A' ||
                (userRole === 'T' && clientCount !== 0)) && (
                <li>
                  <a
                    className={`${key === 2 ? 'active' : ''} `}
                    id="client-tab"
                    data-toggle="tab"
                    href="#client"
                    role="tab"
                    aria-controls="client"
                    aria-selected="true"
                    onClick={() => {
                      this.handleTabSelect(2);
                    }}
                  >
                    {/* {clientsList.length}{' '} */}
                    <FormattedMessage
                      id={LocalizationText.CLIENTS}
                      defaultMessage={LocalizationText.CLIENTS}
                    />
                  </a>
                </li>
              )}
            </ul>
          </div>
          <div className="clear" />
          <div className="tab-content" id="myTabContent">
            <div
              className="tab-pane show active" // fade class to add fade effect
              id="group"
              role="tabpanel"
              aria-labelledby="group-tab"
            >
              {this.setTabContent('group')}
            </div>
            {(userRole === 'O' ||
              userRole === 'A' ||
              (userRole === 'T' && clientCount !== 0)) && (
              <div
                className="tab-pane" // fade class to add fade effect
                id="client"
                role="tabpanel"
                aria-labelledby="client-tab"
              >
                {this.setTabContent('client')}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

GroupClientTabs.propTypes = {
  userRole: PropTypes.string.isRequired,
  clientsList: PropTypes.oneOfType([PropTypes.array, PropTypes.object])
    .isRequired,
  searchList: PropTypes.func.isRequired,
  checkedGroups: PropTypes.objectOf(PropTypes.bool),
  checkedClients: PropTypes.objectOf(PropTypes.bool),
  searchedGroupList: PropTypes.arrayOf(PropTypes.object),
  groupsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  searchedClientList: PropTypes.arrayOf(PropTypes.object),
  searchModeGroup: PropTypes.bool.isRequired,
  searchModeClient: PropTypes.bool.isRequired,
  setSelectedAccountsList: PropTypes.func.isRequired,
  pathfrom: PropTypes.string,
  isBulk: PropTypes.bool,
};

GroupClientTabs.defaultProps = {
  searchedClientList: [],
  searchedGroupList: [],
  checkedClients: {},
  checkedGroups: {},
  pathfrom: '',
  isBulk: false,
};

export default GroupClientTabs;

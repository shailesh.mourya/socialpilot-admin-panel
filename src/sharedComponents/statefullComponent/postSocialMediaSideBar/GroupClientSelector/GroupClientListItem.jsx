import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './GroupClientListItem.css';
import SPCheckbox from '../../../pureComponent(stateless)/checkbox/SPCheckbox';

const displayName = str => {
  const stringArray = Array.from(str);
  if (stringArray.length < 25) {
    return str;
  }
  return `${stringArray.splice(0, 25).join('')}...`;
};
export default class GroupClientListItem extends Component {
  handleChange = (id, checked) => {
    const { changefunc } = this.props;
    changefunc(id, checked);
  };

  render() {
    const { checked, id, labelText } = this.props;
    return (
      <SPCheckbox
        inline
        checked={checked}
        id={id}
        onChange={this.handleChange}
        value={labelText}
      >
        <span title={labelText}>{displayName(labelText)}</span>
      </SPCheckbox>
    );
  }
}

GroupClientListItem.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  checked: PropTypes.bool,
  labelText: PropTypes.string,
  changefunc: PropTypes.func.isRequired,
};

GroupClientListItem.defaultProps = {
  checked: false,
  labelText: '',
};

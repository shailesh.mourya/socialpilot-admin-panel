const formatGroupClientData = (type, data) => {
  let list = null;
  let groupClientObj = {};
  if (type === 'groups') {
    groupClientObj = {
      id: 'groupId',
      name: 'groupName',
      accountsList: 'groupAccounts',
    };
  } else if (type === 'clients') {
    groupClientObj = {
      id: 'clientId',
      name: 'clientName',
      accountsList: 'clientAccounts',
    };
  }
  if (data) {
    list = [];
    for (let i = 0; i < data.list.length; i += 1) {
      list[i] = {};
      Object.entries(groupClientObj).forEach(([key, value]) => {
        list[i][key] = data.list[i][value];
      });
    }
  }
  return list;
};

export default formatGroupClientData;

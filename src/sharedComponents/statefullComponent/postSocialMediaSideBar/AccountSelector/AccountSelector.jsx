import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import AccountSelectorHeader from './AccountSelectorHeader';

import SocialMediaSelector from './SocialMediaSelector';
import UserList from './UserList';
import './AccountSelector.css';
import Searchbar from '../../Searchbar/Searchbar';
import getLocalizeText from '../../../../utils/functions/getLocalizeText';
import GlobalDisplayTexts from '../../../../utils/labels/localizationText';

class AccountSelector extends Component {
  constructor(props) {
    super(props);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleSocialMediaSelection = this.handleSocialMediaSelection.bind(
      this,
    );
  }

  handleSearch(searchKey = '') {
    const { searchList } = this.props;
    searchList('accounts', searchKey);
  }

  handleSocialMediaSelection(searchFilter) {
    const { handleSocialMediaFilter } = this.props;
    handleSocialMediaFilter(searchFilter);
  }

  geCheckedAccountsLength() {
    const { checkedAccounts } = this.props;
    let count = 0;

    Object.values(checkedAccounts).forEach(value => {
      if (value === true) {
        count += 1;
      }
    });
    return count;
  }

  render() {
    const {
      selectAllAccounts,
      allChecked,
      userDetails,
      formloginIdsError,
      accountsList,
      checkedAccounts,
      markAccountAsChecked,
      searchedAccountsList,
      socialMediaSelectorActive,
      twitterChecked,
      pathfrom,
      boostPostAllowedAccounts,
      isBulk,
      fromComponent,
    } = this.props;
    return (
      <div
        className={`accountv2-select ${
          isBulk ? '' : 'tableborder-right'
        } tableborder-left tableborder-bottom`}
      >
        <AccountSelectorHeader
          selectAllAccounts={selectAllAccounts}
          labelCount={this.geCheckedAccountsLength()}
          pathfrom={pathfrom}
          allChecked={allChecked}
        />

        <div className={`${isBulk ? 'row' : ''}`}>
          <div className={`${isBulk ? 'col-lg-5 col-xl-4' : ''}`}>
            <div className="row mb-2">
              <div className="col-lg-12">
                <SocialMediaSelector
                  userDetails={userDetails}
                  handleSocialMediaSelection={this.handleSocialMediaSelection}
                  fromComponent={fromComponent}
                  isBulk={isBulk}
                />
              </div>
            </div>
          </div>

          <div className={`${isBulk ? 'col-lg-5 col-xl-4' : ''}`}>
            <div className="row">
              <div className="col-lg-12">
                <div className="form-group search-group-filter-block">
                  <Searchbar
                    placeholder={getLocalizeText(
                      GlobalDisplayTexts.SEARCH_ACCOUNTS,
                      userDetails.defaultLanguage,
                    )}
                    searchfunc={this.handleSearch}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>

        {formloginIdsError && (
          <div className="row mb-2">
            <div className="col-lg-12">
              <div className="text-danger createpost-account-error">
                <ul>
                  <li>
                    <FormattedMessage
                      id={formloginIdsError}
                      defaultMessage={formloginIdsError}
                    />
                  </li>
                </ul>
              </div>
            </div>
          </div>
        )}

        <div className="row">
          <div className="col-lg-12">
            <UserList
              list={accountsList}
              formloginIdsError={formloginIdsError}
              checkedList={checkedAccounts}
              markAccountAsChecked={markAccountAsChecked}
              searchedAccountsList={searchedAccountsList}
              socialMediaSelectorActive={socialMediaSelectorActive}
              twitterChecked={twitterChecked}
              pathfrom={pathfrom}
              boostPostAllowedAccounts={boostPostAllowedAccounts}
              isBulk={isBulk}
            />
          </div>
        </div>
      </div>
    );
  }
}
AccountSelector.propTypes = {
  pathfrom: PropTypes.string,
  searchList: PropTypes.func.isRequired,
  handleSocialMediaFilter: PropTypes.func.isRequired,
  checkedAccounts: PropTypes.oneOfType([PropTypes.object]),
  userDetails: PropTypes.oneOfType([PropTypes.object]),
  selectAllAccounts: PropTypes.func.isRequired,
  accountsList: PropTypes.oneOfType([PropTypes.array]),
  allChecked: PropTypes.bool,
  formloginIdsError: PropTypes.string,
  markAccountAsChecked: PropTypes.func.isRequired,
  searchedAccountsList: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
  socialMediaSelectorActive: PropTypes.bool,
  twitterChecked: PropTypes.bool,
  boostPostAllowedAccounts: PropTypes.oneOfType([PropTypes.array]),
  isBulk: PropTypes.bool,
  fromComponent: PropTypes.string.isRequired,
};
AccountSelector.defaultProps = {
  pathfrom: '',
  checkedAccounts: {},
  userDetails: {},
  allChecked: false,
  accountsList: [],
  formloginIdsError: '',
  twitterChecked: false,
  socialMediaSelectorActive: false,
  searchedAccountsList: [],
  boostPostAllowedAccounts: [],
  isBulk: false,
};
export default AccountSelector;

/* eslint-disable consistent-return */
/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/role-has-required-aria-props */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Picky from 'react-picky';
import SPCheckbox from '../../SPCheckbox/SPCheckBox';
import getLocalizeText from '../../../../utils/functions/getLocalizeText';
import 'react-picky/dist/picky.css';

class SocialMediaSelector extends Component {
  constructor(props) {
    super(props);

    const opt = this.getOptions();
    this.state = {
      value: [],
      dropdownOpen: false,
      options: opt,
    };
  }

  getOptions = () => {
    let options = [
      { id: 1, name: 'Facebook' },
      { id: 2, name: 'Twitter' },
      { id: 3, name: 'LinkedIn' },
      { id: 4, name: 'Pinterest' },
      { id: 5, name: 'Google My Bussiness' },
      { id: 6, name: 'Instagram' },
      { id: 7, name: 'Tumblr' },
      { id: 8, name: 'VK' },
    ];

    const { fromComponent, isBulk } = this.props;

    if (fromComponent !== 'curation' && !isBulk) {
      options = [...options, { id: 9, name: 'TikTok' }];
    }
    return options;
  };

  // componentDidMount() {
  //   const { userDetails } = this.props;
  //   const bufferOp = { id: 9, name: 'Buffer' };
  //   // if (this.checkBufferAccountCondition(userDetails)) {
  //   //   let options = this.state.options;
  //   //   options.push(bufferOp);
  //   //   this.setState({
  //   //     options: [...options],
  //   //   });
  //   // }
  // }
  // checkBufferAccountCondition({ betaFeature, enableBuffer }) {
  //   const checkBetaFeature =
  //     betaFeature &&
  //     betaFeature.bufferAccount &&
  //     betaFeature.bufferAccount === 'Y';
  //   return checkBetaFeature || enableBuffer === 'Y';
  // }

  handleOnChange = value => {
    const { handleSocialMediaSelection } = this.props;
    this.setState(
      {
        value,
      },
      () => {
        const array = [];
        for (let i = 0; i < value.length; i += 1) {
          array[i] = value[i].name;
        }
        const strValue = array.join('|');
        let searchFilter = strValue;
        if (strValue.split('|').indexOf('Facebook') !== -1) {
          searchFilter = `${searchFilter}|users|calendar`;
        }
        if (strValue.split('|').indexOf('LinkedIn') !== -1) {
          searchFilter = `${searchFilter}|suitcase`;
        }
        if (strValue.split('|').indexOf('Google My Bussiness') !== -1) {
          searchFilter = `${searchFilter}|briefcase`;
        }
        if (strValue.split('|').indexOf('Buffer') !== -1) {
          searchFilter = `${searchFilter}|buffer`;
        }
        if (strValue.split('|').indexOf('TikTok') !== -1) {
          searchFilter = `${searchFilter}|tik-tok`;
        }
        handleSocialMediaSelection(searchFilter);
      },
    );
  };

  openDropDown = () => {
    this.setState({
      dropdownOpen: true,
    });
  };

  closeDropDown = () => {
    this.setState({
      dropdownOpen: false,
    });
  };

  render() {
    const { options, value, dropdownOpen } = this.state;
    const { hint } = this.props;
    return (
      <div className="section react_commonDropdown">
        <Picky
          value={value}
          placeholder={getLocalizeText('Filter by social platforms')}
          options={options}
          onChange={this.handleOnChange}
          valueKey="id"
          labelKey="name"
          multiple
          includeSelectAll={false}
          numberDisplayed={0}
          onOpen={this.openDropDown}
          onClose={this.closeDropDown}
          buttonProps={{
            class: `picky__input ${dropdownOpen ? 'InputDropdownOpen' : ''}`,
          }}
          dropdownHeight={400}
          className={value.length !== 0 ? 'abc' : ''}
          // renderSelectAll={({
          //   filtered,
          //   tabIndex,
          //   allSelected,
          //   toggleSelectAll,
          //   multiple,
          // }) => {
          //   if (multiple && !filtered) {
          //     return (
          //       <div
          //         tabIndex={tabIndex}
          //         role="presentation"
          //         className={`form-inline option accountSelectAll`}
          //         onClick={() => {
          //           toggleSelectAll();
          //         }}
          //       >
          //         <SPCheckbox
          //           checked={allSelected}
          //           onChange={() => {
          //             toggleSelectAll();
          //           }}
          //         />
          //         <span>{allSelected ? 'Deselect All' : 'Select All'}</span>
          //       </div>
          //     );
          //   }
          // }}
          render={({
            index,
            style,
            item,
            isSelected,
            selectValue,
            labelKey,
            valueKey,
            multiple,
          }) => (
            <li
              className={`accountItem ${isSelected ? 'selected' : ''}`}
              key={item[valueKey]}
            >
              <div className="">
                <SPCheckbox
                  id={item[valueKey]}
                  checked={isSelected}
                  onChange={() => {
                    selectValue(item);
                  }}
                >
                  <div className="copy_account_name copyToAccountIcon">
                    <span> {item[labelKey]}</span>
                  </div>
                </SPCheckbox>
              </div>
            </li>
          )}
        />
        <div className="hint">{hint}</div>
      </div>
    );
  }
}
SocialMediaSelector.propTypes = {
  handleSocialMediaSelection: PropTypes.func.isRequired,
  hint: PropTypes.string,
  fromComponent: PropTypes.string.isRequired,
  isBulk: PropTypes.bool,
};
SocialMediaSelector.defaultProps = {
  hint: '',
  isBulk: false,
};
export default SocialMediaSelector;

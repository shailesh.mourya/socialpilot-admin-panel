import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import PropTypes from 'prop-types';
// import AlertMsg from 'sharedComponents/pureComponent(stateless)/Alerts/Alert';
import AlertMsg from '../../../pureComponent(stateless)/Alerts/AlertMsg';
import DisplayText from '../../../lables/index';

import UserIcon from '../../../pureComponent(stateless)/UserIcon/UserIcon';
import SPCheckbox from '../../../pureComponent(stateless)/checkbox/SPCheckbox';
import './UserList.css';

export default class UserList extends Component {
  constructor(props) {
    super(props);
    this.count = 0;
    this.handleCheckedChange = this.handleCheckedChange.bind(this);
    this.loginIds = [];
    this.state = { listHeight: '524px' };
  }

  componentDidUpdate(prePops) {
    const { formloginIdsError } = this.props;
    if (prePops.formloginIdsError !== formloginIdsError) {
      let postErrorHeight = 0;
      if (formloginIdsError && window.$('.createpost-account-error').length) {
        postErrorHeight = window.$('.createpost-account-error').height();
      }

      const listHeight = `${524 - postErrorHeight}px`;
      this.setState({ listHeight });
    }
  }

  displayAccountName = str => {
    const stringArray = Array.from(str);

    if (stringArray.length < 30) {
      return str;
    }
    return `${stringArray.splice(0, 30).join('')}...`;
  };

  boostPostIcon = () => (
    <div className="account-boost-available align-self-center">
      <i className="fa fa-bullhorn" aria-hidden="true" />
    </div>
  );

  handleCheckedChange(id, checked) {
    const { markAccountAsChecked } = this.props;
    const IdData = id.replace('FeedCuration', '');
    markAccountAsChecked(checked, parseInt(IdData, 10));
  }

  extractListContent() {
    const {
      searchedAccountsList,
      list,
      pathfrom,
      twitterChecked,
      checkedList,
      boostPostAllowedAccounts,
      isBulk,
    } = this.props;
    const boostPostAccountLength = boostPostAllowedAccounts.length;
    const userAccountList = [];
    if (searchedAccountsList !== null) {
      for (let i = 0; i < searchedAccountsList.length; i += 1) {
        if (searchedAccountsList[i].isConnected === 'N') {
          userAccountList[i] = (
            <div
              className={` ${
                isBulk ? 'col-4' : ''
              } form-group reconnect-checkbox-block `}
              key={i}
            >
              <SPCheckbox className="account_select_icon">
                <div className="d-flex">
                  <div
                    className="createpost-reconnect"
                    title={
                      list[i].accountId === 6 && list[i].fbAppInstall === 'N'
                        ? 'Authorize'
                        : 'Reconnect'
                    }
                  >
                    <i className="fas fa-sync-alt" />
                  </div>
                  <UserIcon
                    user_name={searchedAccountsList[i].accountUserName}
                    profile_type={searchedAccountsList[i].accountType}
                    user_img_src={searchedAccountsList[i].profilePicture}
                  />
                  <span
                    className="account_name align-self-center"
                    title={searchedAccountsList[i].accountUserName}
                  >
                    {this.displayAccountName(
                      searchedAccountsList[i].accountUserName,
                    )}
                  </span>
                  {boostPostAccountLength > 0 &&
                    searchedAccountsList[i].accountId === 5 &&
                    boostPostAllowedAccounts.indexOf(
                      searchedAccountsList[i].loginId,
                    ) > -1 &&
                    this.boostPostIcon()}
                </div>
              </SPCheckbox>
            </div>
          );
        } else {
          userAccountList[i] = (
            <div className={`${isBulk ? 'col-4' : ''} form-group item`} key={i}>
              <SPCheckbox
                id={`${searchedAccountsList[i].loginId}${pathfrom}`}
                disabled={
                  searchedAccountsList[i].isDisabled === 'Y' ||
                  (twitterChecked &&
                    searchedAccountsList[i].accountType === 'twitter' &&
                    checkedList[searchedAccountsList[i].loginId] === false)
                }
                checked={checkedList[searchedAccountsList[i].loginId]}
                onChange={this.handleCheckedChange}
                className="account_select_icon"
              >
                <div className="d-flex">
                  <UserIcon
                    user_name={searchedAccountsList[i].accountUserName}
                    profile_type={searchedAccountsList[i].accountType}
                    user_img_src={searchedAccountsList[i].profilePicture}
                  />
                  <span
                    className="account_name align-self-center"
                    title={searchedAccountsList[i].accountUserName}
                  >
                    {this.displayAccountName(
                      searchedAccountsList[i].accountUserName,
                    )}
                  </span>
                  {boostPostAccountLength > 0 &&
                    searchedAccountsList[i].accountId === 5 &&
                    boostPostAllowedAccounts.indexOf(
                      searchedAccountsList[i].loginId,
                    ) > -1 &&
                    this.boostPostIcon()}
                </div>
              </SPCheckbox>
            </div>
          );
        }
      }
    } else {
      for (let i = 0; i < list.length; i += 1) {
        if (list[i].isConnected === 'N') {
          userAccountList[i] = (
            <div
              className={`${
                isBulk ? 'col-4' : ''
              } form-group reconnect-checkbox-block`}
              key={i}
            >
              <SPCheckbox className="account_select_icon">
                <div className="d-flex">
                  <div
                    className="createpost-reconnect"
                    title={
                      list[i].accountId === 6 && list[i].fbAppInstall === 'N'
                        ? 'Authorize'
                        : 'Reconnect'
                    }
                  >
                    <i className="fas fa-sync-alt" />
                  </div>
                  <UserIcon
                    user_name={list[i].accountUserName}
                    profile_type={list[i].accountType}
                    user_img_src={list[i].profilePicture}
                  />
                  <span
                    className="account_name align-self-center"
                    title={list[i].accountUserName}
                  >
                    {this.displayAccountName(list[i].accountUserName)}
                  </span>
                  {boostPostAccountLength > 0 &&
                    list[i].accountId === 5 &&
                    boostPostAllowedAccounts.indexOf(list[i].loginId) > -1 &&
                    this.boostPostIcon()}
                </div>
              </SPCheckbox>
            </div>
          );
        } else {
          userAccountList[i] = (
            <div className={`form-group item ${isBulk ? 'col-4' : ''}`} key={i}>
              <SPCheckbox
                id={`${list[i].loginId}${pathfrom}`}
                disabled={
                  list[i].isDisabled === 'Y' ||
                  (twitterChecked &&
                    list[i].accountType === 'twitter' &&
                    checkedList[list[i].loginId] === false)
                }
                checked={checkedList[list[i].loginId]}
                onChange={this.handleCheckedChange}
                className="account_select_icon"
              >
                <div className="d-flex">
                  <UserIcon
                    user_name={list[i].accountUserName}
                    profile_type={list[i].accountType}
                    user_img_src={list[i].profilePicture}
                  />
                  <span
                    className="account_name align-self-center"
                    title={list[i].accountUserName}
                  >
                    {this.displayAccountName(list[i].accountUserName)}
                  </span>

                  {boostPostAccountLength > 0 &&
                    list[i].accountId === 5 &&
                    boostPostAllowedAccounts.indexOf(list[i].loginId) > -1 &&
                    this.boostPostIcon()}
                </div>
              </SPCheckbox>
            </div>
          );
        }
      }
    }
    return userAccountList;
  }

  render() {
    const { isBulk } = this.props;
    const { listHeight } = this.state;
    const listContent = this.extractListContent();
    if (listContent.length === 0) {
      return (
        <div className="sunot-found-account">
          <center>
            <AlertMsg msg={DisplayText.NO_DATA_FOUND} type="warning" />
          </center>
        </div>
      );
    }
    return (
      <Scrollbars style={{ height: listHeight }} autoHeightMax={30}>
        <div className={`${isBulk ? 'row' : ''}`}>{listContent}</div>
      </Scrollbars>
    );
  }
}
UserList.propTypes = {
  formloginIdsError: PropTypes.string,
  pathfrom: PropTypes.string,
  markAccountAsChecked: PropTypes.func.isRequired,
  list: PropTypes.oneOfType([PropTypes.array]).isRequired,
  searchedAccountsList: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
  ]),
  twitterChecked: PropTypes.bool,
  checkedList: PropTypes.oneOfType([PropTypes.object]),
  boostPostAllowedAccounts: PropTypes.oneOfType([PropTypes.array]),
  isBulk: PropTypes.bool,
};
UserList.defaultProps = {
  formloginIdsError: '',
  pathfrom: '',
  searchedAccountsList: [],
  twitterChecked: false,
  checkedList: {},
  boostPostAllowedAccounts: [],
  isBulk: false,
};

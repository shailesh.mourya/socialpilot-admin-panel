/* eslint-disable camelcase */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import socialMediaAccount from '../../../../assets/social_media_account.png';
import './UserIcon.css';

export default class UserIcon extends Component {
  getUserAvatar() {
    const { user_img_src, user_name } = this.props;
    let avatar;
    if (user_img_src === '') {
      avatar = (
        <div className="post-user-icon user_icon_account hidden-sm float-left">
          <span className="account-first-alphabate">
            {user_name.charAt(0).toUpperCase()}
          </span>
          {this.getProfileIcon()}
        </div>
      );
    } else {
      avatar = (
        <div className="post-user-icon user_icon_account hidden-sm float-left">
          <img
            src={user_img_src}
            className="img-circle"
            alt="user_avatar"
            width="35px"
            height="35px"
            bsClass="hidden-sm"
            onError={e => {
              e.target.src = socialMediaAccount;
            }}
          />
          {this.getProfileIcon()}
        </div>
      );
    }
    return avatar;
  }

  getProfileIcon() {
    const { profile_type } = this.props;
    return <i className={`fa fa-${profile_type} ${profile_type}_round`} />;
  }

  render() {
    return this.getUserAvatar();
  }
}

UserIcon.propTypes = {
  user_name: PropTypes.string.isRequired,
  user_img_src: PropTypes.string,
  profile_type: PropTypes.string.isRequired,
};

UserIcon.defaultProps = {
  user_img_src: '',
};

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import GlobalDisplayTexts from '../../../../utils/labels/localizationText';

import SPCheckbox from '../../../pureComponent(stateless)/checkbox/SPCheckbox';

import './AccountSelectorHeader.scss';

class AccountSelectorHeader extends Component {
  constructor(props) {
    super(props);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
  }

  handleCheckboxChange(id, checked) {
    const { selectAllAccounts } = this.props;
    selectAllAccounts(checked);
  }

  render() {
    const { allChecked, labelCount, pathfrom } = this.props;
    return (
      <div className="row mb-2">
        <div className="col select-allcheckbox my-auto">
          <form>
            <SPCheckbox
              id={`select-all${pathfrom}`}
              value="select all"
              onChange={this.handleCheckboxChange}
              checked={allChecked}
            >
              <FormattedMessage
                id={GlobalDisplayTexts.SELECT_ALL_ACCOUNTS}
                defaultMessage={GlobalDisplayTexts.SELECT_ALL_ACCOUNTS}
              />
            </SPCheckbox>
          </form>
        </div>

        <div className="col-auto user_ac_header extraClass">
          <span className="numberCount">{labelCount}</span>
        </div>
      </div>
    );
  }
}

export default AccountSelectorHeader;

AccountSelectorHeader.propTypes = {
  // checkedAllAccounts: PropTypes.bool,
  pathfrom: PropTypes.string,
  labelCount: PropTypes.number,
  selectAllAccounts: PropTypes.func.isRequired,
  allChecked: PropTypes.bool.isRequired,
};

AccountSelectorHeader.defaultProps = {
  // checkedAllAccounts: false,
  labelCount: 0,
  pathfrom: '',
};

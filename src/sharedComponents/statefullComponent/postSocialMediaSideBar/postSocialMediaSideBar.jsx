import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import sort from 'fast-sort/sort.es5';
import GroupClientTabs from './GroupClientSelector/GroupClientTabs';
import AccountSelector from './AccountSelector/AccountSelector';
import DisplayText from '../../lables/index';

import { fetchGroupsListAction } from '../../../actions/group-actions/group.action';
import { fetchClientsListAction } from '../../../actions/client-actions/client.action';
import SPLoader from '../../pureComponent(stateless)/Loader/SPLoader';
import './postSocialMediaSideBar.scss';

class PostSocialMediaSideBar extends Component {
  constructor(props) {
    super(props);
    this.mapGroupsToAccounts = {};
    this.mapClientsToAccounts = {};
    this.accountInfoMap = {};
    this.defaultCheckRank = 999;
    this.state = this.initializeState();
    this.addCheckRankToAccountList();
    this.sortLists();
  }

  componentDidMount() {
    const {
      fetchGroupList,
      fetchClientList,
      userRole,
      checkedAccountsListInUpdate,
      groupList,
      clientList,
    } = this.props;

    if (groupList.status !== 'success') {
      fetchGroupList();
    } else {
      this.callAfterGroupClientApi();
    }

    if (userRole !== 'C' && clientList.status !== 'success') {
      fetchClientList();
    }

    if (clientList.status === 'success') {
      this.callAfterGroupClientApi();
    }

    this.createAccountInfoMapper();
    if (checkedAccountsListInUpdate.length > 0) {
      this.feedUpdateCheckedList();
    }
    this.validationForTikTok();
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      clientList,
      groupList,
      checkedAccountsListInUpdate,
      formsubmitting,
      accountsList,
    } = this.props;
    const {
      allChecked,
      socialMediaSelectorActive,
      groupApiCall,
      clientApiCall,
    } = this.state;

    this.validationForTikTok();
    // if (clientsList !== prevProps.clientsList) {
    //   this.createClientAccountMapper();
    // }
    if (
      accountsList !== prevProps.accountsList &&
      accountsList.length !== prevProps.accountsList.length
    ) {
      this.setState(
        state => {
          let currentState = { ...state };
          currentState = this.initializeState();
          return currentState;
        },
        () => {
          this.createAccountInfoMapper();
        },
      );
    }
    if (
      checkedAccountsListInUpdate !== prevProps.checkedAccountsListInUpdate &&
      formsubmitting !== true
    ) {
      this.feedUpdateCheckedList();
    }
    if (groupApiCall === false && groupList.status === 'success') {
      this.setState({
        groupApiCall: true,
        groupsList: groupList.successResponse.data.list,
      });
    }

    if (clientApiCall === false && clientList.status === 'success') {
      this.setState({
        clientApiCall: true,
        clientsList: clientList.successResponse.data.list,
      });
    }
    if (groupList !== prevProps.groupList && groupList.status === 'success') {
      this.callAfterGroupClientApi();
    }

    if (
      clientList !== prevProps.clientList &&
      clientList.status === 'success'
    ) {
      this.callAfterGroupClientApi();
    }

    if (prevState.socialMediaSelectorActive !== socialMediaSelectorActive) {
      if (allChecked) {
        this.handleSelectAllCheckBox();
      }
    }
  }

  validationForInstaPintrest = () => {
    const { originalPostType, originalPostData } = this.props;
    const { instaPinterestDisable } = this.state;
    let isDisable = true;

    if (originalPostData) {
      const { images, multipleImages } = originalPostData;

      if (
        originalPostType === 'video' ||
        multipleImages.length >= 1 ||
        (originalPostType === 'article' && images.length >= 1)
      ) {
        isDisable = false;
      }

      const disableArray = [];
      disableArray[14] = isDisable ? 'Y' : 'N';
      disableArray[15] = isDisable ? 'Y' : 'N';

      if (instaPinterestDisable !== isDisable) {
        this.accountDisabledHandler(disableArray);
        this.setState({ instaPinterestDisable: isDisable });
      }
    }
  };

  validationForTikTok = () => {
    const { originalPostType, originalPostData } = this.props;
    const { tikTokDisable } = this.state;
    let isTikTokDisable = true;

    if (originalPostData) {
      const { videoExtraData } = originalPostData;
      if (
        (originalPostType === 'image' || originalPostType === 'carousel') &&
        originalPostData.multipleImages.length > 1
      ) {
        isTikTokDisable = false;
      } else if (
        originalPostType === 'video' &&
        videoExtraData &&
        videoExtraData.durationSec >= 3 &&
        videoExtraData.mimeType === 'video/mp4' &&
        videoExtraData.videoFrames <= 1100
      ) {
        isTikTokDisable = false;
      }
      if (originalPostData.counter > 150) {
        isTikTokDisable = true;
      }
      const isDisable = [];
      isDisable[24] = isTikTokDisable ? 'Y' : 'N';
      if (tikTokDisable !== isTikTokDisable) {
        this.accountDisabledHandler(isDisable);
        this.setState({ tikTokDisable: isTikTokDisable });
      }
      this.validationForInstaPintrest();
    }
  };

  displayBoostPostIcon = loginIds => {
    const boostPostErrorMessage =
      loginIds.length === 0 ? DisplayText.BOOST_POST_NOT_AVAILABLE : '';

    this.setState(state => {
      const currentState = { ...state };
      currentState.boostPostAllowedAccounts = loginIds;
      currentState.boostPostErrorMessage = boostPostErrorMessage;
      sort(currentState.accountsList).by([
        { asc: 'checkRank' },
        {
          desc: account =>
            currentState.checkedAccounts[account.loginId] === true,
        },
        { desc: account => loginIds.indexOf(account.loginId) > -1 },

        { asc: account => account.accountUserName.toLowerCase() },
      ]);
      return currentState;
    });
  };

  removeBoostPostIcon = () => {
    this.setState({ boostPostAllowedAccounts: [], boostPostErrorMessage: '' });
  };

  getCheckedAccounts = () => {
    const accounts = [];
    const { checkedAccounts } = this.state;
    Object.entries(checkedAccounts).forEach(([key, value]) => {
      if (value === true) {
        accounts.push(key);
      }
    });
    return accounts;
  };

  resetChecked = () => {
    const accounts = {};
    const { checkedAccounts } = { ...this.state };

    Object.entries(checkedAccounts).forEach(([key]) => {
      accounts[key] = false;
    });

    this.setState({
      checkedAccounts: accounts,
      twitterChecked: false,
      allChecked: false,
    });
  };

  setSelectedGroupClientAccountsList = (id, tab, checked) => {
    const { featureUsedHandler } = this.props;
    let map;
    try {
      switch (tab) {
        case 'groups':
          map = this.mapGroupsToAccounts;
          break;

        case 'clients':
          map = this.mapClientsToAccounts;
          break;

        default:
          map = null;
      }
      const groupClientAccounts = map[id];
      this.setState(
        state => {
          const currentState = { ...state };
          if (tab === 'groups') {
            currentState.checkedGroups[id] = checked;
          } else if (tab === 'clients') {
            currentState.checkedClients[id] = checked;
          }
          if (groupClientAccounts) {
            if (checked) {
              currentState.currentGroupAndClientCheckRank += 1;
              for (let i = 0; i < groupClientAccounts.length; i += 1) {
                if (
                  this.accountInfoMap[groupClientAccounts[i]] &&
                  this.accountInfoMap[groupClientAccounts[i]].isConnected ===
                    'Y' &&
                  this.accountInfoMap[groupClientAccounts[i]].isDisabled === 'N'
                ) {
                  if (
                    this.accountInfoMap[groupClientAccounts[i]].checkRank ===
                    this.defaultCheckRank
                  ) {
                    this.accountInfoMap[groupClientAccounts[i]].checkRank =
                      currentState.currentGroupAndClientCheckRank;
                    if (
                      currentState.checkedAccounts[groupClientAccounts[i]] ===
                      false
                    ) {
                      if (
                        this.accountInfoMap[groupClientAccounts[i]]
                          .accountType === 'twitter'
                      ) {
                        if (currentState.twitterChecked === false) {
                          currentState.checkedAccounts[
                            groupClientAccounts[i]
                          ] = true;
                          this.accountInfoMap[
                            groupClientAccounts[i]
                          ].checked = true;
                          currentState.twitterChecked = true;
                        }
                      } else {
                        currentState.checkedAccounts[
                          groupClientAccounts[i]
                        ] = true;
                        this.accountInfoMap[
                          groupClientAccounts[i]
                        ].checked = true;
                      }
                    }
                  }
                }
              }
              currentState.accountsList = Object.values(this.accountInfoMap);
              sort(currentState.accountsList).by([
                { asc: 'checkRank' },
                { desc: account => account.checked === true },
                { asc: account => account.accountUserName.toLowerCase() },
              ]);
            } else {
              currentState.currentGroupAndClientCheckRank -= 1;
              for (let i = 0; i < groupClientAccounts.length; i += 1) {
                if (
                  this.accountInfoMap[groupClientAccounts[i]] &&
                  this.accountInfoMap[groupClientAccounts[i]].isConnected ===
                    'Y'
                ) {
                  this.accountInfoMap[
                    groupClientAccounts[i]
                  ].checkRank = this.defaultCheckRank;
                  if (
                    this.accountInfoMap[groupClientAccounts[i]].accountType ===
                      'twitter' &&
                    currentState.checkedAccounts[groupClientAccounts[i]] ===
                      true
                  ) {
                    currentState.twitterChecked = false;
                  }
                  currentState.checkedAccounts[groupClientAccounts[i]] = false;
                  this.accountInfoMap[groupClientAccounts[i]].checked = false;
                }
              }
              currentState.accountsList = Object.values(this.accountInfoMap);
              sort(currentState.accountsList).by([
                { asc: 'checkRank' },
                { desc: account => account.checked === true },
                { asc: account => account.accountUserName.toLowerCase() },
              ]);
            }
          }
          return currentState;
        },
        () => {
          const { socialMediaSelectorActive, lastFilter } = this.state;
          if (socialMediaSelectorActive && lastFilter !== null) {
            this.handleSocialMediaFilter(lastFilter);
          }
          this.handleSelectAllCheckBox();
        },
      );

      if (featureUsedHandler) {
        const feature = tab === 'groups' ? 'filterByGroup' : 'filterByClient';
        featureUsedHandler(feature);
      }
    } catch (error) {
      // console.error(error);
    }
  };

  initializeState = () => {
    const checkedAccounts = {};
    const checkedGroups = {};
    const checkedClients = {};
    let allChecked = false;
    const clientsList = {};
    const groupsList = {};
    let accList = [];
    const { accountsList, fromComponent, isBulk } = this.props;

    if (accountsList.length === 1) {
      if (accountsList[0].isConnected === 'Y') {
        checkedAccounts[accountsList[0].loginId] = true;
        allChecked = true;
      } else {
        checkedAccounts[accountsList[0].loginId] = false;
        allChecked = false;
      }
    } else {
      for (let i = 0; i < accountsList.length; i += 1) {
        checkedAccounts[accountsList[i].loginId] = false;

        // This condition is for removing tiktok accounts(id = 24) from filter
        // for feeds and content curation pages
        if (
          (fromComponent === 'curation' || isBulk) &&
          accountsList[i].accountId !== 24
        ) {
          accList.push(accountsList[i]);
        }
      }
    }
    if (accList.length === 0) {
      accList = accountsList;
    }

    return {
      checkedAccounts,
      checkedGroups,
      checkedClients,
      clientsList,
      clientApiCall: false,
      groupsList,
      groupApiCall: false,
      selectedAllAccounts: true,
      searchModeGroup: false,
      searchModeClient: false,
      currentGroupAndClientCheckRank: 0,
      socialMediaSelectorActive: false,
      searchedGroupsList: null,
      searchedClientsList: null,
      searchedAccountsList: null,
      networkFilteredList: null,
      twitterChecked: false,
      backUpSearchFilterList: null,
      backUpAccountFilterList: null,
      lastSearchTerm: null,
      lastFilter: null,
      allChecked,
      accountsList: accList,
      boostPostAllowedAccounts: [],
      boostPostErrorMessage: '',
      tikTokDisable: false,
      instaPinterestDisable: false,
    };
  };

  handleSocialMediaFilter = (socialMediaFilter = '') => {
    const { featureUsedHandler } = this.props;
    try {
      this.setState(state => {
        const currentState = { ...state };
        if (socialMediaFilter === '') {
          currentState.lastFilter = null;
          if (currentState.searchedAccountsList !== null) {
            const regExp = new RegExp(`${currentState.lastSearchTerm}`, 'gi');
            const tempAccountsList = Object.values(this.accountInfoMap);
            currentState.searchedAccountsList = tempAccountsList.filter(
              listItem => listItem.accountUserName.match(regExp),
            );
            sort(currentState.searchedAccountsList).by([
              { asc: 'checkRank' },
              { desc: account => account.checked === true },
              { asc: account => account.accountUserName.toLowerCase() },
            ]);
          } else {
            const tempAccountsList = Object.values(this.accountInfoMap);
            currentState.accountsList = tempAccountsList;
            sort(currentState.accountsList).by([
              { asc: 'checkRank' },
              { desc: account => account.checked === true },
              { asc: account => account.accountUserName.toLowerCase() },
            ]);
          }
          currentState.socialMediaSelectorActive = false;
          currentState.backUpSearchFilterList = null;
        } else {
          currentState.lastFilter = socialMediaFilter;
          const regexp = new RegExp(`${socialMediaFilter}`, 'gi');
          if (currentState.searchedAccountsList !== null) {
            const filterName = currentState.lastSearchTerm;
            const regexpFilter = new RegExp(
              // eslint-disable-next-line no-useless-escape
              `${filterName.replace(/([.*+?^${}()|\[\]\/\\])/g, '\\$1')}`,
              'gi',
            );
            const tempAccountsList = Object.values(this.accountInfoMap);
            currentState.accountsList = tempAccountsList.filter(listItem =>
              listItem.accountType.match(regexp),
            );
            currentState.backUpAccountFilterList = currentState.accountsList;
            currentState.searchedAccountsList = currentState.backUpAccountFilterList.filter(
              listItem => listItem.accountUserName.match(regexpFilter),
            );

            sort(currentState.searchedAccountsList).by([
              { desc: account => account.checked === true },
              { asc: 'checkRank' },
              { asc: account => account.accountUserName.toLowerCase() },
            ]);
          } else {
            const tempAccountsList = Object.values(this.accountInfoMap);
            currentState.accountsList = tempAccountsList.filter(listItem =>
              listItem.accountType.match(regexp),
            );
            sort(currentState.accountsList).by([
              { asc: 'checkRank' },
              { desc: account => account.checked === true },
              { asc: account => account.accountUserName.toLowerCase() },
            ]);
            currentState.backUpSearchFilterList = null;
          }
          currentState.socialMediaSelectorActive = true;
        }
        return currentState;
      });
      if (featureUsedHandler) {
        featureUsedHandler('filterByPlatform');
      }
    } catch (error) {
      // console.error(error);
    }
  };

  handleSearch = (ListType, searchFilter = '') => {
    let searchedList = null;
    let filterList = null;
    let searchVariable = null;
    const { fromComponent, isBulk } = this.props;
    switch (ListType) {
      case 'groups':
        searchedList = 'searchedGroupsList';
        filterList = 'groupsList';
        searchVariable = 'name';
        break;

      case 'clients':
        searchedList = 'searchedClientsList';
        filterList = 'clientsList';
        searchVariable = 'name';
        break;

      case 'accounts':
        searchedList = 'searchedAccountsList';
        filterList = 'accountsList';
        searchVariable = 'accountUserName';
        break;

      default:
        searchedList = null;
        filterList = null;
        searchVariable = null;
    }
    this.setState(state => {
      const currentState = { ...state };
      if (searchFilter.trim() === '') {
        currentState.lastSearchTerm = null;
        currentState[searchedList] = null;
        if (currentState.socialMediaSelectorActive === true) {
          const regExp = new RegExp(`${currentState.lastFilter}`, 'gi');
          const tempAccountsList = Object.values(this.accountInfoMap);
          currentState.accountsList = tempAccountsList.filter(listItem =>
            listItem.accountType.match(regExp),
          );
          sort(currentState.accountsList).by([
            { asc: 'checkRank' },
            { desc: account => account.checked === true },
            { asc: account => account.accountUserName.toLowerCase() },
          ]);
        }
        if (ListType === 'groups') {
          currentState.searchModeGroup = false;
        } else if (ListType === 'clients') {
          currentState.searchModeClient = false;
        }
        sort(currentState.accountsList).by([
          { asc: 'checkRank' },
          {
            desc: account =>
              currentState.checkedAccounts[account.loginId] === true,
          },
          { asc: account => account.accountUserName.toLowerCase() },
        ]);
        currentState.searchedAccountsList = null;
        currentState.backUpAccountFilterList = null;
      } else {
        currentState.lastSearchTerm = searchFilter;
        const regexp = new RegExp(
          // eslint-disable-next-line no-useless-escape
          `${searchFilter.replace(/([.*+?^${}()|\[\]\/\\])/g, '\\$1')}`,
          'gi',
        );
        if (ListType === 'accounts') {
          if (currentState.socialMediaSelectorActive) {
            if (currentState.backUpAccountFilterList === null) {
              currentState.backUpAccountFilterList = currentState.accountsList;
            }
            currentState.searchedAccountsList = currentState.backUpAccountFilterList.filter(
              listItem => listItem.accountUserName.match(regexp),
            );
          } else {
            const tempAccountsList = Object.values(this.accountInfoMap);
            let accList = tempAccountsList;
            if (fromComponent === 'curation' || isBulk) {
              accList = tempAccountsList.filter(
                listItem => listItem.accountId !== 24,
              );
            }
            currentState.searchedAccountsList = accList.filter(listItem =>
              listItem.accountUserName.match(regexp),
            );
            currentState.backUpAccountFilterList = null;
          }
          sort(currentState.searchedAccountsList).by([
            { asc: 'checkRank' },
            { desc: account => account.checked === true },
            { asc: account => account.accountUserName.toLowerCase() },
          ]);
        } else {
          // eslint-disable-next-line react/destructuring-assignment
          currentState[searchedList] = state[filterList].filter(listItem =>
            listItem[searchVariable].match(regexp),
          );
        }
        if (ListType === 'groups') {
          currentState.searchModeGroup = true;
        } else if (ListType === 'clients') {
          currentState.searchModeClient = true;
        }
      }
      return currentState;
    });
  };

  sortLists = () => {
    // eslint-disable-next-line react/prop-types
    const { groupsList, clientsList, accountsList } = this.props;
    sort(groupsList).asc([group => group.name.toLowerCase()]);
    sort(clientsList).asc([client => client.name.toLowerCase()]);
    sort(accountsList).asc([
      'checkRank',
      account => account.accountUserName.toLowerCase(),
    ]);
  };

  addCheckRankToAccountList = () => {
    const { accountsList } = this.props;
    const accountsListlength = accountsList.length;
    for (let i = 0; i < accountsListlength; i += 1) {
      accountsList[i] = {
        ...accountsList[i],
        checkRank:
          accountsList[i].isConnected === 'Y' ? this.defaultCheckRank : -1,
        checked: false,
      };
    }
  };

  markAccountAsChecked = (checked, id) => {
    try {
      this.setState(
        state => {
          const currentState = { ...state };
          if (checked) {
            if (this.accountInfoMap[id].accountType === 'twitter') {
              if (currentState.twitterChecked === false) {
                currentState.checkedAccounts[id] = checked;
                this.accountInfoMap[id].checked = true;
                currentState.twitterChecked = true;
              }
            } else {
              currentState.checkedAccounts[id] = checked;
              this.accountInfoMap[id].checked = true;
            }
          } else {
            currentState.checkedAccounts[id] = checked;
            this.accountInfoMap[id].checked = false;
            if (this.accountInfoMap[id].accountType === 'twitter') {
              currentState.twitterChecked = false;
            }
          }
          return currentState;
        },
        () => this.handleSelectAllCheckBox(),
      );
    } catch (error) {
      // console.error(error);
    }
  };

  /** This select all the accounts object to map groupIds to accountsIds */

  selectAllAccounts = checked => {
    try {
      // eslint-disable-next-line react/prop-types
      const { accountsList, videoTitleDisableHandler } = this.props;

      this.setState(state => {
        const currentState = { ...state };
        let list = null;
        if (currentState.socialMediaSelectorActive) {
          list = currentState.accountsList;
        } else if (currentState.searchedAccountsList !== null) {
          list = currentState.searchedAccountsList;
        } else if (currentState.selectedAllAccounts === true) {
          list = accountsList;
          currentState.selectedAllAccounts = false;
        } else {
          list = accountsList;
          currentState.selectedAllAccounts = !currentState.selectedAllAccounts;
        }
        let checkedFbAccounts = 0;
        if (checked) {
          for (let i = 0; i < list.length; i += 1) {
            const checkIsDisable = !!(
              this.accountInfoMap &&
              this.accountInfoMap[list[i].loginId] &&
              this.accountInfoMap[list[i].loginId].isDisabled === 'Y'
            );

            if (list[i].isConnected === 'Y' && !checkIsDisable) {
              if (
                list[i].accountType === 'users' ||
                list[i].accountType === 'facebook-official'
              ) {
                checkedFbAccounts += 1;
              }

              if (list[i].accountType === 'twitter') {
                if (currentState.twitterChecked === false) {
                  currentState.checkedAccounts[list[i].loginId] = true;
                  this.accountInfoMap[list[i].loginId].checked = true;
                  currentState.twitterChecked = true;
                }
              } else {
                currentState.checkedAccounts[list[i].loginId] = true;
                this.accountInfoMap[list[i].loginId].checked = true;
              }
            }
          }
          currentState.allChecked = true;
        } else {
          for (let i = 0; i < list.length; i += 1) {
            currentState.checkedAccounts[list[i].loginId] = false;
            this.accountInfoMap[list[i].loginId].checked = false;
            currentState.twitterChecked = false;
          }
          currentState.allChecked = false;
        }

        if (videoTitleDisableHandler) {
          videoTitleDisableHandler(checkedFbAccounts);
        }
        return currentState;
      });
    } catch (error) {
      // console.error(error);
    }
  };

  accountDisabledHandler = accountDisabledArray => {
    const previewsState = { ...this.state };
    const { accountsList, checkedAccounts, twitterChecked } = previewsState;
    let preTwitterChecked = twitterChecked;
    for (let i = 0; i < accountsList.length; i += 1) {
      const { loginId, accountId } = accountsList[i];

      const isDisabled = accountDisabledArray[accountId];
      const checkedloginId = checkedAccounts[loginId];
      const accountInfoMapLogin = this.accountInfoMap[loginId];

      if (typeof isDisabled !== 'undefined') {
        if (
          twitterChecked === true &&
          isDisabled === 'N' &&
          accountId === 1 &&
          checkedloginId === false
        ) {
          accountsList[i].isDisabled = 'Y';
        } else {
          accountsList[i].isDisabled = isDisabled;
        }

        if (typeof accountInfoMapLogin !== 'undefined') {
          this.accountInfoMap[loginId].isDisabled = isDisabled;
        }

        if (preTwitterChecked === true && isDisabled === 'Y') {
          this.twitterChecked = false;
          preTwitterChecked = false;
        }

        if (
          isDisabled === 'Y' &&
          typeof checkedloginId !== 'undefined' &&
          checkedloginId === true
        ) {
          checkedAccounts[accountsList[i].loginId] = false;
        }
      }
    }

    this.setState({
      accountsList,
      checkedAccounts,
      twitterChecked: preTwitterChecked,
    });
  };

  directlyCheckedByRef = checkedAccountsListInUpdate => {
    if (checkedAccountsListInUpdate.length > 0) {
      try {
        const checkedAccountsListInUpdateLength =
          checkedAccountsListInUpdate.length;
        if (checkedAccountsListInUpdate.length > 0) {
          this.setState(state => {
            const currentState = { ...state };
            for (let i = 0; i < checkedAccountsListInUpdateLength; i += 1) {
              const loginId = parseInt(checkedAccountsListInUpdate[i], 10);
              if (
                this.accountInfoMap[loginId] &&
                this.accountInfoMap[loginId].isConnected === 'Y'
              ) {
                currentState.checkedAccounts[loginId] = true;
                if (this.accountInfoMap[loginId].accountType === 'twitter') {
                  currentState.twitterChecked = true;
                }
              }
            }
            return currentState;
          });
        }
      } catch (error) {
        // console.error(error);
      }
    }
  };

  feedUpdateCheckedList() {
    try {
      const { checkedAccountsListInUpdate } = this.props;
      const checkedAccountsListInUpdateLength =
        checkedAccountsListInUpdate.length;
      if (checkedAccountsListInUpdate.length > 0) {
        this.setState(
          state => {
            const currentState = { ...state };
            for (let i = 0; i < checkedAccountsListInUpdateLength; i += 1) {
              const loginId = parseInt(checkedAccountsListInUpdate[i], 10);
              if (
                this.accountInfoMap[loginId] &&
                this.accountInfoMap[loginId].isConnected === 'Y'
              ) {
                this.accountInfoMap[loginId].checked = true;
                currentState.checkedAccounts[loginId] = true;
                if (this.accountInfoMap[loginId].accountType === 'twitter') {
                  currentState.twitterChecked = true;
                }
              }
            }
            // eslint-disable-next-line array-callback-return
            Object.keys(currentState.checkedAccounts).map(key => {
              const loginIdFound = checkedAccountsListInUpdate.find(
                element => element === key,
              );
              if (!loginIdFound) {
                currentState.checkedAccounts[key] = false;
              }
            });
            return currentState;
          },
          () => {
            const { checkedAccounts } = this.state;
            if (
              Object.keys(this.accountInfoMap).length ===
              Object.keys(checkedAccounts).length
            ) {
              this.handleSelectAllCheckBox();
            }
          },
        );
      }
    } catch (error) {
      // console.error(error);
    }
  }

  /** This method creates a object to map groupIds to accountsIds  */
  createGroupAccountMapper() {
    try {
      const { groupList } = this.props;
      if (groupList.status === 'success') {
        const groupsList = groupList.successResponse.data.list;
        let i = 0;
        let j = 0;
        let accountIds;
        for (i = 0; i < groupsList.length; i += 1) {
          accountIds = [];
          for (j = 0; j < groupsList[i].accountsList.length; j += 1) {
            accountIds.push(groupsList[i].accountsList[j].loginId);
          }
          if (accountIds.length) {
            this.mapGroupsToAccounts[groupsList[i].id] = accountIds;
          }
        }
      }
    } catch (error) {
      // console.error(error);
    }
  }

  /** This method creates an object to map clientIds to accountIds */
  createClientAccountMapper() {
    try {
      const { clientList } = this.props;

      if (clientList.status === 'success') {
        const clientsList = clientList.successResponse.data.list;
        let i = 0;
        let j = 0;
        let accountIds;
        for (i = 0; i < clientsList.length; i += 1) {
          accountIds = [];
          for (j = 0; j < clientsList[i].accountsList.length; j += 1) {
            accountIds.push(clientsList[i].accountsList[j]);
          }

          if (accountIds.length) {
            this.mapClientsToAccounts[clientsList[i].id] = accountIds;
          }
        }
      }
    } catch (error) {
      // console.error(error);
    }
  }

  handleSelectAllCheckBox() {
    let atleastOneIsUnchecked = false;
    let checkedTwitterAccounts = 0;
    let checkedFbAccounts = 0;
    const { twitterChecked } = this.state;
    // eslint-disable-next-line react/prop-types
    const { videoTitleDisableHandler } = this.props;
    const accountsListArr = Object.entries(this.accountInfoMap).map(
      account => account[1],
    );

    for (let i = 0; i < accountsListArr.length; i += 1) {
      if (accountsListArr[i].accountType === 'twitter') {
        if (accountsListArr[i].checked === true) {
          // eslint-disable-next-line no-plusplus
          checkedTwitterAccounts++;
        } else if (checkedTwitterAccounts === 0) {
          atleastOneIsUnchecked = true;
        }
      } else if (
        accountsListArr[i].isConnected === 'Y' &&
        accountsListArr[i].accountId !== 24
      ) {
        if (!accountsListArr[i].checked) {
          atleastOneIsUnchecked = true;
        }
      }
      if (
        (accountsListArr[i].accountType === 'users' ||
          accountsListArr[i].accountType === 'facebook-official') &&
        accountsListArr[i].checked === true
      ) {
        checkedFbAccounts += 1;
      }
    }

    if (videoTitleDisableHandler) {
      videoTitleDisableHandler(checkedFbAccounts);
    }
    if (atleastOneIsUnchecked === false) {
      if ((twitterChecked && checkedTwitterAccounts === 1) || !twitterChecked) {
        this.setState({ allChecked: true });
      } else {
        this.setState({ allChecked: false });
      }
    } else {
      this.setState({ allChecked: false });
    }
  }

  createAccountInfoMapper() {
    const { accountsList } = this.props;
    for (let i = 0; i < accountsList.length; i += 1) {
      this.accountInfoMap[accountsList[i].loginId] = {
        ...accountsList[i],
        checkRank:
          accountsList[i].isConnected === 'Y' ? this.defaultCheckRank : -1,
        checked: false,
      };
    }
  }

  callAfterGroupClientApi() {
    const { groupList, clientList } = this.props;
    const checkedGroups = {};
    const checkedClients = {};
    if (groupList.status === 'success') {
      const groupsList = groupList.successResponse.data.list;

      this.createGroupAccountMapper();
      for (let i = 0; i < groupsList.length; i += 1) {
        checkedGroups[groupsList[i].id] = false;
      }
      this.setState({ checkedGroups });
    }

    if (clientList.status === 'success') {
      const clientsList = clientList.successResponse.data.list;
      this.createClientAccountMapper();
      for (let i = 0; i < clientsList.length; i += 1) {
        checkedClients[clientsList[i].id] = false;
      }
      this.setState({ checkedClients });
    }
  }

  render() {
    const {
      userRole,
      locale,
      formloginIdsError,
      userDetails,
      pathfrom,
      isBulk,
      fromComponent,
    } = this.props;

    const {
      searchedGroupsList,
      searchedClientsList,
      searchModeGroup,
      searchModeClient,
      checkedGroups,
      checkedClients,
      searchedAccountsList,
      accountsList,
      checkedAccounts,
      socialMediaSelectorActive,
      twitterChecked,
      allChecked,
      clientsList,
      groupsList,
      groupApiCall,
      clientApiCall,
      boostPostAllowedAccounts,
      boostPostErrorMessage,
    } = this.state;
    const loginIdsError = formloginIdsError || boostPostErrorMessage;
    return (
      <div className="row">
        <div className={`col-lg-${isBulk ? '4' : '6'} col-sm-6 pr-0 mobile-pr`}>
          {(userRole === 'C' || clientApiCall === true) &&
          groupApiCall === true ? (
            <GroupClientTabs
              groupsList={groupsList}
              clientsList={clientsList}
              searchList={this.handleSearch}
              searchedGroupList={searchedGroupsList}
              searchedClientList={searchedClientsList}
              searchModeGroup={searchModeGroup}
              searchModeClient={searchModeClient}
              checkedGroups={checkedGroups}
              checkedClients={checkedClients}
              setSelectedAccountsList={this.setSelectedGroupClientAccountsList}
              userRole={userRole}
              locale={locale}
              pathfrom={pathfrom}
              isBulk={isBulk}
            />
          ) : (
            <div className="text-center pt-5">
              <SPLoader
                type="scale"
                loading
                color="#0f67ea"
                height={20}
                width={4}
                radius={2}
                margin="2px"
              />
            </div>
          )}
        </div>
        <div
          className={`col-lg-${
            isBulk ? '8' : '6'
          } col-sm-6 post_account_select create-post-account-selction pl-0`}
        >
          <AccountSelector
            locale={locale}
            searchedAccountsList={searchedAccountsList}
            accountsList={accountsList}
            handleSocialMediaFilter={this.handleSocialMediaFilter}
            searchList={this.handleSearch}
            formloginIdsError={loginIdsError}
            checkedAccounts={checkedAccounts}
            selectAllAccounts={this.selectAllAccounts}
            markAccountAsChecked={this.markAccountAsChecked}
            socialMediaSelectorActive={socialMediaSelectorActive}
            twitterChecked={twitterChecked}
            allChecked={allChecked}
            userDetails={userDetails}
            pathfrom={pathfrom}
            boostPostAllowedAccounts={boostPostAllowedAccounts}
            isBulk={isBulk}
            fromComponent={fromComponent}
          />
        </div>
      </div>
    );
  }
}

PostSocialMediaSideBar.defaultProps = {
  checkedAccountsListInUpdate: [],
  originalPostType: '',
  originalPostData: {},
};

PostSocialMediaSideBar.propTypes = {
  accountsList: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.array,
  ]).isRequired,

  groupList: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
    .isRequired,
  clientList: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
    .isRequired,
  clientsList: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
    .isRequired,
  formloginIdsError: PropTypes.string,
  fetchClientList: PropTypes.func.isRequired,
  locale: PropTypes.string.isRequired,
  userRole: PropTypes.string.isRequired,
  userDetails: PropTypes.oneOfType([PropTypes.string, PropTypes.object])
    .isRequired,
  checkedAccountsListInUpdate: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.object,
  ]),
  fetchGroupList: PropTypes.func.isRequired,
  pathfrom: PropTypes.string,
  formsubmitting: PropTypes.bool,
  originalPostType: PropTypes.string,
  originalPostData: PropTypes.oneOfType([PropTypes.object]),
  isBulk: PropTypes.bool,
  fromComponent: PropTypes.string,
  featureUsedHandler: PropTypes.func,
};

PostSocialMediaSideBar.defaultProps = {
  formloginIdsError: '',
  pathfrom: '',
  formsubmitting: false,
  isBulk: false,
  fromComponent: '',
  featureUsedHandler: () => {},
};

const mapStateToProps = state => ({
  groupList: state.groupList,
  clientList: state.clientList,
});

const mapDispatchToProps = dispatch => ({
  fetchGroupList: (limit = 10000, pageNo = 1) => {
    dispatch(fetchGroupsListAction(limit, pageNo));
  },
  fetchClientList: (limit = 10000, pageNo = 1) => {
    dispatch(fetchClientsListAction(limit, pageNo));
  },
});
export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(PostSocialMediaSideBar);

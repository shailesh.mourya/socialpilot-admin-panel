import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import PropTypes from 'prop-types';
import { AlertMsg } from '../../pureComponent(stateless)/Alerts/Alert';
import history from '../../../history';

import {
  showAlert,
  cancelAlert,
} from '../../../actions/flashMessage/falshMessage.action';

const FlashMessage = ({
  message,
  disableAlert,
  layout,
  componentAlert,
  disableClose,
  showALertAction,
}) => {
  // eslint-disable-next-line no-shadow
  const [showAlert, setShowAlert] = useState(false);
  const locationState = history.location.state;

  useEffect(() => {
    if (message.showAlert) {
      setShowAlert(true);
    } else {
      setShowAlert(false);
    }
  }, [message.showAlert]);

  useEffect(
    () => {
      if (locationState && locationState.alert) {
        showALertAction({
          ...locationState.alert,
        });
        history.replace({ ...history.location, state: null });
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [locationState],
  );

  const handleCloseAlert = () => {
    setShowAlert(false);
    disableAlert();
  };

  const AlertMessageUI = (type, alertMessage, onDisableClose) => (
    <AlertMsg
      type={type}
      msg={alertMessage}
      onClick={handleCloseAlert}
      disableClose={onDisableClose}
    />
  );

  if (!message.componentAlertMessage && componentAlert) {
    return null;
  }

  if (message.componentAlertType && componentAlert) {
    return AlertMessageUI(
      message.componentAlertType,
      message.componentAlertMessage,
      disableClose,
    );
  }
  if (showAlert || (componentAlert && message.message)) {
    if (layout) {
      return (
        <div className={layout}>
          {AlertMessageUI(message.type, message.message, disableClose)}
        </div>
      );
    }
    return AlertMessageUI(message.type, message.message, disableClose);
  }
  return null;
};

const mapStateToProps = state => ({
  message: state.flashMesssage,
});

const mapDispatchToProps = dispatch => ({
  showALertAction: obj => {
    dispatch(showAlert(obj));
  },
  disableAlert: () => {
    dispatch(cancelAlert());
  },
});

const Component = withRouter(FlashMessage);
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(React.memo(Component));

FlashMessage.propTypes = {
  disableAlert: PropTypes.func.isRequired,
  message: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
    PropTypes.bool,
  ]).isRequired,
  showAlert: PropTypes.func,
  componentAlert: PropTypes.bool,
  layout: PropTypes.string,
  disableClose: PropTypes.bool,
  showALertAction: PropTypes.func.isRequired,
};
FlashMessage.defaultProps = {
  showAlert: () => {},
  componentAlert: false,
  disableClose: false,
  layout: '',
};

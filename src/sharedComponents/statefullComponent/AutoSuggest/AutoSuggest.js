import React from 'react';
import Autosuggest from 'react-autosuggest';
import PropTypes from 'prop-types';
import './AutoSuggest.scss';

class AutoSuggest extends React.Component {
  constructor() {
    super();

    this.state = {
      value: '',
      suggestions: [],
    };
  }

  componentDidUpdate(prevprop) {
    const { inputText } = this.props;
    if (inputText !== prevprop.inputText && !inputText) {
      this.setState({
        value: '',
      });
    }
  }

  escapeRegexCharacters = str => str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');

  getSuggestions = value => {
    const { list } = this.props;
    const escapedValue = this.escapeRegexCharacters(value.trim());

    if (escapedValue === '') {
      return [];
    }

    const regex = new RegExp(`^${escapedValue}`, 'i');

    return list.filter(language => regex.test(language.name));
  };

  getSuggestionValue = suggestion => suggestion.name;

  renderSuggestion = suggestion => <span>{suggestion.name}</span>;

  onChange = (_, { newValue }) => {
    const { id, onChange } = this.props;

    this.setState({
      value: newValue,
    });

    onChange(id, newValue);
  };

  onKeyPress = e => {
    const { onEnterPress, id } = this.props;
    const { value } = this.state;
    if (e.key === 'Enter') {
      onEnterPress(id, value);
    }
  };

  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value),
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  render() {
    const { id, placeholder } = this.props;
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder,
      value,
      onChange: this.onChange,
      className: 'form-control',
      onKeyPress: this.onKeyPress,
    };

    return (
      <Autosuggest
        id={id}
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={this.getSuggestionValue}
        renderSuggestion={this.renderSuggestion}
        inputProps={inputProps}
      />
    );
  }
}

AutoSuggest.defaultProps = {
  onEnterPress: () => {},
  inputText: '',
};

AutoSuggest.propTypes = {
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onEnterPress: PropTypes.func,
  list: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
  inputText: PropTypes.string,
};

export default AutoSuggest;

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Searchbar.scss';

class Searchbar extends Component {
  constructor(props) {
    super(props);
    this.state = { searchText: '' };
    this.handleSearch = this.handleSearch.bind(this);
    this.clearText = this.clearText.bind(this);
  }

  clearText() {
    const { searchfunc } = this.props;
    this.setState(
      {
        searchText: '',
      },
      () => {
        const { searchText } = this.state;
        searchfunc(searchText);
      },
    );
  }

  handleSearch(event) {
    const { searchfunc } = this.props;
    this.setState(
      {
        [event.target.id]: event.target.value,
      },
      () => {
        const { searchText } = this.state;
        searchfunc(searchText);
      },
    );
  }

  render() {
    const { searchText } = this.state;
    const { placeholder } = this.props;
    const onClick = searchText.length > 0 ? this.clearText : () => {};
    return (
      // <div className="col-lg-12">
      //  <div className="form-group search-group-filter-block">
      <>
        <input
          id="searchText"
          className="curated-search form-control"
          value={searchText}
          onChange={this.handleSearch}
          placeholder={placeholder}
          type="text"
        />
        <i
          className={
            searchText.length > 0
              ? 'fas fa-times search-icon'
              : 'fas fa-search search-icon'
          }
          aria-hidden="true"
          onClick={onClick}
        />
        <input value="0" id="search_id" type="hidden" />
        {/* // </div> */}
        {/* // </div> */}
      </>
    );
  }
}

Searchbar.propTypes = {
  placeholder: PropTypes.string,
  searchfunc: PropTypes.func.isRequired,
};

Searchbar.defaultProps = {
  placeholder: '',
};

export default Searchbar;

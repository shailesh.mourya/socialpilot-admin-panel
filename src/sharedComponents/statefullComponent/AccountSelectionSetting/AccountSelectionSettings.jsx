import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '../../pureComponent(stateless)/Buttons/Button';
import SPDropdown from '../SPdropDown/SPDropdown';
import accountsData from '../../../utils/misc/accountsData';
import getLocalizeText from '../../../utils/functions/getLocalizeText';
import DisplayText from '../../lables/index';
import GlobalDisplayTexts from '../../../utils/labels/localizationText';

import './AccountSelectionSettings.scss';

class AccountSelectionSettings extends Component {
  constructor(props) {
    super(props);
    this.options = this.initializeOptions();
    this.state = {
      defaulttime: { value: '', label: GlobalDisplayTexts.ALL },
      keyword: '',
      firstinitilize: true,
      criteriaKeyword: {
        value: '',
        selectedGroupOption: 0,
        selectedSubOption: 0,
      },
    };
    this.handleSearchInput = this.handleSearchInput.bind(this);
    this.handleSearchCriteria = this.handleSearchCriteria.bind(this);
  }

  componentDidMount() {
    const { clientFromProps } = this.props;
    const { firstinitilize } = this.state;
    this.initializeOptions();
    if (clientFromProps && firstinitilize && this.options) {
      let optionIndex = 0;
      this.options.forEach((option, indexvalue) => {
        if (option.label === 'Clients') {
          optionIndex = indexvalue;
        }
      });
      const temp = this.options[optionIndex].options.filter(
        (account, index) =>
          account.value === `client_${index}_${clientFromProps}`,
      );

      if (temp[0]) {
        this.setState({
          firstinitilize: false,
          defaulttime: temp,
          criteriaKeyword: {
            value: `${temp[0].value.split('_')[0]}_${
              temp[0].value.split('_')[2]
            }`,
          },
        });
      }
    }
  }

  componentDidUpdate(preprops) {
    const { accountDelete, allCheckedState } = this.props;
    if (accountDelete !== preprops.accountDelete) {
      if (allCheckedState !== preprops.accountDelete) {
        if (allCheckedState) {
          this.setState({
            defaulttime: { value: '', label: GlobalDisplayTexts.ALL },
            keyword: '',
          });
        }
      }
    }
  }

  checkBetaFeatureCondition = () => {
    const { userDetails } = this.props;
    const betafeatureavailable =
      !!userDetails.betaFeature &&
      !!userDetails.betaFeature.bufferAccount &&
      userDetails.betaFeature.bufferAccount === 'Y';

    if (userDetails.enableBuffer === 'Y') {
      return false;
    }
    return !betafeatureavailable;
  };

  handlesearchbtn = () => {
    const { criteriaKeyword, keyword } = this.state;
    const { handleSearch } = this.props;
    const [criteria, value] = criteriaKeyword.value.split('_');
    handleSearch(keyword, criteria, parseInt(value, 10));
  };

  initializeOptions() {
    const { groupsList, clientsList } = this.props;
    const groups = [];
    const clients = [];
    const accounts = [];
    const groupwithlabel = [{ label: DisplayText.BY_GROUPS, options: groups }];
    const clientwithlabel = [
      { label: DisplayText.BY_CLIENTS, options: clients },
    ];
    const accountwithlabel = [
      { label: DisplayText.BY_ACCOUNTS, options: accounts },
    ];
    // const allWithLabel = [{ options: [{ value: '', label: 'All' }] }];
    for (let i = 0; i < groupsList.length; i += 1) {
      const { groupId, groupName } = groupsList[i];
      groups.push({ label: groupName, value: `group_${i}_${groupId}` });
    }

    for (let i = 0; i < clientsList.length; i += 1) {
      const { teamId, firstName } = clientsList[i];
      clients.push({ label: firstName, value: `client_${i}_${teamId}` });
    }

    Object.values(accountsData).forEach((act, index) => {
      if (act.accountIds[0] === 22 && this.checkBetaFeatureCondition()) {
        // do not push any buffer related data
      } else {
        accounts.push({
          label: act.socialmedaia,
          value: `accounts|${act.socialmedaia}_${index}`,
        });
      }
    });
    let generatedOptions = [];
    if (
      Object.values(clients).length === 0 &&
      Object.values(groups).length === 0
    ) {
      generatedOptions = [...accountwithlabel];
    }
    if (Object.values(clients).length === 0) {
      generatedOptions = [...groupwithlabel, ...accountwithlabel];
    }
    if (Object.values(groups).length === 0) {
      generatedOptions = [...clientwithlabel, ...accountwithlabel];
    }
    generatedOptions = [
      ...clientwithlabel,
      ...groupwithlabel,
      ...accountwithlabel,
    ];
    if (generatedOptions[0].custom) {
      generatedOptions.shift();
    }
    return generatedOptions;
  }

  handleSearchCriteria(obj) {
    this.setState(state => {
      const currentState = { ...state };

      let {
        selectedGroupOption,
        selectedSubOption,
        value,
      } = currentState.criteriaKeyword;
      if (obj.value === '') {
        selectedGroupOption = 0;
        selectedSubOption = 0;
        value = '';
      } else {
        const [criteria, index, id] = obj.value.split('_');
        // eslint-disable-next-line default-case
        switch (criteria) {
          case 'group':
            selectedGroupOption = 2;
            break;

          case 'client':
            selectedGroupOption = 1;
            break;

          default:
            selectedGroupOption = 3;
            break;
        }
        selectedSubOption = parseInt(index, 10);
        value = `${criteria}_${id}`;
      }
      currentState.criteriaKeyword = {
        selectedGroupOption,
        selectedSubOption,
        value,
      };
      return currentState;
    });
  }

  handleSearchInput(event) {
    this.setState({
      keyword: event.target.value,
    });
  }

  render() {
    const { keyword } = this.state;
    const { isAdmin, userDetails } = this.props;
    const dropdownOptions = this.options;

    return (
      <div className="search-account-menu">
        <div className="search-criteria">
          <SPDropdown
            options={dropdownOptions}
            isDisabled={isAdmin}
            onChange={this.handleSearchCriteria}
            dropdownType="group"
            //   isClearable
            placeHolder={getLocalizeText(DisplayText.SELECT_ACCOUNT)}
            isSearchable={dropdownOptions && dropdownOptions.length > 5}
          />
        </div>
        <div className="search-input">
          <input
            className="form-control"
            type="text"
            placeholder={getLocalizeText(
              DisplayText.SEARCH,
              userDetails.defaultLanguage,
            )}
            disabled={isAdmin}
            value={keyword}
            onChange={this.handleSearchInput}
            onKeyPress={event => {
              if (event.key === 'Enter') {
                this.handlesearchbtn();
              }
            }}
          />
        </div>

        <div className="search-btn">
          <Button
            type="button"
            bsClass="btn btn-secondary search-account"
            onClick={this.handlesearchbtn}
          >
            <i className="fas fa-search" />
          </Button>
        </div>
      </div>
    );
  }
}

AccountSelectionSettings.propTypes = {
  accountDelete: PropTypes.bool,
  allCheckedState: PropTypes.bool,
  clientFromProps: PropTypes.string,
  clientsList: PropTypes.arrayOf(PropTypes.object),
  groupsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  handleSearch: PropTypes.func.isRequired,
  isAdmin: PropTypes.bool,
  userDetails: PropTypes.objectOf(PropTypes.object).isRequired,
};

AccountSelectionSettings.defaultProps = {
  accountDelete: false,
  allCheckedState: false,
  clientFromProps: '',
  clientsList: [],
  isAdmin: false,
};

export default AccountSelectionSettings;

import React from 'react';
import { connect } from 'react-redux';
import Img from 'react-image';
import PropTypes from 'prop-types';
import SocialPilotLogo from '../assets/SocialPilotLogo.png';
import { IS_ENTERPRISE } from '../utils/misc/config';
import Loading from '../assets/post-loading.gif';
import isParamInURL from '../utils/functions/isParamInURL';

const LoadingImage = () => <img height={30} src={Loading} alt="img" />;

const CompanyLogo = ({
  userSetting,
  enterPriceDetails,
  clientSignupLinkDetails,
}) => {
  let logoSrc = SocialPilotLogo;
  let logoAlt = 'SocialPilot';

  if (userSetting.status === 'success') {
    const { role, clientLogo } = userSetting.successResponse.data;

    if (role === 'C' && clientLogo) logoSrc = clientLogo;
  }
  if (userSetting.status === 'success' && IS_ENTERPRISE) {
    const {
      enterpriseDetails: { companyLogo },
      role,
      clientLogo,
    } = userSetting.successResponse.data;
    if (IS_ENTERPRISE) logoSrc = companyLogo;
    if (role === 'C' && clientLogo) logoSrc = clientLogo;
  }

  if (clientSignupLinkDetails.status === 200 && isParamInURL('client') !== -1) {
    const { orgName, logo } = clientSignupLinkDetails.response;
    logoSrc = logo;
    logoAlt = orgName;
  }
  if (isParamInURL('client') !== -1 && clientSignupLinkDetails.isLoading) {
    return <LoadingImage />;
  }
  if (IS_ENTERPRISE && enterPriceDetails.status === 'success') {
    const { companyLogo, companyName } = enterPriceDetails;
    logoSrc = companyLogo;
    logoAlt = companyName;
  }
  return (
    <Img src={logoSrc} height={30} alt={logoAlt} loader=<LoadingImage /> />
  );
};

CompanyLogo.propTypes = {
  userSetting: PropTypes.objectOf(PropTypes.object).isRequired,
  enterPriceDetails: PropTypes.objectOf(PropTypes.string).isRequired,
  clientSignupLinkDetails: PropTypes.objectOf(PropTypes.string).isRequired,
};

const mapStateToProps = state => ({
  userSetting: state.userSetting,
  enterPriceDetails: state.enterPriceDetailsReducer,
  clientSignupLinkDetails: state.clientSignupLinkDetails,
});

export default connect(mapStateToProps)(CompanyLogo);

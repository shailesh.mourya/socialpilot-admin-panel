import englishText from '../index';

const portugeaze = {
  [englishText.TERMS_POLICY]: 'Ao continuar, você concorda em',
  [englishText.CANCEL_ACCOUNT]: 'Cancelar Conta',
  [englishText.SET_PASSWORD]: 'Configurar Senha',
  [englishText.RESET_PASSWORD_NOTE]: 'Digite seu email para redefinir a senha',
  [englishText.COMPANY_SIZE]: 'Dimensão da empresa',
  [englishText.LOGIN]: 'Entrar',
  [englishText.FORGET_PASSWORD]: 'Esqueceu a senha?',
  [englishText.SIGN_UP_USING]: 'Inscreva-se usando',
  [englishText.LOGIN_USING]: 'Login em uso',
  [englishText.CHANGE_PASSWORD]: 'Mudar senha',
  [englishText.ACCEPT_TERMS]:
    'Por favor, leia e aceite os termos para se inscrever',
  [englishText.RESET_PASSWORD]: 'Redefinir Senha',
  [englishText.RETYPE_PASSWORD]: 'Redigite a senha',
  [englishText.RESET_PASSWORD_BTN]: 'Reset da minha senha',
  [englishText.PASSWORD]: 'Senha',
  [englishText.OLD_PASSWORD]: 'Senha Antiga',
  [englishText.SIGN_IN]: 'Sign in',
  [englishText.SIGN_UP]: 'Sign Up',
  [englishText.TERMS_OF_SERVICE]: 'Termos de serviço',
  [englishText.ERROR_COMPANY_TYPE_BLANK]: 'Tipo de empresa é obrigatório',
  [englishText.BACK_TO_LOGIN]: 'Volte ao login',
  [englishText.DONT_HAVE_ACCOUNT]: 'Não tem uma conta?',

  [englishText.SAVED]: 'Salvo!',
  [englishText.PROPER_OLD_PASSWORD_PROVIDED]:
    'Uma senha antiga correta deve ser fornecida',
  [englishText.PROPER_NEW_PASSWORD_PROVIDED]:
    'Uma senha nova correta deve ser fornecida',
  [englishText.PROPER_RETYPE_PASSWORD_PROVIDED]:
    'Uma redigitação correta deve ser feira',
  [englishText.SUCCESSFULLY_REGISTERED_AS_CLIENT]:
    'Você se registrou com sucesso como um cliente.',
  [englishText.ERROR_FIRST_NAME]: 'Primeiro Nome é obrigatório',
  [englishText.HAVE_ACCOUNT]: 'Tem uma conta?',
  [englishText.NEW_CONFIRM_PASSWORD_MATCH]:
    'A nova senha e a senha de confirmação não coincidem',
  [englishText.SET_A_NEW_PASSWORD]: 'Defina uma Nova Senha',
  [englishText.ATLEAST_5_CHAR]: 'Com no mínimo 5 caracteres',
  [englishText.ERROR_PASSWORD_BLANK]:
    'A nova senha não pode ser deixada em branco',
  [englishText.ERROR_NEW_PASSWORD]:
    'A nova senha deve conter no mínimo 5 caracteres.',
  [englishText.ERROR_RETYPE_PASSWORD_BLANK]:
    'Redigite a Senha para não deixar em branco',
  [englishText.ERROR_RETYPE_PASSWORD]:
    'Redigite a Senha para que ela tenha no mínimo 5 caracteres.',
  [englishText.ERROR_EMAIL_BLANK]:
    'Email do Usuário não pode ser deixado em branco.',
  [englishText.PRIVACY_POLICY]: 'Política de Privacidade.',
  [englishText.WHAT_DESCRIBES_YOU_BEST]: 'O que lhe descreve o melhor?',
  [englishText.I_AGREE]: 'Eu concorco com o SocialPilot',
  [englishText.ERROR_COMPANY_SIZE_BLANK]: 'O tamanho da empresa é obrigatório',

  [englishText.ACCOUNT_DELETED_1]:
    'Your SocialPilot account has been deleted. Feel free to ',
  [englishText.SIGNUP]: 'sign up',
  [englishText.AGAIN]: 'again!',
};

export default portugeaze;

import englishText from '../index';

const spanish = {
  [englishText.FORGET_PASSWORD]: '\u00bfSe te olvid\u00f3 tu contrase\u00f1a?',
  [englishText.CHANGE_PASSWORD]: 'Cambiar Contrase\u00f1a',
  [englishText.CANCEL_ACCOUNT]: 'Cancelar cuenta',
  [englishText.SET_PASSWORD]: 'Configurar la Clave',
  [englishText.PASSWORD]: 'Contrase\u00f1a',
  [englishText.OLD_PASSWORD]: 'Contrase\u00f1a Antigua',
  [englishText.ERROR_EMAIL_BLANK]: 'E-mail no puede estar vacío.',
  [englishText.SET_A_NEW_PASSWORD]: 'Escoge una nueva clave',
  [englishText.RETYPE_PASSWORD]: 'Reescriba su contrase\u00f1a',
  [englishText.SIGN_UP]: 'Registrarse',
  [englishText.RESET_PASSWORD]: 'Restablecer la Contraseña',
  [englishText.RESET_PASSWORD_BTN]: 'Restablecer mi contraseña',
  [englishText.SIGN_IN]: 'Sign in',
  [englishText.COMPANY_SIZE]: 'Tamaño de la empresa',
  [englishText.TERMS_OF_SERVICE]: 'Termos de serviço',
  [englishText.ERROR_COMPANY_TYPE_BLANK]: 'Tipo de empresa é obrigatório',
  [englishText.BACK_TO_LOGIN]: 'volver a Iniciar Sesi\u00f3n',

  [englishText.SAVED]: 'Saved! ',
  [englishText.PROPER_OLD_PASSWORD_PROVIDED]:
    'A proper old password must be provided',
  [englishText.PROPER_NEW_PASSWORD_PROVIDED]:
    'A proper new password must be provided',
  [englishText.PROPER_RETYPE_PASSWORD_PROVIDED]:
    'A proper retype password must be provided',
  [englishText.SUCCESSFULLY_REGISTERED_AS_CLIENT]:
    'You’ve successfully registered as a client.',
  [englishText.ERROR_FIRST_NAME]: 'First Name is required',
  [englishText.HAVE_ACCOUNT]: 'Have an account?',
  [englishText.NEW_CONFIRM_PASSWORD_MATCH]:
    'New password and confirm password did not match.',
  [englishText.ATLEAST_5_CHAR]: 'At least five characters',
  [englishText.ERROR_PASSWORD_BLANK]: 'Password cannot be blank',
  [englishText.ERROR_NEW_PASSWORD]:
    'New password should contain at least 5 characters.',
  [englishText.ERROR_RETYPE_PASSWORD_BLANK]: 'Retype Password cannot be blank',
  [englishText.ERROR_RETYPE_PASSWORD]:
    'Retype password should contain at least 5 characters.',
  [englishText.RESET_PASSWORD_NOTE]:
    'Type in your registered email to reset password',
  [englishText.LOGIN]: 'Log In',
  [englishText.DONT_HAVE_ACCOUNT]: "Don't have an account?",
  [englishText.LOGIN_USING]: 'Log In Using',
  [englishText.TERMS_POLICY]: 'By continuing you agree to',
  [englishText.PRIVACY_POLICY]: 'Privacy Policy.',
  [englishText.WHAT_DESCRIBES_YOU_BEST]: 'What describes you the best?',
  [englishText.I_AGREE]: "I agree to SocialPilot's",
  [englishText.ACCEPT_TERMS]: 'Please read and accept the terms to sign up',
  [englishText.ERROR_COMPANY_SIZE_BLANK]: 'Company size is required',
  [englishText.SIGN_UP_USING]: 'Sign Up Using',
  [englishText.ACCOUNT_DELETED_1]:
    'Your SocialPilot account has been deleted. Feel free to ',
  [englishText.SIGNUP]: 'sign up',
  [englishText.AGAIN]: 'again!',
};

export default spanish;

import englishText from '../index';

const german = {
  [englishText.CANCEL_ACCOUNT]: 'Konto Abbrechen',
  [englishText.COMPANY_SIZE]: 'Unternehmensgröße',
  [englishText.OLD_PASSWORD]: 'Altes Passwort',
  [englishText.SIGN_UP]: 'Anmelden',
  [englishText.SIGN_IN]: 'Anmelden',
  [englishText.ACCEPT_TERMS]:
    'Bitte lesen und akzeptieren Sie die Nutzungsbedingungen',
  [englishText.LOGIN]: 'Einloggen',
  [englishText.LOGIN_USING]: 'Einloggen mit',
  [englishText.RESET_PASSWORD_NOTE]:
    'Geben Sie die registrierte Email ein, um das Passwort zur\u00fcckzusetzen',
  [englishText.TERMS_POLICY]: 'Indem du fortf\u00e4hrst, stimmst du zu',
  [englishText.RESET_PASSWORD_BTN]: 'Mein Passwort zur\u00fccksetzen',
  [englishText.TERMS_OF_SERVICE]: 'Nutzungsbedingungen',
  [englishText.PASSWORD]: 'Passwort',
  [englishText.CHANGE_PASSWORD]: 'Passwort \u00e4ndern',
  [englishText.RETYPE_PASSWORD]: 'Passwort erneut eingeben',
  [englishText.SET_PASSWORD]: 'Passwort festlegen',
  [englishText.FORGET_PASSWORD]: 'Passwort vergessen?',
  [englishText.RESET_PASSWORD]: 'Passwort zur\u00fccksetzen',
  [englishText.SIGN_UP_USING]: 'Registrieren mit',
  [englishText.ERROR_COMPANY_TYPE_BLANK]:
    'Unternehmensgr\u00f6\u00dfe ist erforderlich',
  [englishText.BACK_TO_LOGIN]: 'zur\u00fcck zur Anmeldung',
  [englishText.SAVED]: 'Gespeichert!',
  [englishText.PROPER_OLD_PASSWORD_PROVIDED]:
    'Ein korrektes altes Passwort muss angegeben werden',
  [englishText.PROPER_NEW_PASSWORD_PROVIDED]:
    'Es muss ein neues Passwort vergeben werden',
  [englishText.PROPER_RETYPE_PASSWORD_PROVIDED]:
    'Es muss ein korrektes Wiederholungspasswort eingegeben werden.',
  [englishText.SUCCESSFULLY_REGISTERED_AS_CLIENT]:
    'Sie haben sich erfolgreich als Kunde registriert.',
  [englishText.ERROR_FIRST_NAME]: 'Vorname ist erforderlich',
  [englishText.HAVE_ACCOUNT]: 'Haben Sie ein Konto?',
  [englishText.NEW_CONFIRM_PASSWORD_MATCH]:
    'Neues Passwort und Bestätigungs-Passwort stimmen nicht überein.',
  [englishText.SET_A_NEW_PASSWORD]: 'Neues Passwort festlegen',
  [englishText.ATLEAST_5_CHAR]: 'Mindestens fünf Zeichen',
  [englishText.ERROR_PASSWORD_BLANK]: 'Passwort kann nicht leer sein',
  [englishText.ERROR_NEW_PASSWORD]:
    'Das neue Passwort sollte mindestens 5 Zeichen enthalten.',
  [englishText.ERROR_RETYPE_PASSWORD_BLANK]:
    'Wiederholungspasswort kann nicht leer sein',
  [englishText.ERROR_RETYPE_PASSWORD]:
    'Das Passwort sollte mindestens 5 Zeichen enthalten.',
  [englishText.ERROR_EMAIL_BLANK]: 'Die Benutzer-E-Mail kann nicht leer sein.',
  [englishText.DONT_HAVE_ACCOUNT]: 'Sie haben kein Konto?',
  [englishText.PRIVACY_POLICY]: 'Datenschutzerklärung.',
  [englishText.WHAT_DESCRIBES_YOU_BEST]: 'Was beschreibt Sie am besten?',
  [englishText.I_AGREE]: "Ich stimme SocialPilot's",
  [englishText.ERROR_COMPANY_SIZE_BLANK]: 'Unternehmensgröße ist erforderlich',

  [englishText.ACCOUNT_DELETED_1]:
    'Your SocialPilot account has been deleted. Feel free to ',
  [englishText.SIGNUP]: 'sign up',
  [englishText.AGAIN]: 'again!',
};

export default german;

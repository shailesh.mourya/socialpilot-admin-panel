/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';


import LoginForm from './components/Form';
//import Table from './Components/TableComponent';
import './index.scss';
import CompanyLogo from '../../../assets/SocialPilotLogo.png';

class Login extends Component {
    
  submitLogin = e => {
    e.preventDefault();

    const email = e.target.email.value;
    const password = e.target.password.value;

    console.log(email);
    console.log(password);
    // const { fetchUserToken } = this.props;
    // const emailError = emailValidator(email);
    // const validatePassword = passwordValidator(password);
    // const passwordError = validatePassword.required() || validatePassword.min();

    // if (emailError || passwordError) {
    //   this.setState({ emailError, passwordError });
    // } else {
    //   this.setState({ isSubmited: true });
    //   fetchUserToken({ username: email, password });
    // }
  };

  Logo = () => (
    <div className="row logo-signup" style={{ textAlign: 'center' }}>
      <div className="col">
          <CompanyLogo />
      </div>
    </div>
  );

  render() {

    return (
    <>
      <div className="row">
        <div className="col-lg-12">
          
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <div className="login-main-block">
            <img src={CompanyLogo} alt="logo"/>
            <h3>Admin Panel Login</h3>
            <div className="login-register">
              <LoginForm
                submitLogin={this.submitLogin}
              />
            </div>
          </div>
        </div>
      </div>
      </>
    );
  }
}

export default Login;

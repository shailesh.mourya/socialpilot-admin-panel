import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Cookies from 'js-cookie';
import Button from '../../../../sharedComponents/pureComponent(stateless)/Buttons/Button';
//import getLocalizeText from '../../../../utils/functions/getLocalizeText';
import DisplayText from '../../lables/index';
import GlobalDisplayTexts from '../../../../utils/labels/localizationText';

const LoginForm = props => {
  const { submitLogin } = props;
//   const { emailError, passwordError, isSubmited } = loginData;
//   const defaultLanguage = Cookies.get('language');

  return (
    <form onSubmit={submitLogin} className="mb-2">
      <div className="form-group">
        <input
          type="text"
          name="email"
          className="form-control"
          placeholder="Email"
        />
      </div>
      <div className="form-group">
        <input
          type="password"
          name="password"
          className="form-control"
          placeholder="password"
        />
      </div>
      <div className="form-inline">
        <Button type="submit" className="btn btn-primary">
          <FormattedMessage
            id={DisplayText.LOGIN}
            defaultValue={DisplayText.LOGIN}
          />
        </Button>
        &nbsp;
        {/*isSubmited && <i className="fa fa-spinner fa-spin" />*/}
      </div>
    </form>
  );
};

LoginForm.propTypes = {
  loginData: PropTypes.objectOf(PropTypes.object).isRequired,
  submitLogin: PropTypes.func.isRequired,
};

export default LoginForm;

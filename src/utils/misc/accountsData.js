export default {
  Facebook: {
    socialmedaia: 'Facebook',
    accountIds: [2, 5, 6, 18],
  },
  Twitter: {
    socialmedaia: 'Twitter',
    accountIds: [1],
  },
  LinkedIn: {
    socialmedaia: 'LinkedIn',
    accountIds: [3, 8, 9],
  },
  Pinterest: {
    socialmedaia: 'Pinterest',
    accountIds: [14],
  },
  GoogleMyBusiness: {
    socialmedaia: 'Google My Business',
    accountIds: [21],
  },
  Instagram: {
    socialmedaia: 'Instagram',
    accountIds: [15],
  },
  Tumblr: {
    socialmedaia: 'Tumblr',
    accountIds: [10],
  },
  VK: {
    socialmedaia: 'VK',
    accountIds: [11, 13],
  },
};

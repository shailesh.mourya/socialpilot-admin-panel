export const dummyGroupsList = [
  {
    groupId: 12725,
    groupName: 'Dummy Group 1',
    accounts: [{ loginId: 57234, accountName: 'dummy1' }],
  },
  {
    groupId: 12726,
    groupName: 'Dummy Group 2',
    accounts: [{ loginId: 57235, accountName: 'dummy2' }],
  },
  {
    groupId: 12727,
    groupName: 'Dummy Group 3',
    accounts: [
      { loginId: 57236, accountName: 'dummy3' },
      { loginId: 57239, accountName: 'dummy6' },
    ],
  },
];

export const dummyClientsList = [];

// export const dummyUserSetting = {
//   membershipId: 10
// };

export const dummyPlans = {
  currentPlan: {
    membershipId: 10,
    features: [
      {
        name: 'Connected Profiles',
        value: '200',
      },
      {
        name: 'Post',
        value: '500/day',
      },
      {
        name: 'RSS Feeds',
        value: '5',
      },

      {
        name: 'Post In scheduling queue',
        value: '5000',
      },

      {
        name: 'Team Members',
        value: '20',
      },

      {
        name: 'Social Media Analytics',
        value: 'Y',
      },

      {
        name: 'Bulk Scheduling',
        value: 'Y',
      },

      {
        name: 'Brandable PDF Analytics Report',
        value: 'Y',
      },
    ],
  },
  praposedPlan: [
    {
      membershipId: 39,
      features: [
        {
          name: 'Connected Profiles',
          value: '200',
        },
        {
          name: 'Post',
          value: '500/day',
        },
        {
          name: 'RSS Feeds',
          value: '20',
        },
        {
          name: 'Post In scheduling queue',
          value: '5000',
        },
        {
          name: 'Team Members',
          value: '20',
        },

        {
          name: 'Social Media Analytics',
          value: 'Y',
        },

        {
          name: 'Bulk Scheduling',
          value: 'Y',
        },

        {
          name: 'Brandable PDF Analytics Report',
          value: 'Y',
        },
      ],
    },
  ],
};

export const dummyAPIResponse = {
  message: 'Plan list displayed successfully.',
  response: {
    currentPlan: {
      planName: 'Individual',
      palnType: 'M',
      membershipId: 39,
      price: 10,
      summary: [
        'Up to 10 connected profiles',
        '50 posts sharing per day',
        '0 Team members',
        '1000 posts in scheduling queue',
      ],
      features: {
        'Connected Profiles': 10,
        Post: '50/day',
        'RSS Feeds': 5,
        'Post In scheduling queue': 1000,
        'Team Members': 0,
        'Social Media Analytics': 'No',
        'Bulk Scheduling': 'No',
        'Brandable PDF Analytics Report': 'No',
        'Social Inbox (coming soon)': 'Yes',
      },
    },
    praposedPlans: [
      {
        planName: 'Individual',
        Y: {
          price: 100,
          membershipId: 40,
          summary: [
            'Up to 10 connected profiles',
            '50 posts sharing per day',
            '0 Team members',
            '1000 posts in scheduling queue',
          ],
          features: {
            'Connected Profiles': 10,
            Post: '50/day',
            'RSS Feeds': 20,
            'Post In scheduling queue': 1000,
            'Team Members': 0,
            'Social Media Analytics': 'No',
            'Bulk Scheduling': 'No',
            'Brandable PDF Analytics Report': 'No',
            'Social Inbox (coming soon)': 'Yes',
          },
        },
      },
      {
        planName: 'Professional',
        M: {
          price: 30,
          membershipId: 41,
          summary: [
            'Up to 50 connected profiles',
            '200 posts sharing per day',
            '5 Team members',
            '2500 posts in scheduling queue',
          ],
          features: {
            'Connected Profiles': 50,
            Post: '200/day',
            'RSS Feeds': 20,
            'Post In scheduling queue': 2500,
            'Team Members': 5,
            'Social Media Analytics': 'Yes',
            'Bulk Scheduling': 'Yes',
            'Brandable PDF Analytics Report': 'No',
            'Social Inbox (coming soon)': 'Yes',
          },
        },
        Y: {
          price: 300,
          membershipId: 42,
          summary: [
            'Up to 50 connected profiles',
            '200 posts sharing per day',
            '5 Team members',
            '2500 posts in scheduling queue',
          ],
          features: {
            'Connected Profiles': 50,
            Post: '200/day',
            'RSS Feeds': 20,
            'Post In scheduling queue': 2500,
            'Team Members': 5,
            'Social Media Analytics': 'Yes',
            'Bulk Scheduling': 'Yes',
            'Brandable PDF Analytics Report': 'No',
            'Social Inbox (coming soon)': 'Yes',
          },
          monthlyPlanId: 41,
        },
      },
      {
        planName: 'Small Team',
        M: {
          price: 50,
          membershipId: 43,
          summary: [
            'Up to 100 connected profiles',
            '500 posts sharing per day',
            '10 Team members',
            '5000 posts in scheduling queue',
            'Priority Support',
            'Personalized Onboarding',
          ],
          features: {
            'Connected Profiles': 100,
            Post: '500/day',
            'RSS Feeds': 20,
            'Post In scheduling queue': 5000,
            'Team Members': 10,
            'Social Media Analytics': 'Yes',
            'Bulk Scheduling': 'Yes',
            'Brandable PDF Analytics Report': 'Yes',
            'Social Inbox (coming soon)': 'Yes',
          },
        },
        Y: {
          price: 500,
          membershipId: 44,
          summary: [
            'Up to 100 connected profiles',
            '500 posts sharing per day',
            '10 Team members',
            '5000 posts in scheduling queue',
            'Priority Support',
            'Personalized Onboarding',
          ],
          features: {
            'Connected Profiles': 100,
            Post: '500/day',
            'RSS Feeds': 20,
            'Post In scheduling queue': 5000,
            'Team Members': 10,
            'Social Media Analytics': 'Yes',
            'Bulk Scheduling': 'Yes',
            'Brandable PDF Analytics Report': 'Yes',
            'Social Inbox (coming soon)': 'Yes',
          },
          monthlyPlanId: 43,
        },
      },
      {
        planName: 'Agency',
        M: {
          price: 100,
          membershipId: 45,
          summary: [
            'Up to 200 connected profiles',
            '1000 posts sharing per day',
            '20 Team members',
            '10000 posts in scheduling queue',
            'Priority Support',
            'Personalized Onboarding',
          ],
          features: {
            'Connected Profiles': 200,
            Post: '1000/day',
            'RSS Feeds': 20,
            'Post In scheduling queue': 10000,
            'Team Members': 20,
            'Social Media Analytics': 'Yes',
            'Bulk Scheduling': 'Yes',
            'Brandable PDF Analytics Report': 'Yes',
            'Social Inbox (coming soon)': 'Yes',
          },
        },
        Y: {
          price: 1000,
          membershipId: 46,
          summary: [
            'Up to 200 connected profiles',
            '1000 posts sharing per day',
            '20 Team members',
            '10000 posts in scheduling queue',
            'Priority Support',
            'Personalized Onboarding',
          ],
          features: {
            'Connected Profiles': 200,
            Post: '1000/day',
            'RSS Feeds': 20,
            'Post In scheduling queue': 10000,
            'Team Members': 20,
            'Social Media Analytics': 'Yes',
            'Bulk Scheduling': 'Yes',
            'Brandable PDF Analytics Report': 'Yes',
            'Social Inbox (coming soon)': 'Yes',
          },
          monthlyPlanId: 45,
        },
      },
    ],
  },
};

export const dummyAccountsList = [
  {
    loginId: 57234,
    accountUsername: 'dummy1',
    profilePicture:
      'https://i.pinimg.com/236x/7e/bc/88/7ebc888a34305274628610bf02137aaa--profile-pictures-deadpool.jpg',
    accountType: 'facebook',
  },
  {
    loginId: 57235,
    accountUsername: 'dummy2',
    profilePicture:
      'https://i.pinimg.com/236x/7e/bc/88/7ebc888a34305274628610bf02137aaa--profile-pictures-deadpool.jpg',
    accountType: 'facebook',
  },
  {
    loginId: 57236,
    accountUsername: 'dummy3',
    profilePicture:
      'https://i.pinimg.com/236x/7e/bc/88/7ebc888a34305274628610bf02137aaa--profile-pictures-deadpool.jpg',
    accountType: 'twitter',
  },
  {
    loginId: 57237,
    accountUsername: 'dummy4',
    profilePicture:
      'https://i.pinimg.com/236x/7e/bc/88/7ebc888a34305274628610bf02137aaa--profile-pictures-deadpool.jpg',
    accountType: 'instagram',
  },
  {
    loginId: 57238,
    accountUsername: 'dummy5',
    profilePicture:
      'https://i.pinimg.com/236x/7e/bc/88/7ebc888a34305274628610bf02137aaa--profile-pictures-deadpool.jpg',
    accountType: 'linkedin',
  },
  {
    loginId: 57239,
    accountUsername: 'dummy6',
    profilePicture:
      'https://i.pinimg.com/236x/7e/bc/88/7ebc888a34305274628610bf02137aaa--profile-pictures-deadpool.jpg',
    accountType: 'twitter',
  },
];

export const dummyFacebookInboxList = [
  {
    type: 'conversation',
    id: 't_286226408772829',
    postId: 't_286226408772829',
    messageCount: 7,
    userName: 'HK Artoon',
    userId: '286226442106159',
    userImage:
      'https://graph.facebook.com/286226442106159/picture?height=100&width=100&return_ssl_resources=1',
    message: '7.',
    userFormatDate: 'Sep 24, 2018 03:23 PM',
    utcDateTime: '2018-09-24T09:53:19.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2191639850907677_2191781174226878',
    postId: '2168764053195257_2191639850907677',
    userName: 'HK Artoon',
    userId: '286226442106159',
    userImage:
      'https://graph.facebook.com/286226442106159/picture?height=100&width=100&return_ssl_resources=1',
    message: 'nice.............',
    userFormatDate: 'Sep 24, 2018 02:31 PM',
    utcDateTime: '2018-09-24T09:01:51.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2191639850907677_2191780987560230',
    postId: '2168764053195257_2191639850907677',
    userName: 'HK Artoon',
    userId: '286226442106159',
    userImage:
      'https://graph.facebook.com/286226442106159/picture?height=100&width=100&return_ssl_resources=1',
    message: 'goooood',
    userFormatDate: 'Sep 24, 2018 02:31 PM',
    utcDateTime: '2018-09-24T09:01:41.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2191639850907677_2191780760893586',
    postId: '2168764053195257_2191639850907677',
    userName: 'HK Artoon',
    userId: '286226442106159',
    userImage:
      'https://graph.facebook.com/286226442106159/picture?height=100&width=100&return_ssl_resources=1',
    message: 'ok',
    userFormatDate: 'Sep 24, 2018 02:31 PM',
    utcDateTime: '2018-09-24T09:01:33.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2191639850907677_2191656010906061',
    postId: '2168764053195257_2191639850907677',
    userName: 'HK Artoon',
    userId: '286226442106159',
    userImage:
      'https://graph.facebook.com/286226442106159/picture?height=100&width=100&return_ssl_resources=1',
    message: 'ok_1',
    userFormatDate: 'Sep 24, 2018 12:22 PM',
    utcDateTime: '2018-09-24T06:52:45.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2191599460911716',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: '8',
    userFormatDate: 'Sep 24, 2018 11:22 AM',
    utcDateTime: '2018-09-24T05:52:00.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2191599440911718',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: '7',
    userFormatDate: 'Sep 24, 2018 11:21 AM',
    utcDateTime: '2018-09-24T05:51:59.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2191599407578388',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: '66',
    userFormatDate: 'Sep 24, 2018 11:21 AM',
    utcDateTime: '2018-09-24T05:51:57.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2191599390911723',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: '5',
    userFormatDate: 'Sep 24, 2018 11:21 AM',
    utcDateTime: '2018-09-24T05:51:57.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2191599367578392',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: '4',
    userFormatDate: 'Sep 24, 2018 11:21 AM',
    utcDateTime: '2018-09-24T05:51:56.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2191599360911726',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: '3',
    userFormatDate: 'Sep 24, 2018 11:21 AM',
    utcDateTime: '2018-09-24T05:51:56.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2191599350911727',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: '2',
    userFormatDate: 'Sep 24, 2018 11:21 AM',
    utcDateTime: '2018-09-24T05:51:55.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2191599337578395',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: '1',
    userFormatDate: 'Sep 24, 2018 11:21 AM',
    utcDateTime: '2018-09-24T05:51:54.000Z',
    isArchive: false,
  },
  {
    type: 'conversation',
    id: 't_452302025291729',
    postId: 't_452302025291729',
    messageCount: 619,
    userName: 'Nayan Artoon',
    userId: '452809561907642',
    userImage:
      'https://graph.facebook.com/452809561907642/picture?height=100&width=100&return_ssl_resources=1',
    message: 'hi....',
    userFormatDate: 'Sep 20, 2018 05:29 PM',
    utcDateTime: '2018-09-20T11:59:03.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2183673868370942',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: 'oooooooooooooooooooooooo',
    userFormatDate: 'Sep 19, 2018 06:54 PM',
    utcDateTime: '2018-09-19T13:24:45.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2183673018371027',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: '',
    userFormatDate: 'Sep 19, 2018 06:54 PM',
    utcDateTime: '2018-09-19T13:24:08.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2183672188371110',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: '',
    userFormatDate: 'Sep 19, 2018 06:53 PM',
    utcDateTime: '2018-09-19T13:23:32.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2183670268371302',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: 'Social Test',
    userFormatDate: 'Sep 19, 2018 06:52 PM',
    utcDateTime: '2018-09-19T13:22:05.000Z',
    isArchive: false,
  },
  {
    type: 'post',
    id: '2168764053195257_2183585345046461',
    postId: '2168764053195257_2183585345046461',
    message: 'this is best think',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    userFormatDate: 'Sep 19, 2018 05:58 PM',
    utcDateTime: '2018-09-19T12:28:07.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2183585345046461_2183595918378737',
    postId: '2168764053195257_2183585345046461',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: 'one more',
    userFormatDate: 'Sep 19, 2018 05:54 PM',
    utcDateTime: '2018-09-19T12:24:43.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2183585345046461_2183593255045670',
    postId: '2168764053195257_2183585345046461',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: 'nice',
    userFormatDate: 'Sep 19, 2018 05:52 PM',
    utcDateTime: '2018-09-19T12:22:31.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2183303781741284_2183308028407526',
    postId: '2168764053195257_2183303781741284',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: 'nice',
    userFormatDate: 'Sep 19, 2018 01:30 PM',
    utcDateTime: '2018-09-19T08:00:24.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2183222911749371',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: 'Social Test  ok',
    userFormatDate: 'Sep 19, 2018 12:07 PM',
    utcDateTime: '2018-09-19T06:37:02.000Z',
    isArchive: false,
  },
  {
    type: 'post',
    id: '2168764053195257_2175247062546956',
    postId: '2168764053195257_2175247062546956',
    message: 'Test',
    userName: 'Hiral Artoon',
    userId: '466316333887494',
    userImage:
      'https://graph.facebook.com/466316333887494/picture?height=100&width=100&return_ssl_resources=1',
    userFormatDate: 'Sep 14, 2018 03:13 PM',
    utcDateTime: '2018-09-14T09:43:04.000Z',
    isArchive: false,
  },
  {
    type: 'conversation',
    id: 't_1909901982386906',
    postId: 't_1909901982386906',
    messageCount: 2,
    userName: 'Amit Tank',
    userId: '1911231192253985',
    userImage:
      'https://graph.facebook.com/1911231192253985/picture?height=100&width=100&return_ssl_resources=1',
    message: 'Hello ******',
    userFormatDate: 'Sep 13, 2018 12:06 PM',
    utcDateTime: '2018-09-13T06:36:33.000Z',
    isArchive: false,
  },
  {
    type: 'comment',
    id: '2170507699687559_2172249959513333',
    postId: '2168764053195257_2170507699687559',
    userName: 'Akki Patel',
    userId: '395713207627324',
    userImage:
      'https://graph.facebook.com/395713207627324/picture?height=100&width=100&return_ssl_resources=1',
    message: 'Good',
    userFormatDate: 'Sep 12, 2018 05:52 PM',
    utcDateTime: '2018-09-12T12:22:23.000Z',
    isArchive: false,
  },
];

export const mention = {
  message: 'Social Inbox Preview',
  response: {
    type: 'mention',
    previousItem: [],
    currentItem: [
      {
        id: '1090145426215456773',
        createdOn: 'Jan 29, 2019 12:41',
        utcTime: '2019-01-29T07:11:38.000Z',
        message: '@davidjo16250117  https://t.co/PkjSWWZDCp',
        isArchive: false,
        user: 'smith',
        screenName: 'smith_8827',
        userImage:
          'https://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png',
        isFavorited: false,
        favoriteCount: 0,
        isRetweeted: false,
        retweetCount: 0,
        attachments: [],
      },
    ],
    nextItem: [],
  },
};

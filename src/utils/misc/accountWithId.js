const objectwithvalue = {
  [`Twitter Profile`]: {
    socialmedaia: 'Twitter Profile',
    accountIds: [1],
  },
  [`LinkedIn Profile`]: {
    socialmedaia: 'LinkedIn Profile',
    accountIds: [3],
  },
  [`Tumblr Blog`]: {
    socialmedaia: 'Tumblr Blog',
    accountIds: [10],
  },
  [`Facebook Pages`]: {
    socialmedaia: 'Facebook Pages',
    accountIds: [5],
  },
  [`Facebook Groups`]: {
    socialmedaia: 'Facebook Groups',
    accountIds: [6],
  },
  [`LinkedIn Pages`]: {
    socialmedaia: 'LinkedIn Pages',
    accountIds: [9],
  },
  [`VK.com Profile`]: {
    socialmedaia: 'VK.com Profile',
    accountIds: [11],
  },
  [`VK.com Community`]: {
    socialmedaia: 'VK.com Community',
    accountIds: [13],
  },

  [`Pinterest Board`]: {
    socialmedaia: 'Pinterest Board',
    accountIds: [14],
  },
  [`Instagram Profile`]: {
    socialmedaia: 'Instagram Profile',
    accountIds: [15],
  },
  [`Google My Business`]: {
    socialmedaia: 'Google My Business',
    accountIds: [21],
  },
  [`TikTok`]: {
    socialmedaia: 'TikTok',
    accountIds: [24],
  },
};

export const findAccountWithId = id => {
  const convertIntoArray = Object.values(objectwithvalue);
  const find = convertIntoArray.find(i => i.accountIds.includes(id));
  if (find) {
    return find.socialmedaia;
  }
  return '';
};

export default objectwithvalue;

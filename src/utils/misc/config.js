/* eslint-disable camelcase */
export const RUN_ENVIORNMENT = {
  0: 'LOCAL',
  1: 'STAGING',
  2: 'PROD_STAGING',
  3: 'PRODUCTION',
  4: 'DILIP_LOCAL',
  5: 'RUTUL_LOCAL',
};
let ENTERPRISE = false;
export const FEED_PORT = null;
export const GROUP_PORT = null;
export const CURRENT_PORT = 5010;
export const LOCAL_PORT = 66;
export const CURRENT_ENVIORMENT = RUN_ENVIORNMENT[3];
export const BASE_URL = window.location.origin;
export const BASE_HOST = window.location.hostname;
export const BASE_PORT = window.location.port;

export const WEB_DEVELOPMENT_DOMAINS = [
  `http://192.168.1.195:${window.location.port}`,
  `http://localhost:${window.location.port}`,
  'https://staging.socialpilot.co:8080',
  'https://mento.io',
  `http://192.168.1.183:${window.location.port}`,
  `http://192.168.1.181:${window.location.port}`,
  'http://sp-react-staging.s3-website.ap-south-1.amazonaws.com',
  'https://master.d2fnwipghxlasu.amplifyapp.com',
  `https://192.168.1.170:${window.location.port}`,
  `http://192.168.1.170:${window.location.port}`,
];
export const LOCAL_DEVELOPMENT_DOMAIN = [
  // rutul's local domain
  `http://192.168.1.170:${window.location.port}`,
];

export const DEVELOPMENT_DOMAINS = [
  'https://app-dev.socialpilot.co',
  'https://dev.socialpilot.co:8080',
  `http://192.168.1.187:${window.location.port}`,
  `http://192.168.1.176:${window.location.port}`,
  `http://192.168.43.50:${window.location.port}`,
];

export const WEB_DEVELOPMENT_ENTERPRISE_DOMAINS = ['https://mento.io'];

export const WEB_PRODUCTION_DOMAINS = [
  'https://panel.socialpilot.co',
  'https://app.socialpilot.co',
];

export const WEB_ENTERPRISE_DOMAINS = [
  'https://socialclimber.us',
  'https://app.socializerhub.de',
  'https://social.agentcrate.com',
  'https://socialapp.saleslogy.com',
  'https://social.klibrand.com',
];

export const providedAnalyticsAccountIds = [14, 5, 1, 21, 9];
export const providedInboxAccountIds = [5];

export const LOCAL_DEVELOPMENT_DOMAINS = '192.168.1.124';
const PORT_MAP = {
  3000: 138,
  4000: 105,
  5000: 130,
  6000: 108,
  7000: 140,
  8000: 112,
  9000: 145,
};
let IS_PRODUCTION = false;
let DEVLOPEMENT = false;
let API_URL = 'https://restapi-stag.socialpilot.co';
if (LOCAL_DEVELOPMENT_DOMAIN.some(domain => domain === BASE_URL)) {
  API_URL = 'http://192.168.1.170:3000';
} else if (WEB_DEVELOPMENT_DOMAINS.some(domain => domain === BASE_URL)) {
  // ENTERPRISE = true;
  API_URL = 'https://restapi-stag.socialpilot.co';
} else if (DEVELOPMENT_DOMAINS.some(domain => domain === BASE_URL)) {
  API_URL = 'https://restapi-stag.socialpilot.co';
  DEVLOPEMENT = true;
} else if (WEB_PRODUCTION_DOMAINS.some(domain => domain === BASE_URL)) {
  API_URL = 'https://restapi.socialpilot.co';
  IS_PRODUCTION = true;
} else if (WEB_ENTERPRISE_DOMAINS.some(domain => domain === BASE_URL)) {
  API_URL = 'https://restapi.socialpilot.co';
  ENTERPRISE = true;
  IS_PRODUCTION = true;
} else if (window.location.hostname === LOCAL_DEVELOPMENT_DOMAINS) {
  API_URL = `http://192.168.1.${PORT_MAP[BASE_PORT]}:3000`;
} else {
  API_URL = 'https://restapi.socialpilot.co';
  ENTERPRISE = true;
  IS_PRODUCTION = true;
}
// API_URL = 'http://192.168.1.112:3000';

let bucket_name = 'awsstage-test';
let video_bucket_name = 'awsstage-test-video';
let META_DATA_API_URL = 'https://mediaapi-stag.socialpilot.co/sampleFunction';
if (IS_PRODUCTION) {
  bucket_name = 'sp-media-image';
  video_bucket_name = 'sp-media-video';
  META_DATA_API_URL = 'https://mediaapi.socialpilot.co/metadata';
}

let awsAccessKey = process.env.REACT_APP_AWS_ACCESS_KEY_STAG;
let secretKey = process.env.REACT_APP_AWS_SECRET_KEY_STAG;
if (IS_PRODUCTION) {
  awsAccessKey = process.env.REACT_APP_AWS_ACCESS_KEY_PROD;
  secretKey = process.env.REACT_APP_AWS_SECRET_KEY_PROD;
}
export const ISDEV = DEVLOPEMENT;
export const AWS_ACESS_KEY = awsAccessKey;
export const SECRET_KEY = secretKey;
export const BUCKET_NAME = bucket_name;
export const VIDEO_BUCKET_NAME = video_bucket_name;
export const API_BASE_URL = API_URL;
export const IS_ENTERPRISE = ENTERPRISE;
export const PRODUCTION_ENVIORNMENT = IS_PRODUCTION;
export const META_DATA_ENDPOINT = META_DATA_API_URL;

let auth_domain = process.env.REACT_APP_CONNECT_ACCOUNT_AUTH_DOMAIN;
// if (WEB_DEVELOPMENT_DOMAINS.some(domain => domain === BASE_URL)) {
auth_domain = 'https://app-dev.socialpilot.co';
// }

export const ACCOUNT_AUTH_DOMAIN = auth_domain;

export const betaFeatures = IS_ENTERPRISE ? ['boostPost'] : ['noFeature'];

export const CONFIG = {
  LOCAL: {
    dummyToken:
      'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjb21wYW55SWQiOjI1MjMwLCJyb2xlIjoidXNlciIsImNsaWVudElkIjoiOWk1Z2ZvYzhqbHI1c2ZpNHh2MGZrczV1a2NsY282NHlvOWY0eHJjaCIsImVudGVycHJpc2VJZCI6MSwiY2lkIjowLCJpYXQiOjE1NDIxMTA5MDAsImV4cCI6MTU0NzI5NDkwMH0.aFJ20NC5YJxkWgszrxzKsCCavVJZ10HfNYR3vXrgfpU',
    manageFeedRedirect: `${BASE_URL}`,
    manageGroupRedirect: `${BASE_URL}`,
    feedFormSubmitRedirect: `${BASE_URL}`,
    feedformCancelRedirect: `${BASE_URL}`,
    groupFormSubmitRedirect: `${BASE_URL}`,
    groupFormCancelRedirect: `${BASE_URL}`,
    manageFeedUpdateRelativePath: `${BASE_URL}`,
    manageGroupUpdateRelativePath: `${BASE_URL}`,
    manageFeedDeleteRedirect: `${BASE_URL}`,
    manageFeedUpdateRedirect: `${BASE_URL}`,
    manageGroupUpdateRedirect: `${BASE_URL}`,
    manageGroupDeleteRedirect: `${BASE_URL}`,
    feedCreateMountParam: 'feedcreate',
    groupCreateMountParam: 'creategroup',
    AddfeedRelativePath: '/feedcreate',
    AddGroupRelativePath: '/creategroup',
    noAccountRedirect: '#',
    CACHE_CLEAR_BASE_URL: BASE_URL,
    // API_BASE_URL: 'https://restapi-stag.socialpilot.co'
  },

  STAGING: {
    dummyToken: null,
    manageFeedRedirect:
      'https://staging.socialpilot.co:8080/react/feeds/managefeeds',
    feedFormSubmitRedirect:
      'https://staging.socialpilot.co:8080/react/feeds/managefeeds',
    manageFeedUpdateRelativePath: '/react/feeds/update/',
    manageGroupUpdateRelativePath: '/react/groups/update/',
    manageFeedUpdateRedirect:
      'https://staging.socialpilot.co:8080/react/feeds/managefeeds',
    manageFeedDeleteRedirect:
      'https://staging.socialpilot.co:8080/react/feeds/managefeeds',
    feedformCancelRedirect:
      'https://staging.socialpilot.co:8080/react/feeds/managefeeds',
    AddfeedRelativePath: '/react/feeds/feedcreate',
    AddGroupRelativePath: '/react/groups/creategroup',
    groupFormSubmitRedirect:
      'https://staging.socialpilot.co:8080/react/groups/managegroup',
    groupFormCancelRedirect:
      'https://staging.socialpilot.co:8080/react/groups/managegroup',
    manageGroupRedirect:
      'https://staging.socialpilot.co:8080/react/groups/managegroup',
    manageGroupUpdateRedirect:
      'https://staging.socialpilot.co:8080/react/groups/managegroup',
    manageGroupDeleteRedirect:
      'https://staging.socialpilot.co:8080/react/groups/managegroup',
    createGroupLink:
      'https://staging.socialpilot.co:8080/react/groups/creategroup/',
    inviteClientLink: 'https://staging.socialpilot.co:8080/client/link/create',
    stagingDashboardPath: 'https://staging.socialpilot.co:8080/launchpad',
    feedCreateMountParam: 'feedcreate',
    groupCreateMountParam: 'creategroup',
    // API_BASE_URL: 'https://restapi-stag.socialpilot.co'
  },

  PROD_STAGING: {
    dummyToken: null,
    manageFeedRedirect: `${BASE_URL}/feeds/index`,
    feedFormSubmitRedirect: `${BASE_URL}/feeds/index`,
    manageFeedUpdateRelativePath: '/feeds/update/',
    manageGroupUpdateRelativePath: '/group/update/',
    manageFeedUpdateRedirect: `${BASE_URL}/feeds/index`,
    manageFeedDeleteRedirect: `${BASE_URL}/feeds/index`,
    feedformCancelRedirect: `${BASE_URL}/feeds/index`,
    AddfeedRelativePath: `${BASE_URL}/feeds/create`,
    AddGroupRelativePath: `${BASE_URL}/groups/create`,
    groupFormSubmitRedirect: `${BASE_URL}/groups`,
    groupFormCancelRedirect: `${BASE_URL}/groups`,
    manageGroupRedirect: `${BASE_URL}/groups`,
    manageGroupUpdateRedirect: `${BASE_URL}/groups`,
    manageGroupDeleteRedirect: `${BASE_URL}/groups`,
    createGroupLink: `${BASE_URL}/groups/create`,
    inviteClientLink: `${BASE_URL}/client/link/create`,
    DashboardPath: `${BASE_URL}/launchpad`,
    feedCreateMountParam: 'create',
    groupCreateMountParam: 'create',
    CACHE_CLEAR_BASE_URL: BASE_URL,
    reconnectAccount: `${BASE_URL}/accounts/create`,
    noAccountRedirect: `${BASE_URL}/accounts/create`,
  },

  PRODUCTION: {
    dummyToken: null,
    manageFeedRedirect: `${BASE_URL}/feeds/index`,
    feedFormSubmitRedirect: `${BASE_URL}/feeds/index`,
    manageFeedUpdateRelativePath: '/feeds/update/',
    manageGroupUpdateRelativePath: '/group/update/',
    manageFeedUpdateRedirect: `${BASE_URL}/feeds/index`,
    feedformCancelRedirect: `${BASE_URL}/feeds/index`,
    createGroupLink: `${BASE_URL}/groups/create`,
    inviteClientLink: `${BASE_URL}/client/link/create`,
    AddfeedRelativePath: '/feeds/create',
    AddGroupRelativePath: '/groups/create',
    groupFormSubmitRedirect: `${BASE_URL}/groups`,
    groupFormCancelRedirect: `${BASE_URL}/groups`,
    manageGroupRedirect: `${BASE_URL}/groups`,
    manageGroupUpdateRedirect: `${BASE_URL}/groups`,
    manageGroupDeleteRedirect: `${BASE_URL}/groups`,
    DashboardPath: `${BASE_URL}/launchpad`,
    feedCreateMountParam: 'create',
    groupCreateMountParam: 'create',
    // API_BASE_URL: 'https://restapi.socialpilot.co',
    CACHE_CLEAR_BASE_URL: BASE_URL,
    reconnectAccount: `${BASE_URL}/accounts/create`,
    noAccountRedirect: `${BASE_URL}/accounts/create`,
  },

  RUTUL_LOCAL: {
    dummyToken: null,
    manageFeedRedirect: 'http://192.168.1.170:67/feeds',
    feedFormSubmitRedirect: 'http://192.168.1.170:67/feeds',
    manageFeedUpdateRelativePath: '/feeds/update',
    manageFeedUpdateRedirect: 'http://192.168.1.170:67/feeds',
    manageFeedDeleteRedirect: 'http://192.168.1.170:67/feeds',
    feedformCancelRedirect: 'http://192.168.1.170:67/feeds',
    AddfeedRelativePath: 'feeds/create',
    createGroupLink: 'http://192.168.1.170:67/groups/create',
    inviteClientLink: 'http://192.168.1.170:67/client/link/create',
    stagingDashboardPath: 'http://192.168.1.170:67/launchpad',
    CACHE_CLEAR_BASE_URL: 'https://panel.socialpilot.co',
  },
};

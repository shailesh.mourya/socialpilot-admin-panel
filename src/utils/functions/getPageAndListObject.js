const createObject = (totalRecord, pageLimit, listOfArray, totalPages) => {
  const totalCount = totalRecord || listOfArray.length;
  const limit = pageLimit || 5;
  const totalPage = totalPages || Math.ceil(totalCount / limit);

  const newObject = {};
  // eslint-disable-next-line no-plusplus
  for (let index = 0; index < totalPage; index++) {
    newObject[index + 1] = {
      list: listOfArray.slice(
        (index + 1) * limit - (limit - 1) - 1,
        limit * (index + 1),
      ),
      pageDetails: {
        currentPage: index + 1,
        pageLimit: limit,
        totalPages: totalPage,
        totalRecords: totalCount,
      },
    };
  }

  return newObject;
};

export default createObject;

import { connectAccountRoutes } from '../../features/accounts/connectAccount/mics/connectAccountUrls';
// import queryString from 'query-string';

export default () => {
  let login = true;
  // eslint-disable-next-line no-unused-vars
  const { pathname, search } = window.location;
  if (
    connectAccountRoutes('auth').indexOf(pathname) > -1 ||
    pathname === '/accounts/redirect'
  ) {
    login = false;
  }
  return login;
};

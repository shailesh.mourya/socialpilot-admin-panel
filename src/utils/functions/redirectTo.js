/* eslint-disable no-unused-vars */
import history from '../../history';
import { ACCOUNT_AUTH_DOMAIN, BASE_URL } from '../misc/config';

const parseUrl = (string, prop) => {
  const a = document.createElement('a');
  a.setAttribute('href', string);
  const { host, hostname, pathname, port, protocol, search, hash } = a;
  const origin = `${protocol}//${hostname}${port.length ? `:${port}` : ''}`;
  return prop
    ? // eslint-disable-next-line no-eval
      eval(prop)
    : { origin, host, hostname, pathname, port, protocol, search, hash };
};

const redirectTo = url => {
  const { pathname } = history.location;
  history.push(pathname.replace(pathname, url));
};

export const redirectWithProps = (url, props) => {
  const { pathname } = history.location;
  history.push(pathname.replace(pathname, url), props);
};

export const redirectToExternalSite = url => {
  if (
    ACCOUNT_AUTH_DOMAIN === parseUrl(url, 'origin') &&
    BASE_URL === parseUrl(url, 'origin')
  ) {
    const pushUrl = url.replace(ACCOUNT_AUTH_DOMAIN, '');

    history.push(pushUrl);
  } else {
    window.location.href = url;
  }
};

export default redirectTo;

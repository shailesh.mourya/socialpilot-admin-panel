import { DateTime } from 'luxon';

export const countDiffInDays = (startDate, endDate, timezoneName = '') => {
  const startDateISO = timezoneName
    ? DateTime.fromISO(`${startDate}T00:00:00`, {
        zone: timezoneName,
      })
    : DateTime.fromISO(startDate);
  const endDateISO = timezoneName
    ? DateTime.fromISO(`${endDate}T23:59:59`, {
        zone: timezoneName,
      })
    : DateTime.fromISO(endDate);
  const diff = endDateISO.diff(startDateISO, 'days');
  return timezoneName
    ? Math.ceil(diff.toObject().days)
    : diff.toObject().days + 1;
};

export default type => {
  switch (type) {
    case 'facebook-official':
      return 'fa fa-facebook facebook-official_round';
    case 'instagram':
      return 'fa fa-instagram instagram_round';
    case 'twitter':
      return 'fab fa-twitter twitter_round';
    case 'vk-group':
      return 'fa fa-vk vk-group_round';
    case 'linkedin':
      return 'fa fa-linkedin linkedin_round';
    case 'pinterest-p':
      return 'fab fa-pinterest';
    case 'tumblr':
      return 'fa fa-tumblr tumblr_round';
    case 'vk':
      return 'fa fa-vk  vk-group_round';
    case 'briefcase':
      return 'fas fa-briefcase briefcase_round';
    case 'suitcase':
      return 'fas fa-suitcase suitcase_round';
    case 'xing':
      return 'fa fa-xing xing_round';
    case 'users':
      return 'fas fa-users users_round';
    case 'buffer':
      return 'fa fa-buffer buffer_round';
    case 'bullhorn':
      return 'fas fa-bullhorn bullhorn_round';

    default:
      return `fa fa-${type}`;
  }
};

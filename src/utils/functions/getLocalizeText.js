import Cookies from 'js-cookie';

import de from '../../languges/german/german';
import es from '../../languges/spanish/spanish';
import pt from '../../languges/portugeaze/portugeaze';

export default (message, defaultLanguage = Cookies.get('language')) => {
  if (defaultLanguage === 'es') {
    return es[message] || message;
  }
  if (defaultLanguage === 'de') {
    return de[message] || message;
  }
  if (defaultLanguage === 'pt') {
    return pt[message] || message;
  }
  return message;
};

const compareArray = (arr1, arr2) => {
  const finalArr = [];
  arr1.forEach(element1 =>
    arr2.forEach(element2 => {
      if (element1 === element2) {
        finalArr.push(element1);
      }
    }),
  );

  return finalArr.length === arr1.length && finalArr.length === arr2.length;
};

export default compareArray;

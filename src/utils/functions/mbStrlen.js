export default (str, maxLength) => {
  const stringArray = Array.from(str);

  if (stringArray.length < maxLength) {
    return str;
  }
  return `${stringArray.splice(0, maxLength).join('')}...`;
};

const isParamInURL = param => {
  const URLpath = window.location.pathname.split('/');
  return URLpath.indexOf(param);
};

export default isParamInURL;

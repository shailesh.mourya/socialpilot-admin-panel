// This common function is used to load the scripts for canva, dropbox,
// box API and google drive

const loadScript = (scriptUrl, scriptId, clientKey, callback) => {
  const existingScript = document.getElementById(scriptId);

  if (!existingScript) {
    const script = document.createElement('script');
    const s = document.getElementsByTagName('script')[0];

    script.src = scriptUrl;
    script.id = scriptId;
    if (clientKey) {
      const appKey = clientKey;
      script.setAttribute('data-app-key', appKey);
    }

    s.parentNode.insertBefore(script, s);

    script.onload = () => {
      if (callback) callback();
    };
  }

  if (existingScript && callback) callback();
};

export default loadScript;

/* eslint-disable */
const getQueryStringParams = key => {
  decodeURIComponent(
    window.location.search.replace(
      new RegExp(
        `^(?:.*[&\\?]${encodeURIComponent(key).replace(
          /[\.\+\*]/g,
          '\\$&',
        )}(?:\\=([^&]*))?)?.*$`,
        'i',
      ),
      '$1',
    ),
  );
};

export default getQueryStringParams;

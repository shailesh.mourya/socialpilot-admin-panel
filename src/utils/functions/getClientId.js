import {
  WEB_DEVELOPMENT_DOMAINS,
  BASE_URL,
  LOCAL_DEVELOPMENT_DOMAINS,
  DEVELOPMENT_DOMAINS,
  WEB_DEVELOPMENT_ENTERPRISE_DOMAINS,
} from '../misc/config';

const getClientId = API_URL => {
  let CLIENT_ID = null;
  const pathArr = API_URL.split('?')[0].split('/');

  if (
    pathArr.indexOf('auth') !== -1 ||
    (pathArr.indexOf('client') !== -1 && pathArr.indexOf('signup') !== -1) ||
    (pathArr.indexOf('client') !== -1 && pathArr.indexOf('login') !== -1) ||
    pathArr.indexOf('reset-password') !== -1 ||
    pathArr.indexOf('create-password') !== -1 ||
    pathArr.indexOf('verify-link') !== -1 ||
    pathArr.indexOf('video') !== -1 ||
    (pathArr.indexOf('team') !== -1 && pathArr.indexOf('create') !== -1) ||
    pathArr.indexOf('confirminvitation') !== -1 ||
    pathArr.indexOf('accept') !== -1 ||
    pathArr.indexOf('enterprise') !== -1
  ) {
    if (
      WEB_DEVELOPMENT_DOMAINS.some(domain => domain === BASE_URL) ||
      WEB_DEVELOPMENT_ENTERPRISE_DOMAINS.some(domain => domain === BASE_URL) ||
      DEVELOPMENT_DOMAINS.some(domain => domain === BASE_URL) ||
      window.location.hostname === LOCAL_DEVELOPMENT_DOMAINS
    ) {
      CLIENT_ID = '9i5gfoc8jlr5sfi4xv0fks5ukclco64yo9f4xrch';
    } else {
      CLIENT_ID = 'f1s8ddnyvboelbm5vtmpleq66glqm2l13e9hrxgm';
    }
  }
  return CLIENT_ID;
};

export default getClientId;

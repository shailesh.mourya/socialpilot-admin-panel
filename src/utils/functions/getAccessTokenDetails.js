/* eslint-disable no-empty */
import base64 from 'base-64';
import getCookie from './getCookie';

export default () => {
  try {
    const token = getCookie('token');
    if (token) {
      const splitToken = token.split('.');
      return base64.decode(splitToken[1]);
    }
  } catch (e) {}

  return false;
};

import axios from 'axios';

const clearcache = (apiURL, shouldRedirect = false, redirectURL = null) => {
  axios({
    url: apiURL,
    method: 'GET',
    data: null,
    headers: { 'Cache-Control': 'no-cache' },
  })
    .then(response => {
      if (response.status === 200 && shouldRedirect) {
        window.location.href = redirectURL;
      }
    })
    .catch(err => {
      console.error(err);
      window.location.href = redirectURL;
    });
};

export default clearcache;

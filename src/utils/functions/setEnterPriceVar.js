const setEnterPriceVariable = (proprty, value) => {
  document.body.style.setProperty(`--${proprty}`, value);
};

export const setEnterPriceAllVariables = data => {
  const {
    primaryBtnColor,
    secondaryBtnColor,
    sidebarWideMenuColor,
    sidebarColor,
    iconSpriteImg,
  } = data;
  // extra fields like hover, focus  we can add this to db or use opacity instead
  // in scss file with primary colors

  setEnterPriceVariable('colorPrimary', primaryBtnColor);
  setEnterPriceVariable('primaryBtnHover', primaryBtnColor);
  setEnterPriceVariable('primaryBtnFocus', primaryBtnColor);
  setEnterPriceVariable('colorSecondary', secondaryBtnColor);
  setEnterPriceVariable('secoundaryBtnHover', secondaryBtnColor);
  setEnterPriceVariable('btnSecondary', secondaryBtnColor);
  setEnterPriceVariable('sideBarColor', sidebarWideMenuColor);
  setEnterPriceVariable('sideBarWrapper', sidebarColor);
  setEnterPriceVariable('stripeSideMenuIconImage', `url(${iconSpriteImg})`);
};

export default setEnterPriceVariable;

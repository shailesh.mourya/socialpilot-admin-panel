import getCookie from './getCookie';

const getAuthToken = fieldName => {
  const token = getCookie(fieldName);
  return token === '' ? null : `Bearer ${token}`;
};

export default getAuthToken;

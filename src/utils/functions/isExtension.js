import queryString from 'query-string';

const isExtension = location => {
  if (location) {
    const { search } = location;
    if (search) {
      const searchValues = queryString.parse(search);
      if (searchValues) {
        const { from } = searchValues;

        if (from === 'extension' || from === 'twitterextension') {
          return from;
        }
      }
    }
  }

  return false;
};

export const isTwitterExtension = location => {
  if (location) {
    const { search } = location;
    if (search) {
      const searchValues = queryString.parse(search);
      if (searchValues) {
        const { image, title, url } = searchValues;

        if (image || title || url) {
          return searchValues;
        }
      }
    }
  }

  return false;
};

export default isExtension;

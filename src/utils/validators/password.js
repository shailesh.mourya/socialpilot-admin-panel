import GlobalDisplayTexts from '../labels/localizationText';

export const passwordValidator = password => {
  const required = (errMsg = GlobalDisplayTexts.PASSWORD_REQUIRED_ERROR) =>
    !password || !password.trim() ? errMsg : '';

  const min = (errMsg = GlobalDisplayTexts.PASSWORD_LIMIT_ERROR) =>
    password.trim().length < 5 ? errMsg : '';

  return { required, min };
};

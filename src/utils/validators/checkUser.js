export const checkUser = userSetting => {
  const {
    role,
    isLocked,
    userLocked,
    membershipId,
    loginAsTeamMember,
    permission,
    enableAnalytics,
  } = userSetting.successResponse.data;

  const isLock = () =>
    membershipId === 13 || isLocked === 'Y' || userLocked === 'Y';

  const isTeamMember = () =>
    role === 'A' || role === 'T' || (role === 'O' && loginAsTeamMember === 'Y');

  const isClient = () => role === 'C';

  const viewPost = () => permission.viewPost === 'Y';

  const canBoost = () => permission.canBoost === 'Y';

  const canCreatePost = () => permission.createPost === 'Y';

  const addFeed = () => permission.addFeed === 'Y';

  const isAnalyticsAccess = () => enableAnalytics === 'Y';

  return {
    isLock,
    isTeamMember,
    isClient,
    viewPost,
    canCreatePost,
    canBoost,
    addFeed,
    isAnalyticsAccess,
  };
};

export const MatchPath = path => {
  const groupUnAuthPath = () =>
    path.includes('group') || path.includes('groups');

  const postUnAuthPath = () =>
    (path.includes('posts') ||
      path.includes('draft') ||
      path.includes('calendar')) &&
    !path.includes('bulkschedule');

  const clientUnAuthPath = () =>
    path.includes('posts') &&
    (path.includes('unschedule') ||
      path.includes('delivered') ||
      path.includes('error') ||
      path.includes('contribution'));

  const feedUnAuthPath = () => path.includes('feeds');

  const adsUnAuthPath = () => path.includes('ads');

  const analyticsAuthPath = () => path.includes('analytics');

  const urlshortening = () => path.includes('urlshortening');

  const createPostRoute = () =>
    path.includes('draft') ||
    path.includes('suggestion') ||
    path.includes('articles') ||
    path.includes('posts/create') ||
    path.includes('bulkschedule');

  const membership = () => path.includes('membership');

  const teamClientAuthPath = () =>
    !path.includes('posts') &&
    (path.includes('team') || path.includes('client'));

  const billingRoutes = () => path.includes('membership');

  return {
    groupUnAuthPath,
    postUnAuthPath,
    feedUnAuthPath,
    adsUnAuthPath,
    analyticsAuthPath,
    createPostRoute,
    clientUnAuthPath,
    urlshortening,
    membership,
    teamClientAuthPath,
    billingRoutes,
  };
};

export const checkUnAuthAdmin = (userSetting, history) => {
  const Path = MatchPath(history.location.pathname);

  return Path.billingRoutes();
};

export const checkUnAuthorizedTeam = (userSetting, history) => {
  const Path = MatchPath(history.location.pathname);
  const user = checkUser(userSetting);

  if (history.location.pathname === '/feeds' && user.isTeamMember()) {
    return true;
  }
  //  for client group
  if (Path.groupUnAuthPath() && user.isClient()) {
    if (user.canCreatePost()) {
      return false;
    }
    return !user.viewPost() || !user.canBoost();
  }

  // create post path
  if (Path.createPostRoute()) {
    return !user.canCreatePost();
  }

  // manage-post path
  if (Path.postUnAuthPath()) {
    return !user.viewPost();
  }

  // Team-client
  if (Path.teamClientAuthPath()) {
    return true;
  }

  // client unauthorized path
  if (user.isClient() && Path.clientUnAuthPath()) {
    return !user.viewPost();
  }
  // feeds manage-feed and create -feed
  if (Path.feedUnAuthPath()) {
    return !user.addFeed();
  }

  // boost post related path
  if (Path.adsUnAuthPath()) {
    const { clientMediaAccounts } = userSetting.successResponse.data;
    if (clientMediaAccounts.includes('23')) {
      return false;
    }
    return !user.canBoost();
  }

  // analytics path
  if (Path.analyticsAuthPath() && !user.isClient && !user.isTeamMember) {
    return !user.isAnalyticsAccess();
  }

  // url-shortening path
  if (Path.urlshortening() || Path.membership()) {
    return true;
  }

  return false;
};

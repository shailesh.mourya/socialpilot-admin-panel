import englishText from '../localizationText';

const portugeaze = {
  [englishText.PASSWORD_LIMIT_ERROR]:
    'a password deve conter pelo menos 5 caracteres',
  [englishText.ADD_TO_QUEUE]: 'Adicionar à Fila',
  [englishText.SCHEDULE_POST]: 'Agendar Post',
  [englishText.DELETE]: 'Apagar',
  [englishText.USER_EMAIL]: 'Apagar tudo',
  [englishText.DELETE_ALL]: 'Apagar tudo',
  [englishText.UPDATE]: 'Atualizar',
  [englishText.UPGRADE_NOW]: 'Atualize Agora',
  [englishText.UPGRADE_MEMBERSHIP_TITLE]: 'Atualize seu Membership',
  [englishText.UPGRADE_MEMBERSHIP_MSG]:
    'Atualize seu membership para ativar esse recurso.',
  [englishText.CANCEL]: 'Cancelar',
  [englishText.CLIENTS]: 'Clientes',
  [englishText.ACCOUNTS]: 'Contas',
  [englishText.FEED_CONTENT_TAB]: 'Conteúdo do Feed',
  [englishText.CURATED_CONTENT_TAB]: 'Conteúdo Tratado',
  [englishText.CREATE_POST_TAB]: 'Criar post',
  [englishText.START_FREE_TRIAL]: 'Dê início a sua Trial gratuita',
  [englishText.DRAFT_TAB]: 'Drafts',
  [englishText.EDIT_POST]: 'Editar post',
  [englishText.SUBMIT]: 'Enviar',
  [englishText.ERROR]: 'Erro',
  [englishText.VALIDATION_FAILED]: 'Falha de validação',
  [englishText.GROUPS]: 'Grupos',
  [englishText.IMAGE]: 'Imagem',
  [englishText.NO]: 'Não',
  [englishText.NO_RECORD_MSG]: 'Nenhum Registro Encontrado',
  [englishText.NO_RECORD_FOUND]: 'Nenhum Registro Encontrado.',
  [englishText.SHARE_NEXT]: 'Partilhar próximo',
  [englishText.SHARE_NOW]: 'Partilhe agora',
  [englishText.PLEASE_RECONNECT]: 'Por favor, reconecte sua conta.',
  [englishText.FIRST_NAME]: 'Primeiro nome',
  [englishText.SEARCH]: 'Procurar',
  [englishText.REPEAT_POST]: 'Repetir Post',
  [englishText.SAVE_AS_DRAFT]: 'Salvar como draft',
  [englishText.SELECT_ALL]: 'Selecionar tudo',
  [englishText.SELECT_ACCOUNT]: 'Selecione uma conta',
  [englishText.SELECT_ALL_ACCOUNTS]: 'Selecionar todas as contas',
  [englishText.YES]: 'Sim',
  [englishText.TEXT]: 'Texto',
  [englishText.ALL]: 'Todos',
  [englishText.LAST_NAME]: 'Último nome',
  [englishText.VIDEO]: 'Vídeo',

  [englishText.SAVE]: 'Save',
  [englishText.TEAMS]: 'Teams',
  [englishText.UPGRADE_PLAN_NOW]: 'Upgrade Your Plan Now!',
  [englishText.UPGRADE_TO_ACCESS_PRO_FAETURE]:
    'Upgrade your Membership to access this pro feature!',
  [englishText.NOT_ELIGIBLE_FOR_TRIAL_MSG]:
    'Oops! Looks like you are not eligible for any more trials.',
  [englishText.NO_ACCESS_UPGRADE_MSG]:
    "Uh..oh. Looks like you don't have access to this feature.Upgrade and continue to use this feature.",
  [englishText.NO_ACCESS_CONTACT_OWNER_MSG]:
    "Oops! Looks like you don't have access to this feature.To know more, please contact the account’s owner.",
  [englishText.TEAM_MEMBER_LIMIT_REACHED_UPGRADE]:
    'Uh..oh. Looks like you’ve reached your limit of adding team members. Please upgrade to a higher plan and add more members.',
  [englishText.TEAM_MEMBER_LIMIT_REACHED_MSG]:
    'Uh..oh. Looks like you’ve reached your limit of adding team members. Want to add more? Contact us!',
  [englishText.TEAM_MEMBER_LIMIT_REACHED_CONTACT_OWNER]:
    'Uh..oh.This account has reached its limit of adding team members.Please contact the owner for more information.',
  [englishText.FORM_SUBMIT_NETWORK_ERROR]:
    'Error Submitting the form due to network error',
  [englishText.FORM_SUBMIT_ERROR]: 'Error Submitting the form.',
  [englishText.DATA_FETCH_NETWORK_ERROR]:
    'data cannot be fetched due to network error',
  [englishText.ERROR_OCCURED]: 'An error occurred. Try again!',
  [englishText.AN_ERROR_OCCURED]: 'An error occurred. Try again!',
  [englishText.CANT_LOAD_DATA_TRY_AGAIN]:
    "Sorry! We couldn't load the data.Try again?",
  [englishText.SOMETHING_WENT_WRONG_SHORT]: 'something went wrong.',
  [englishText.DELETE_SUCCESSFUL]: 'Delete Successful',
  [englishText.DELETE_FAIL]: 'Delete fail',
  [englishText.DELETE_IN_PROCESS]: 'Delete In Process',
  [englishText.AUTH_ERROR]: 'Authentication error',
  [englishText.PASSWORD_REQUIRED_ERROR]: 'Password is required',
  [englishText.ERROR_OC]: 'Error Occured.',
  [englishText.SELECT_OPTION]: 'Select option',
  [englishText.CONNECT_FB_PAGES]: 'Connect your Facebook Page(s).',
  [englishText.SOMETHING_WENT_WRONG]:
    'Something went wrong. Please try again later',
  [englishText.CLEAR_ALL]: 'Clear All',
  [englishText.YOU_ARE_NOT_AUTHORIZED]:
    'You are not authorized to do this action.',
  [englishText.YOU_ARE_NOT_AUTHORIZED_TO_PERFORM]:
    'Oops! You are not authorized to perform this action.',
  [englishText.ADD_SCHEDULE]: '+ Add Schedule',
  [englishText.TIME_1]: '1 Time',
  [englishText.TIME_2]: '2 Times',
  [englishText.TIME_3]: '3 Times',
  [englishText.TIME_4]: '4 Times',
  [englishText.TIME_5]: '5 Times',
  [englishText.TIME_6]: '6 Times',
  [englishText.TIME_7]: '7 Times',
  [englishText.TIME_8]: '8 Times',
  [englishText.TIME_9]: '9 Times',
  [englishText.TIME_10]: '10 Times',
  [englishText.LOOKS_LIKE_FEATURE_NO_AVAILABLE]:
    "Oops! Looks like you don't have access to this feature.Please contact your team owner.",
  [englishText.SEARCH_ACCOUNTS]: 'Search Accounts',
};

export default portugeaze;

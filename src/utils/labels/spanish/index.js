import englishText from '../localizationText';

const spanish = {
  [englishText.ADD_TO_QUEUE]: 'A\u00f1adir a la Cola',
  [englishText.UPGRADE_MEMBERSHIP_MSG]:
    'Actualice su suscripci\u00f3n para activar esta funci\u00f3n.',
  [englishText.UPGRADE_NOW]: 'Actualiza Ahora Mismo',
  [englishText.UPDATE]: 'Actualizar',
  [englishText.LAST_NAME]: 'Apellido',
  [englishText.DRAFT_TAB]: 'Borradores',
  [englishText.SEARCH]: 'Buscar',
  [englishText.CANCEL]: 'Cancelar',
  [englishText.CLIENTS]: 'Clientes',
  [englishText.START_FREE_TRIAL]: 'Comienza tu Periodo de Prueba',
  [englishText.SHARE_NOW]: 'Compartir Ahora',
  [englishText.SHARE_NEXT]: 'Compartir la Pr\u00f3xima',
  [englishText.FEED_CONTENT_TAB]: 'Contenido de canal',
  [englishText.CURATED_CONTENT_TAB]: 'Contenido sugerido',
  [englishText.PASSWORD_REQUIRED_ERROR]: 'Contraseña is required',
  [englishText.CREATE_POST_TAB]: 'Crear publicación',
  [englishText.ACCOUNTS]: 'Cuentas',
  [englishText.USER_EMAIL]: 'Direcci\u00f3n de e-mail',
  [englishText.EDIT_POST]: 'Editar un Post',
  [englishText.DELETE]: 'Eliminar todos',
  [englishText.DELETE_ALL]: 'Eliminar todos',
  [englishText.SUBMIT]: 'Enviar',
  [englishText.ERROR]: 'Error',
  [englishText.GROUPS]: 'Grupos',
  [englishText.SAVE_AS_DRAFT]: 'Guardar como borrador',
  [englishText.IMAGE]: 'Imagen',
  [englishText.UPGRADE_MEMBERSHIP_TITLE]: 'Mejore su membresía.',
  [englishText.NO]: 'No',
  [englishText.NO_RECORD_MSG]: 'No se encontró registro',
  [englishText.NO_RECORD_FOUND]: 'No se encontró registro.',
  [englishText.FIRST_NAME]: 'Nombre',
  [englishText.SCHEDULE_POST]: 'Programar Post',
  [englishText.REPEAT_POST]: 'Repetir publicación',
  [englishText.SELECT_ACCOUNT]: 'Seleccionar cuenta',
  [englishText.SELECT_ALL_ACCOUNTS]: 'Seleccionar todas las cuentas',
  [englishText.SELECT_ALL]: 'Seleccionar todo',
  [englishText.YES]: 'Sim',
  [englishText.TEXT]: 'Texto',
  [englishText.ALL]: 'Todo',
  [englishText.VIDEO]: 'Video',

  [englishText.SAVE]: 'Save',
  [englishText.TEAMS]: 'Teams',
  [englishText.UPGRADE_PLAN_NOW]: 'Upgrade Your Plan Now!',
  [englishText.UPGRADE_TO_ACCESS_PRO_FAETURE]:
    'Upgrade your Membership to access this pro feature!',
  [englishText.NOT_ELIGIBLE_FOR_TRIAL_MSG]:
    'Oops! Looks like you are not eligible for any more trials.',
  [englishText.NO_ACCESS_UPGRADE_MSG]:
    "Uh..oh. Looks like you don't have access to this feature.Upgrade and continue to use this feature.",
  [englishText.NO_ACCESS_CONTACT_OWNER_MSG]:
    "Oops! Looks like you don't have access to this feature.To know more, please contact the account’s owner.",
  [englishText.TEAM_MEMBER_LIMIT_REACHED_UPGRADE]:
    'Uh..oh. Looks like you’ve reached your limit of adding team members. Please upgrade to a higher plan and add more members.',
  [englishText.TEAM_MEMBER_LIMIT_REACHED_MSG]:
    'Uh..oh. Looks like you’ve reached your limit of adding team members. Want to add more? Contact us!',
  [englishText.TEAM_MEMBER_LIMIT_REACHED_CONTACT_OWNER]:
    'Uh..oh.This account has reached its limit of adding team members.Please contact the owner for more information.',
  [englishText.FORM_SUBMIT_NETWORK_ERROR]:
    'Error Submitting the form due to network error',
  [englishText.FORM_SUBMIT_ERROR]: 'Error Submitting the form.',
  [englishText.DATA_FETCH_NETWORK_ERROR]:
    'data cannot be fetched due to network error',
  [englishText.ERROR_OCCURED]: 'An error occurred. Try again!',
  [englishText.AN_ERROR_OCCURED]: 'An error occurred. Try again!',
  [englishText.CANT_LOAD_DATA_TRY_AGAIN]:
    "Sorry! We couldn't load the data.Try again?",
  [englishText.SOMETHING_WENT_WRONG_SHORT]: 'something went wrong.',
  [englishText.DELETE_SUCCESSFUL]: 'Delete Successful',
  [englishText.DELETE_FAIL]: 'Delete fail',
  [englishText.DELETE_IN_PROCESS]: 'Delete In Process',
  [englishText.VALIDATION_FAILED]: 'Validation Failed',
  [englishText.AUTH_ERROR]: 'Authentication error',
  [englishText.PASSWORD_LIMIT_ERROR]:
    'Password should contain at least 5 characters.',
  [englishText.ERROR_OC]: 'Error Occured.',
  [englishText.SELECT_OPTION]: 'Select option',
  [englishText.CONNECT_FB_PAGES]: 'Connect your Facebook Page(s).',
  [englishText.PLEASE_RECONNECT]: 'Please reconnect your account.',
  [englishText.SOMETHING_WENT_WRONG]:
    'Something went wrong. Please try again later',
  [englishText.CLEAR_ALL]: 'Clear All',
  [englishText.YOU_ARE_NOT_AUTHORIZED]:
    'You are not authorized to do this action.',
  [englishText.YOU_ARE_NOT_AUTHORIZED_TO_PERFORM]:
    'Oops! You are not authorized to perform this action.',
  [englishText.ADD_SCHEDULE]: '+ Add Schedule',
  [englishText.TIME_1]: '1 Time',
  [englishText.TIME_2]: '2 Times',
  [englishText.TIME_3]: '3 Times',
  [englishText.TIME_4]: '4 Times',
  [englishText.TIME_5]: '5 Times',
  [englishText.TIME_6]: '6 Times',
  [englishText.TIME_7]: '7 Times',
  [englishText.TIME_8]: '8 Times',
  [englishText.TIME_9]: '9 Times',
  [englishText.TIME_10]: '10 Times',
  [englishText.LOOKS_LIKE_FEATURE_NO_AVAILABLE]:
    "Oops! Looks like you don't have access to this feature.Please contact your team owner.",
  [englishText.SEARCH_ACCOUNTS]: 'Search Accounts',
};

export default spanish;

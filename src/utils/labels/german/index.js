import englishText from '../localizationText';

const german = {
  [englishText.TEXT]: 'Text', // change this
  [englishText.CANCEL]: 'Abbrechen',
  [englishText.SUBMIT]: 'Absenden',
  [englishText.UPGRADE_MEMBERSHIP_TITLE]:
    'Aktualisieren Sie Ihre Mitgliedschaft',
  [englishText.UPGRADE_MEMBERSHIP_MSG]:
    'Aktualisieren Sie Ihre Mitgliedschaft, um diese Funktion zu aktivieren.',
  [englishText.ALL]: 'Alle',
  [englishText.SELECT_ALL]: 'Alle auswählen',
  [englishText.DELETE_ALL]: 'Alles l\u00f6schen',
  [englishText.SAVE_AS_DRAFT]: 'Als Entwurf speichern',
  [englishText.EDIT_POST]: 'Beitrag bearbeiten',
  [englishText.CREATE_POST_TAB]: 'Beitrag erstellen',
  [englishText.SCHEDULE_POST]: 'Beitrag planen',
  [englishText.IMAGE]: 'Bild',
  [englishText.USER_EMAIL]: 'E-Mail-Addresse',
  [englishText.DRAFT_TAB]: 'Entw\u00fcrfe',
  [englishText.FEED_CONTENT_TAB]: 'Feed Inhalt',
  [englishText.ERROR]: 'Fehler',
  [englishText.GROUPS]: 'Gruppen',
  [englishText.YES]: 'Ja',
  [englishText.SHARE_NOW]: 'Jetzt teilen',
  [englishText.UPGRADE_NOW]: 'Jetzt upgraden',
  [englishText.NO_RECORD_MSG]: 'Kein Datensatz gefunden',
  [englishText.NO_RECORD_FOUND]: 'Kein Eintrag gefunden.',
  [englishText.ACCOUNTS]: 'Konten',
  [englishText.SELECT_ACCOUNT]: 'Konto auswählen',
  [englishText.SELECT_ALL_ACCOUNTS]: 'Alle Konto auswählen',
  [englishText.START_FREE_TRIAL]: 'Kostenlos testen',
  [englishText.CLIENTS]: 'Kunden',
  [englishText.DELETE]: 'Löschen',
  [englishText.SHARE_NEXT]: 'N\u00e4chsten teilen',
  [englishText.LAST_NAME]: 'Nachname',
  [englishText.NO]: 'Nein',
  [englishText.SEARCH]: 'Suche',
  [englishText.TEXT]: 'Textpost',
  [englishText.UPDATE]: 'Update',
  [englishText.VIDEO]: 'Video',
  [englishText.CURATED_CONTENT_TAB]: 'Vorgeschlagener Inhalt',
  [englishText.FIRST_NAME]: 'Vorname',
  [englishText.REPEAT_POST]: 'Wiederholen Sie den Post',
  [englishText.ADD_TO_QUEUE]: 'Zur Warteschlange hinzuf\u00fcgen',

  [englishText.PASSWORD_LIMIT_ERROR]:
    'Password should contain at least 5 characters.',
  [englishText.VALIDATION_FAILED]: 'Validation Failed',
  [englishText.PLEASE_RECONNECT]: 'Please reconnect your account.',
  [englishText.SAVE]: 'Save',
  [englishText.TEAMS]: 'Teams',
  [englishText.UPGRADE_PLAN_NOW]: 'Upgrade Your Plan Now!',
  [englishText.UPGRADE_TO_ACCESS_PRO_FAETURE]:
    'Upgrade your Membership to access this pro feature!',
  [englishText.NOT_ELIGIBLE_FOR_TRIAL_MSG]:
    'Oops! Looks like you are not eligible for any more trials.',
  [englishText.NO_ACCESS_UPGRADE_MSG]:
    "Uh..oh. Looks like you don't have access to this feature.Upgrade and continue to use this feature.",
  [englishText.NO_ACCESS_CONTACT_OWNER_MSG]:
    "Oops! Looks like you don't have access to this feature.To know more, please contact the account’s owner.",
  [englishText.TEAM_MEMBER_LIMIT_REACHED_UPGRADE]:
    'Uh..oh. Looks like you’ve reached your limit of adding team members. Please upgrade to a higher plan and add more members.',
  [englishText.TEAM_MEMBER_LIMIT_REACHED_MSG]:
    'Uh..oh. Looks like you’ve reached your limit of adding team members. Want to add more? Contact us!',
  [englishText.TEAM_MEMBER_LIMIT_REACHED_CONTACT_OWNER]:
    'Uh..oh.This account has reached its limit of adding team members.Please contact the owner for more information.',
  [englishText.FORM_SUBMIT_NETWORK_ERROR]:
    'Error Submitting the form due to network error',
  [englishText.FORM_SUBMIT_ERROR]: 'Error Submitting the form.',
  [englishText.DATA_FETCH_NETWORK_ERROR]:
    'data cannot be fetched due to network error',
  [englishText.ERROR_OCCURED]: 'An error occurred. Try again!',
  [englishText.AN_ERROR_OCCURED]: 'An error occurred. Try again!',
  [englishText.CANT_LOAD_DATA_TRY_AGAIN]:
    "Sorry! We couldn't load the data.Try again?",
  [englishText.SOMETHING_WENT_WRONG_SHORT]: 'something went wrong.',
  [englishText.DELETE_SUCCESSFUL]: 'Delete Successful',
  [englishText.DELETE_FAIL]: 'Delete fail',
  [englishText.DELETE_IN_PROCESS]: 'Delete In Process',
  [englishText.AUTH_ERROR]: 'Authentication error',
  [englishText.PASSWORD_REQUIRED_ERROR]: 'Password is required',
  [englishText.ERROR_OC]: 'Error Occured.',
  [englishText.SELECT_OPTION]: 'Select option',
  [englishText.CONNECT_FB_PAGES]: 'Connect your Facebook Page(s).',
  [englishText.SOMETHING_WENT_WRONG]:
    'Something went wrong. Please try again later',
  [englishText.CLEAR_ALL]: 'Clear All',
  [englishText.YOU_ARE_NOT_AUTHORIZED]:
    'You are not authorized to do this action.',
  [englishText.YOU_ARE_NOT_AUTHORIZED_TO_PERFORM]:
    'Oops! You are not authorized to perform this action.',
  [englishText.ADD_SCHEDULE]: '+ Add Schedule',
  [englishText.TIME_1]: '1 Time',
  [englishText.TIME_2]: '2 Times',
  [englishText.TIME_3]: '3 Times',
  [englishText.TIME_4]: '4 Times',
  [englishText.TIME_5]: '5 Times',
  [englishText.TIME_6]: '6 Times',
  [englishText.TIME_7]: ' 7 Times',
  [englishText.TIME_8]: '8 Times',
  [englishText.TIME_9]: '9 Times',
  [englishText.TIME_10]: '10 Times',
  [englishText.LOOKS_LIKE_FEATURE_NO_AVAILABLE]:
    "Oops! Looks like you don't have access to this feature. Please contact your team owner.",
  [englishText.SEARCH_ACCOUNTS]: 'Search Accounts',
};

export default german;

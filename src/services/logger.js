/* eslint-disable no-empty */
import callapi from './callapi';
import {
  API_BASE_URL,
  PRODUCTION_ENVIORNMENT,
  ISDEV,
  IS_ENTERPRISE,
} from '../utils/misc/config';

import { sendMessage } from './slack';
import getAccessTokenDetails from '../utils/functions/getAccessTokenDetails';

const log = (type, error) => {
  if (PRODUCTION_ENVIORNMENT || ISDEV || IS_ENTERPRISE) {
    const tokenDetails = getAccessTokenDetails();
    let extraData = {};
    if (tokenDetails) {
      try {
        const { companyId, role, ownerId } = JSON.parse(tokenDetails);
        extraData = {
          companyId,
          role,
          ownerId,
        };
      } catch (e) {}
    }

    const errorData = {
      type,
      error: error.message,
      stack: error.stack,
      ...extraData,
      path: window.location.href,
    };

    callapi(
      `${API_BASE_URL}/error/reactError`,
      'POST',
      {
        data: JSON.stringify(errorData),
      },
      true,
    );
    sendMessage('react-staging', errorData);
  }
};

export default log;

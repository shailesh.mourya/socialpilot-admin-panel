/* eslint-disable no-empty */
const { WebClient } = require('@slack/web-api');

const token = process.env.REACT_APP_SLACK_TOKEN;
const web = new WebClient(token);

export const sendMessage = (channel, error) => {
  try {
    let companyData = '';
    if (error.companyId) {
      companyData = ` 
       *Comapany Id*: ${error.companyId}
       *Role*: ${error.role} 
       *Owner Id*: ${error.ownerId}
       *Location*: ${error.path}`;
    }
    const text = `*REACT APP CRASHED* | <!date^${Math.floor(
      new Date() / 1000,
    )}^{date_long} {time_secs}|Update Slack.> ${companyData} \`\`\` ${error.stack ||
      error} \`\`\``;

    web.chat.postMessage({
      text,
      channel: 'react-staging',
    });
  } catch (e) {}
};

/* axios with abort */

import axios from 'axios';
import Cookies from 'js-cookie';

import getAuthToken from '../utils/functions/getAuthToken';
import getClientId from '../utils/functions/getClientId';
import { META_DATA_ENDPOINT } from '../utils/misc/config';
import history from '../history';

const ABORTED_MESSAGE = 'request aborted';

const callAPI = (
  apiURL,
  apiMethod,
  apiData = null,
  dummytoken,
  resolveDataAsReponse = false,
  abort = false,
  setLastAPICall,
  getLastAPICall,
  baseAPIURL,
  contentType = 'application/json',
  headerRequired = true,
) => {
  const CLIENT_ID = getClientId(apiURL);
  const CLIENT_SECRET = '';
  const requestSource = axios.CancelToken.source();
  if (abort && getLastAPICall && getLastAPICall(baseAPIURL)) {
    getLastAPICall(baseAPIURL).cancel(ABORTED_MESSAGE);
  }
  if (process.env.NODE_ENV === 'test') {
    return Promise.resolve();
  }
  if (setLastAPICall) {
    setLastAPICall(requestSource, baseAPIURL);
  }

  let headers = {
    Authorization: getAuthToken('token'),
    'Content-Type': contentType,
    xclientid: CLIENT_ID,
  };

  if (META_DATA_ENDPOINT === apiURL) {
    headers = {
      Authorization: getAuthToken('token'),
      'Content-Type': contentType,
    };
  } else if (!headerRequired) {
    headers = {
      'Content-Type': contentType,
      xclientid: CLIENT_ID,
      xclientsecret: CLIENT_SECRET,
    };
  }

  return new Promise((resolve, reject) => {
    axios({
      url: apiURL,
      method: apiMethod,
      data: apiData,
      cancelToken: requestSource.token,
      headers,
    })
      .then(response => {
        if (resolveDataAsReponse) {
          resolve(apiData);
        } else {
          resolve(response);
        }
      })
      .catch(err => {
        if (
          err.response &&
          err.response.status === 401 &&
          !history.location.pathname.includes('login')
        ) {
          Cookies.remove('token');
          Cookies.remove('defaultCalendarView');
          history.push('/logout');
        }

        if (!axios.isCancel(err)) {
          reject(err.response);
        }
      });
  });
};

export default callAPI;

import React from 'react';
import Login from './features/auth/login/index';
import { IntlProvider } from 'react-intl';

function App() {
  return (
    <div className="App">
      <IntlProvider>
        <Login />
      </IntlProvider>
    </div>
  );
}

export default App;
